//
//  TripData+CoreDataProperties.swift
//  
//
//  Created by Zeeta Andrade on 16/07/20.
//
//

import Foundation
import CoreData


extension TripData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TripData> {
        return NSFetchRequest<TripData>(entityName: "TripData")
    }

    @NSManaged public var destination: String?
    @NSManaged public var endDate: String?
    @NSManaged public var location: String?
    @NSManaged public var name: String?
    @NSManaged public var startDate: String?
    @NSManaged public var startTime: String?
    @NSManaged public var mileStones: NSSet?
    @NSManaged public var riders: NSSet?

}

// MARK: Generated accessors for mileStones
extension TripData {

    @objc(addMileStonesObject:)
    @NSManaged public func addToMileStones(_ value: MileStone)

    @objc(removeMileStonesObject:)
    @NSManaged public func removeFromMileStones(_ value: MileStone)

    @objc(addMileStones:)
    @NSManaged public func addToMileStones(_ values: NSSet)

    @objc(removeMileStones:)
    @NSManaged public func removeFromMileStones(_ values: NSSet)

}

// MARK: Generated accessors for riders
extension TripData {

    @objc(addRidersObject:)
    @NSManaged public func addToRiders(_ value: Riders)

    @objc(removeRidersObject:)
    @NSManaged public func removeFromRiders(_ value: Riders)

    @objc(addRiders:)
    @NSManaged public func addToRiders(_ values: NSSet)

    @objc(removeRiders:)
    @NSManaged public func removeFromRiders(_ values: NSSet)

}

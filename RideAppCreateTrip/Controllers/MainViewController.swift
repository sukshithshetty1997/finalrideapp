//
//  MainViewController.swift
//  rideapp2
//
//  Created by Sukshith Shetty on 02/03/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//


import UIKit

class MainViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    var pageControl = UIPageControl()
    var defaults = UserDefaults.standard
    var backbutton = CustomNavigationViewController()
    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "One"),
                self.newVc(viewController: "Two"), self.newVc(viewController: "Three")]
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ConfigurePageControl()
        self.PageControl(0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //defaults.set("MainVC", forKey: "Initial")
        backbutton.transparent(controller: self)
        defaults.set("One", forKey: "Tag")
        self.dataSource = self
        self.delegate = self

        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        
            let button = UIBarButtonItem(title: "Skip", style: UIBarButtonItem.Style.done, target: self, action: #selector(Skip))
            self.navigationItem.rightBarButtonItem = button
            self.navigationController?.navigationBar.tintColor = UIColor.orange
            self.navigationController?.navigationItem.rightBarButtonItem = button
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        PageControl(pageControl.currentPage)
    }
    @objc func Skip(){
        defaults.set("LoginVC", forKey: "Initial")
       let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        presentDet(dest)
    }
    func ConfigurePageControl(){
        pageControl = UIPageControl(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 100, width: UIScreen.main.bounds.width, height: 50))
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        pageControl.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        PageControl(self.pageControl.currentPage)
        pageControl.defersCurrentPageDisplay = true
        pageControl.updateCurrentPageDisplay()
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 1, green: 0.5764705882, blue: 0, alpha: 1)
        pageControl.isUserInteractionEnabled = true
        pageControl.isEnabled = true
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 1, green: 0.5764705882, blue: 0, alpha: 1)
        self.view.addSubview(pageControl)
        
    }

    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
   
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else {

            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
      
        guard orderedViewControllersCount != nextIndex else {
       
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageControl = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageControl)!
        PageControl(self.pageControl.currentPage)
        
    }
    func PageControl(_ value:Int) {
        for i in 0...2{
            if i == value{
                self.pageControl.subviews[i].transform = CGAffineTransform(scaleX: 2, y: 2)
            }
            else{
                self.pageControl.subviews[i].transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                
            }
        }
    }
    
}
extension UIPageControl {
    func customPageControl(dotWidth: CGFloat) {
        for (_, dotView) in self.subviews.enumerated() {
            dotView.frame.size = CGSize.init(width: dotWidth, height: dotWidth)
        }
    }
}

//
//  ViewController.swift
//  RideappLoginPageDesign
//
//  Created by Shruthi on 06/03/20.
//  Copyright © 2020 Robosoft. All rights reserved.
//

import UIKit
import KeychainSwift

struct Keys {
    static let keyPrefix = Prefix()
    static let userName = "userName"
    static let password = "password"
    static let profile = "profile"
    static let accessToken = "accessToken"
    static let refreshToken = "refreshToken"
}
struct Prefix {
     let key1 = "rideAppLogin"
}
class LoginViewController: UIViewController {
    let userdefaults = UserDefaults.standard
    let authobject = AuthenticationInteractor()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let navigationControllerObject = CustomNavigationViewController()
    
//    let userdefaults = UserDefaults.standard

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    let button = UIButton(type:.custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.hideNavigationBar(controller: self)
        loginButton.layer.cornerRadius = loginButton.frame.size.height/2
        passwordTextField.rightViewMode = .always
        button.setImage(UIImage(named: "eye(1).png"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: -24, bottom: 5, right: 12)
        button.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width-25), y: CGFloat(15), width: CGFloat(15), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.eyeVisibility), for: .touchUpInside)
        passwordTextField.rightView = button
        passwordTextField.rightViewMode = .always
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        userdefaults.set("LoginVC", forKey: "Initial")
        emailTextField.text = userdefaults.value(forKey: "Email") as? String
        passwordTextField.text = userdefaults.value(forKey: "Password") as? String
        
        guard let userName = keychain.get(Keys.userName),
            let password = keychain.get(Keys.password)
            else { return }
        
        if keychain.lastResultCode != noErr {
            print(keychain.lastResultCode)
        }
        emailTextField.text = userName
        passwordTextField.text = password
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action) in alert.dismiss(animated: true, completion: nil)}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func validateFileds() -> Bool {
        if emailTextField.text == "" || passwordTextField.text == "" {
            showAlert(title: "", message: "Email or password fields cannot be empty")
            return false
        } else {
            if emailTextField.text == "" {
                print("Enter valid Phone/Email id")
            } else {
                if String(emailTextField.text!).isValidEmail || String(emailTextField.text!).isValidPhoneNumber {
                } else {
                    showAlert(title: "Error", message: "Invalid Email/Phone number")
                    return false
                }
            
                if (passwordTextField.text?.isValidPassword) == false {
                    showAlert(title: "Error", message: "Enter valid password!!!\nPassword field must contain atleast one small and calpital alphabet, number, special character")
                    return false
                }
            
            }
            return true
        }
    }
    func ValidateEmailForOTP() -> Bool{
        if emailTextField.text == "" || passwordTextField.text == "" {
            showAlert(title: "", message: "Email or password fields cannot be empty")
            return false
        }
        else {
            if emailTextField.text == "" {
                print("Enter valid Phone/Email id")
            }
            else {
                if String(emailTextField.text!).isValidEmail || String(emailTextField.text!).isValidPhoneNumber {
                    
                }
                else {
                    showAlert(title: "Error", message: "Invalid Email/Phone number")
                    return false}
                
            }
            return true
        }
    }
  
    @IBAction func loginButtonPressed(_ sender: Any) {
        if validateFileds() {
            let response = authobject.loginUser(requestBody: ["email":emailTextField.text!, "password": passwordTextField.text!])

            let accessToken = response.data?.accessToken
            let refreshToken = response.data?.refreshToken
            print(accessToken)
            userdefaults.setValue(emailTextField.text, forKey: "Email")
            userdefaults.setValue(passwordTextField.text, forKey: "Password")
//            if let password = passwordTextField.text {
//                let saveSuccessful: Bool = KeychainWrapper.standard.set(password, forKey: "userPassword")
//                print("Save was successfull: \(saveSuccessful)")
//                self.view.endEditing(true)
//            }
            
            guard let userName = self.emailTextField.text,
                let password = self.passwordTextField.text else { return }
            if keychain.set(userName, forKey: Keys.userName, withAccess: KeychainSwiftAccessOptions.accessibleAlways) {
                print("Success")
            } else {
                print("Did not set")
            }

            keychain.set(password, forKey: Keys.password, withAccess: KeychainSwiftAccessOptions.accessibleAlways)

            guard accessToken == nil else{
                print("Hello login")
                keychain.set(accessToken!, forKey: Keys.accessToken, withAccess: KeychainSwiftAccessOptions.accessibleAlways)

                let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! MainTabBar
                self.presentDet(dest)
                return
            }
            if let message = response.message, let errorName = response.name{
                let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                present(alert, animated: true, completion: nil)
            }
            
            
        }
    }
    @IBAction func OTPPage(_ sender: Any) {
        if ValidateEmailForOTP(){
            let response = authobject.forgotPassword(requestBody: ["email" : emailTextField.text!])
            guard let data = response.data?.otpVerificationID else {
                if let message = response.message, let errorName = response.name{
                    let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    present(alert, animated: true, completion: nil)
                }
            return
            }
            let datafile = saveObject(fileName: "VerifyOTP", object: data)
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "OTPLoginNavigate") as! UINavigationController
            self.presentDet(dest)
        }
    }
    
    @IBAction func GoToRegister(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "RegisterNavigate") as! UINavigationController
        self.presentDet(dest)
      //  self.navigationController?.present(dest, animated: true, completion: nil)
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "RegisterNavigate") as! RegisterNavigate
//        self.navigationController?.pushViewController(dest, animated: true)
    }
    
 @objc func eyeVisibility(_ sender: Any){
    (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
    if (sender as! UIButton).isSelected {
        self.passwordTextField.isSecureTextEntry = false
        button.setImage(UIImage(named: "eye(1).png"), for: .normal)
        
    }
    else {
        self.passwordTextField.isSecureTextEntry = true
        button.setImage(UIImage(named: "eye(1).png"), for: .normal)
    }
  }
    
}

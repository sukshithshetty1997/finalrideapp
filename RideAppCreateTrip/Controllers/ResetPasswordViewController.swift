//
//  ResetPasswordViewController.swift
//  RideAppResetPasswordNavigation
//
//  Created by Zeeta Andrade on 12/03/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
import KeychainSwift

class ResetPasswordViewController: UIViewController {
    let auth = AuthenticationInteractor()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    @IBOutlet weak var newPasswordText: UITextField!
    @IBOutlet weak var confirmPasswordText: UITextField!
    @IBOutlet weak var resetButton: UIButton!
    let navigationControllerObject = CustomNavigationViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationControllerObject.navigationBackButtonDesign(controller: self)
        
    }
    
    func showAlertMessage(_ message: String) {
        let alert = UIAlertController(title: "Alert!!", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
        }))
        //alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func validatePassword() -> Bool {
        if newPasswordText.text == "" && confirmPasswordText.text == "" {
            showAlertMessage("Please enter both the fields to proceed")
            return false
        }
        else {
            if newPasswordText.text == confirmPasswordText.text {
                if String(newPasswordText.text!).isValidPassword && String(confirmPasswordText.text!).isValidPassword {
                    return true
                }
                else {
                    showAlertMessage("Password must contain 8 characters with lower and upper case alphabets,atleast one digit and special character")
                    return false
                }
            }
            else {
                showAlertMessage("New Password and the confirm password is not matching")
                return false
            }
        }
    }
    
    @objc func transitionToSuccess() {
        
        let successView = self.storyboard?.instantiateViewController(withIdentifier: "PasswordSuccess") as! PassSuccessViewController
        self.navigationController?.pushViewController(successView, animated: true)
    }
    
    @objc func transitionBackTo() {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        self.present(dest, animated: true, completion: nil)
    }
    
    @IBAction func resetPasswordClicked(_ sender: Any) {
        
        if validatePassword() {
            let newPassword = self.newPasswordText.text
            let token = keychain.get(Keys.accessToken)
           let response = auth.resetPassword(requestBody: ["newPassword" : newPassword], requestHeaders: ["Authorization" : "Bearer \(token!)"])
            guard let data = response.data?.message else{
                print("Error Occured")
               return
            }
            UserDefaults.standard.setValue(newPassword, forKey: "Password")
            transitionToSuccess()
        }
    }


}

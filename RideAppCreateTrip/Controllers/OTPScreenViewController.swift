//
//  OTPScreenViewController.swift
//  RideOtpRecent
//
//  Created by Zeeta Andrade on 12/03/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
import KeychainSwift

class LoginNavigationController: UINavigationController{
    override func viewDidLoad() {
        
    }
}
class RegisterNavigationController: UINavigationController{
    override func viewDidLoad() {
        
    }
}
class OTPScreenViewController: UIViewController, PassControllToMainVC {
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let moduleObject = ModuleHandler()
    let navigationControllerObject = CustomNavigationViewController()
    
    @IBOutlet weak var secondRemainingLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var phoneNoTextField: UILabel!
    @IBOutlet weak var customViewOTP: CustomViewOTP!
    var setScreen = false
    var customViewObject: CustomViewOTP?
    var timer: Timer? = Timer()
    var timeLeft = 60
    var OTPVerificationID:String!
    let auth = AuthenticationInteractor()
    func OTPVerify(){
        self.OTPVerificationID = self.getObject(fileName: "VerifyOTP") as? String
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let present = self.presentingViewController
        if (present?.isKind(of: LoginNavigationController.self))!{
            setScreen = false
        }
        else if (present?.isKind(of: RegisterNavigationController.self))!{
            setScreen = true
        }
        navigationControllerObject.navigationBackButtonDesign(controller: self)
        //navigationControllerObject.transparent(controller: self)
        customViewOTP?.delegate = self
        print(customViewOTP)
        customViewOTP?.switchToResetPassword()
        
//      //Navigation Item in the navigation bar
//      setNavigationBackButton()
        
        //To countdown timer
        setupTimer()
    }
    
    func setupTimer() {
        OTPVerify()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: (#selector(updateTime)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        resendButton.isEnabled = false
        timeLeft -= 1
        secondRemainingLabel.text = "\(timeLeft) seconds left"
        if timeLeft == 0 {
            resendButton.isEnabled = true
            //resendButton.setTitle("enabled", for: .normal)
            timer?.invalidate()
            timer = nil
        }
    }
    
    func setupMobileNumberLabel() {
        let mobileNumber = moduleObject.fetchMobileNumber()
        phoneNoTextField.text = "We have sent an OTP to " + mobileNumber
    }
    
    @objc func transitionBackTo() {
          dismissDet()
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
//        self.present(dest, animated: true, completion: nil)
    }
    
    @IBAction func resendAgainButton(_ sender: Any) {
        let response = auth.resetOTP(requestBody: ["otpVerificationID" : self.OTPVerificationID!])
        guard let data = response.data?.otpVerificationID else{
            if let message = response.message, let errorName = response.name{
                let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                present(alert, animated: true, completion: nil)
            }
            return
        }
        print("Successfully Reset OTP")
        saveObject(fileName: "VerifyOTP", object: data)
        timeLeft = 60
        setupTimer()
    }
    
    func passDataToMainVc(data: Int) {
        if data == 1 {
        let response = auth.verifyOTP(requestBody: ["otpVerificationID": self.OTPVerificationID!, "otp": "1234"])
        guard let AccessData = response.data?.accessToken, let refreshdata = response.data?.refreshToken else {
            if let message = response.message, let errorName = response.name{
                let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                present(alert, animated: true, completion: nil)
            }
            return
        }
            keychain.set(AccessData, forKey: Keys.accessToken, withAccess: KeychainSwiftAccessOptions.accessibleAlways)
            keychain.set(refreshdata, forKey: Keys.refreshToken, withAccess: KeychainSwiftAccessOptions.accessibleAlways)
                print("Successful Saving in Keychain in OTP Page")
                if !setScreen{
                    let resetPasswordController = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
                    self.navigationController?.pushViewController(resetPasswordController, animated: true)
                }
                else if setScreen{
                    let dest = self.storyboard?.instantiateViewController(withIdentifier: "ProfileNavigationController") as! UINavigationController
                    self.presentDet(dest)
                }
            
         
            
        
        
    }

}
}

//
//  UIButton.swift
//  rideAppProfile
//
//  Created by Sukshith Shetty on 09/03/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import Foundation
import UIKit
/*
@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var shadowColor: UIColor? {
        set {
            guard let uiColor = newValue else {return}
            layer.shadowColor = uiColor.cgColor
        }
        get {
            guard let color = layer.shadowColor else {return nil}
            return UIColor(cgColor: color)
        }
    }
    @IBInspectable var shadowWidth: CGFloat {
        set {
            layer.shadowRadius = newValue
            
        }
        get {
            return layer.shadowRadius
        }
    }
    @IBInspectable var shadowOffset: CGSize {
        set {
            
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
*/

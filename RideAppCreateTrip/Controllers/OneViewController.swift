//
//  ViewController.swift
//  RideATC
//
//  Created by Royston Vishal Rodrigues on 28/02/20.
//  Copyright © 2020 Robosoft. All rights reserved.
//

import UIKit

class OneViewController: UIViewController {
 var defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        // defaults.set("MainVC", forKey: "Initial")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() { 
        super.didReceiveMemoryWarning()
}
    
}

class TwoViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

class ThreeViewController: UIViewController {
     var defaults = UserDefaults.standard
    let navigationControllerObject = CustomNavigationViewController()
    
    @IBOutlet weak var register: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        register.layer.cornerRadius = register.frame.size.height/2
        //register.layer.masksToBounds = true
        //register.setGradient(colorOne: Colors.orange, colorTwo: Colors.brightOrange)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //defaults.set("MainVC", forKey: "Initial")
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func RegisterNext(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "ChoiceViewController") as! ChoiceViewController
        self.navigationController?.present(dest, animated: true, completion: nil)
        
    }
}


//
//  ViewController.swift
//  rideapp2
//
//  Created by Sukshith Shetty on 02/03/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class ChoiceViewController: UIViewController{
let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    
    @IBAction func RegisterYes(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        self.presentDet(dest)
        defaults.set("LoginVC", forKey: "Initial")
    }
    
    @IBAction func RegisterNO(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        let alert = UIAlertController(title: "Do you still wanna join the ride?", message: "You can create your account and preferably join the ride with others", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Join", style: UIAlertAction.Style.default, handler: { action in
            self.present(dest, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Don't Wanna Join", style: UIAlertAction.Style.cancel, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

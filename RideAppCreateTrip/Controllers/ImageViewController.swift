//
//  ImageViewController.swift
//  rideAppProfile
//
//  Created by Sukshith Shetty on 09/03/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift

class ImageViewController: UIViewController {
    
    let navigationControllerObject = CustomNavigationViewController()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let riderInteractorObject = RiderInteractor()
    let tripInteractorObject = TripsInteractor()
    @IBOutlet var bigCircle: UIImageView!
      @IBOutlet var MainView: UIView!
    @IBOutlet weak var Awesome: UIButton!
    var littleCircle: UIView!
    var imageView = UIImage()
    @IBOutlet var subview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set("One", forKey: "Tag")
      //  saveProfileToServer()
        navigationControllerObject.navigationBackButtonDesign(controller: self)
        self.bigCircle.image = imageView
      
        MainView.layer.cornerRadius = MainView.frame.height/2
        bigCircle.layer.borderColor = UIColor.gray.cgColor
        bigCircle.layer.borderWidth = 2.5
        bigCircle.layer.cornerRadius = bigCircle.frame.size.height/2
        bigCircle.layer.masksToBounds = true
        
        subview.layer.cornerRadius = subview.frame.height/2
        subview.layer.masksToBounds = true
    }
    
//    func saveProfileToServer() {
//        guard let token = keychain.get(Keys.accessToken) else {
//            return
//        }
//        print(token)
//        
//        let tripResonse = tripInteractorObject.getTrips(requestQueries: ["pageNumber" : "1", "tripParams": "_id", "order": "asc"], requestHeaders: ["Authorization": "Bearer \(token)"])
//        print(tripResonse)
//
//        var mainJSON = [
//            "Profile_picture" : imageView
//            ]
//        guard let json = (try? JSONSerialization.jsonObject(with: imageView)) as? [String: Any] else { return }
//        
//        let data: Data = imageView.jpegData(compressionQuality: 1.0)!
//        
//        let rideResponse = riderInteractorObject.AddProfileImage(requestBody: ["avatar": data], requestHeaders: ["Authorization": "Bearer \(token)"])
//        if let data = rideResponse.data?.message {
//            print(data)
//            print("Succesful in adding Trip")
//            let dest = self.storyboard?.instantiateViewController(withIdentifier: "SuccessTripViewController") as! SuccessTripViewController
//            self.navigationController?.pushViewController(dest, animated: true)
//        } else if let error = rideResponse.name, let message = rideResponse.message {
//            print(error)
//            print(message)
//            let alert = UIAlertController(title: error, message: message, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        }
//        
//    }
    @objc func transitionBackTo() {
        //let dest = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePageViewController") as! ProfilePageViewController
        //self.navigationController?.pushViewController(dest, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func LetsGetToTabBar(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! MainTabBar
        self.presentDet(dest)
      
    }
}

//
//  SuccessViewController.swift
//  RideOtpRecent
//
//  Created by Zeeta Andrade on 12/03/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class PassSuccessViewController: UIViewController {

    let navigationControllerObject = CustomNavigationViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBackButtonDesign(controller: self)
    }
    
    @objc func transitionBackTo() {
        let homePage = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(homePage, animated: true)
    }
    
    func transitioForwardTo() {
        let successViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(successViewController, animated: true)
    }
    @IBAction func doneButtonClicked(_ sender: Any) {
        transitioForwardTo()
    }
}

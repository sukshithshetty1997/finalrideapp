//
//  CustomViewOTP.swift
//  RideOtpRecent
//
//  Created by Zeeta Andrade on 09/03/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

protocol PassControllToMainVC {
    func passDataToMainVc(data: Int)
}

class CustomViewOTP: UIView, UITextFieldDelegate {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var otpTextField1: UITextField!
    @IBOutlet weak var otpTextField2: UITextField!
    @IBOutlet weak var otpTextField3: UITextField!
    @IBOutlet weak var otpTextField4: UITextField!
    
    var flag = 0
    var delegate: PassControllToMainVC?
   
    override init(frame: CGRect) { //Custom view in code
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { //Custom View in IB
        super.init(coder: aDecoder)
        commonInit()
        
        otpTextField1.contentVerticalAlignment = UIControl.ContentVerticalAlignment.bottom
        //otpTextField1.textAlignment = .natural
        
        otpTextField1.delegate = self
        otpTextField2.delegate = self
        otpTextField3.delegate = self
        otpTextField4.delegate = self
        otpTextField1.becomeFirstResponder()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("CustomViewOTP", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
    }
    
    func switchToResetPassword() {
        if otpTextField1.text! != "" && otpTextField2.text! != "" && otpTextField3.text! != "" && otpTextField4.text! != "" {
            if flag == 1 {
                delegate?.passDataToMainVc(data: flag)
            }
        }
    }
    
    @objc func myTargetFunction(textField: UITextField) {
        print("myTargetFunction")
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        flag = 0
        if Int(string) != nil {
            if (textField.text?.count)! < 1 && string.count > 0 {
                if textField == otpTextField1 {
                    otpTextField2.becomeFirstResponder()
                }
                if textField == otpTextField2 {
                    otpTextField3.becomeFirstResponder()
                }
                if textField == otpTextField3 {
                    otpTextField4.becomeFirstResponder()
                }
                if textField == otpTextField4 {
                    flag = 1
                    otpTextField4.resignFirstResponder()
                }
                textField.text = string
                switchToResetPassword()
                return false
            } else if (textField.text?.count)! >= 1 && string.count == 0 {
                if textField == otpTextField2 {
                    otpTextField1.becomeFirstResponder()
                }
                if textField == otpTextField3 {
                    otpTextField2.becomeFirstResponder()
                }
                if textField == otpTextField4 {
                    otpTextField3.becomeFirstResponder()
                }
                if textField == otpTextField1 {
                    otpTextField1.resignFirstResponder()
                }                
                textField.text = ""
                return false
            }
            else if (textField.text?.count)! >= 1 {
                textField.text = string
                return true
            }
            return true
        } else {
            if (textField.text?.count)! >= 1 && string.count == 0 {
                print(string)
                if textField == otpTextField2 {
                    otpTextField1.becomeFirstResponder()
                }
                if textField == otpTextField3 {
                    otpTextField2.becomeFirstResponder()
                }
                if textField == otpTextField4 {
                    otpTextField3.becomeFirstResponder()
                }
                if textField == otpTextField1 {
                    otpTextField1.resignFirstResponder()
                }
                textField.text = ""
                return false                
            }
            return false
        }
    }
    
 /*
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
 */
}


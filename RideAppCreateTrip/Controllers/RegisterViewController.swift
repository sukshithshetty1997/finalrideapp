//
//  ViewController.swift
//  Rideapp
//
//  Created by Velan Salis on 28/02/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit
import KeychainSwift

class RegisterViewController: UIViewController {
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let navigationControllerObject = CustomNavigationViewController()
      let authIntractor = AuthenticationInteractor()
    @IBOutlet weak var nameTextField: NativeTextField!
    @IBOutlet weak var phoneTextField: NativeTextField!
    @IBOutlet weak var emailTextField: NativeTextField!
    @IBOutlet weak var passwordTextField: NativeTextField!
    let auth = AuthenticationInteractor()
    let button = UIButton(type: .custom)
    let validation = Validation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationControllerObject.showNavigationBar(controller: self)
        //navigationControllerObject.navigationBarDesign(controller: self, title: "Register")
        //navigationControllerObject.navigationBackButtonDesign(controller: self)
        navigationControllerObject.navigationBarDesign(controller: self, title: "Register")
        
//        let backbutton = UIBarButtonItem(image: UIImage(named: "app bar"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(back))
//        self.navigationItem.leftBarButtonItem = backbutton
//        self.navigationItem.title = "Register"
//        self.navigationController?.navigationBar.tintColor = UIColor.black
//        self.navigationController?.navigationBar.barTintColor = UIColor.orange
        // This Code will set Eye icon for password field
        button.setImage(UIImage(named: "eye(1).png"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: -24, bottom: 5, right: 15)
        button.frame = CGRect(x: CGFloat(passwordTextField.frame.size.width - 25), y: CGFloat(5), width: CGFloat(15), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.btnPasswordVisiblityClicked), for: .touchUpInside)
        passwordTextField.rightView = button
        passwordTextField.rightViewMode = .always
    }
    
    @objc func transitionBackTo() {
    //    let det = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
       // self.navigationController?.present(det, animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
         dismissDet()
    }
    
    @IBAction func btnPasswordVisiblityClicked(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        if (sender as! UIButton).isSelected {
            self.passwordTextField.isSecureTextEntry = false
        } else {
            self.passwordTextField.isSecureTextEntry = true
        }
    }

    @IBAction func registerButtonPressed(_ sender: UIButton) {
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePageViewController") as! ProfilePageViewController
//        self.navigationController?.pushViewController(dest, animated: true)
        guard
            let name = nameTextField.text,
            let email = emailTextField.text,
            let password = passwordTextField.text,
            let phone = phoneTextField.text
        else {
                return
        }
        
        let isValidateName = self.validation.validateName(name: name)
        if (isValidateName == false) {
            print("Incorrect Name")
            return
        }
        let isValidateEmail = self.validation.validateEmailId(emailID: email)
        if (isValidateEmail == false){
            print("Incorrect Email")
            return
        }
//        let isValidatePass = self.validation.validatePassword(password: password)
//        if (isValidatePass == false) {
//            print("Incorrect Pass")
//            return
//        }
        let isValidatePhone = self.validation.validaPhoneNumber(phoneNumber: phone)
        if (isValidatePhone == false) {
            print("Incorrect Phone")
            return
        }
        if (isValidateName == true && isValidateEmail == true  && isValidatePhone == true){
            let response = auth.registerUser(requestBody: ["name": name, "email": email, "password": password, "phone": phone])
            guard let data = response.data?.otpVerificationID else{
                if let message = response.message, let errorName = response.name{
                    let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    present(alert, animated: true, completion: nil)
                }
                return
            }
            UserDefaults.standard.set(name, forKey: "Name")
            UserDefaults.standard.set(email, forKey: "Email")
            UserDefaults.standard.set(phone, forKey: "Phone")
          //let dest = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePageViewController") as! ProfilePageViewController
          //  self.navigationController?.pushViewController(dest, animated: true)
       saveObject(fileName: "VerifyOTP", object: data)
       let dest  = self.storyboard?.instantiateViewController(withIdentifier: "OTPLoginNavigate") as! UINavigationController
        self.presentDet(dest)

        }
        
    }
    
}

//
//  ProfileViewController.swift
//  RideAppCreateTrip
//
//  Created by Royston Vishal  on 13/07/20.
//  Copyright © 2020 Royston Vishal. All rights reserved.
//

import UIKit
import KeychainSwift

class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let auth = RiderInteractor()
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mottoLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var tripCoreData = [TripData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let vc = JoinTripRecordsViewController()
//        tripCoreData = vc.selectedName
        profileImage.layer.borderColor = UIColor(rgb: 0xF7B557).cgColor
        profileImage.layer.borderWidth = 5
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.layer.masksToBounds = true
        let image = getObject(fileName: "Profile.png") as! UIImage
        profileImage.image = image
        let token = keychain.get(Keys.accessToken)
        print(token!)
        let response = auth.getProfile(requestHeaders: ["Authorization" : "Bearer \(token!)"])
        guard let name = response.data?.name
            else{
                print("No Data Present")
                return
        }
        nameLabel.text = name
       
    }
       /*let numberOfCompleted: Int = {
        var max: Int = 0
        while let _ = TripStatus(rawValue: "Completed") { max += 1 }
        return max
    }()*/
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataStorageInCoreData()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    func dataStorageInCoreData() {
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //collectionView.deselectItem(at: indexPath, animated: true)
       // print(numberOfCompleted)
        print("You Tapped me")
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tripCoreData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Activity", for: indexPath) as! ActivityCollectionViewCell
        cell.tripNameLabel.text = tripCoreData[indexPath.item].name
        cell.dateLabel.text = tripCoreData[indexPath.item].startTime
       // cell.imageView.image = tripArray[indexPath.item].BackgroundImage
        
        //cell.configure(with: UIImage(named: "GOA")!)
        return cell
        
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

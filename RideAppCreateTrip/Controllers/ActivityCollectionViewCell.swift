//
//  ActivityCollectionViewCell.swift
//  RideAppCreateTrip
//
//  Created by Royston Vishal  on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class ActivityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tripNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}

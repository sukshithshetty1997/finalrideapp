//
//  ProfilePageViewController.swift
//  rideapp2
//
//  Created by Sukshith Shetty on 03/03/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift

class ProfilePageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let navigationControllerObject = CustomNavigationViewController()
    let auth = RiderInteractor()
    @IBOutlet weak var transparentview: UIView!
    let image = UIImagePickerController()
    //var transparentView = UIView()
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var MainView: UIStackView!
    @IBOutlet weak internal var label: UILabel!
    
    override internal func viewDidLoad() {
        super.viewDidLoad()
        image.delegate = self
        transparentview.isHidden = true
        transparentview.isUserInteractionEnabled = false
        navigationControllerObject.navigationBackButtonDesign(controller: self)
        let name = UserDefaults.standard.object(forKey: "Name") as? String ?? ""
        //Button.layer.shadowColor = UIColor.black.cgColor
        //Button.layer.shadowOffset = CGSize(width: 2.5, height: 2.5)
        //Button.layer.shadowRadius = 5
        //Button.layer.shadowOpacity = 0.5
            MainView.isHidden = true
            label.isHidden = false
        label.text = "Hey "  + name
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func SkipToTabBar(_ sender: Any) {
        let image:UIImage = #imageLiteral(resourceName: "user(3)")
//        let RawDataImage = image.jpegData(compressionQuality: 0.8)
//        let token = keychain.get(Keys.AccessToken)
//      //  let response = auth.AddProfileImage(requestBody: ["avatar" : RawDataImage], requestHeaders: ["Authorization" : "Bearer \(token!)"])
//        guard let data = response.data?.message else{
//            if let message = response.message, let errorName = response.name{
//                let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                present(alert, animated: true, completion: nil)
//            }
//            return
//        }
        saveObject(fileName: "Profile.png", object: image)
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! MainTabBar
        self.presentDet(dest)
//        if let savedImage = try? NSKeyedArchiver.archivedData(withRootObject: UIImage.init(), requiringSecureCoding: false){
//            UserDefaults.standard.set(savedImage, forKey: "Image")}
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
//        self.navigationController?.present(dest, animated: true, completion: nil)
    }
    
    @objc func transitionBackTo() {
      //let dest = self.storyboard?.instantiateViewController(withIdentifier: "RegisterNavigate") as! UINavigationController
      //  self.navigationController?.present(dest, animated: true, completion: nil)
        //self.navigationController?.popViewController(animated: true)
           dismissDet()
    }
    
//    @IBAction func DismissBack(_ sender: Any) {
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "RegisterNavigate") as! UINavigationController
//       self.navigationController?.present(dest, animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
//    }
    @IBAction func Profile1(_ sender: Any) {
        MainView.isHidden = false
        transparentview.isHidden = false
        transparentview.isUserInteractionEnabled = true
        transparentview.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        //let window = UIApplication.shared.keyWindow
        //transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        //transparentView.frame = self.view.frame
       // window?.addSubview(transparentView)
        //transparentView.alpha = 0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTransparentView))
        transparentview.addGestureRecognizer(tapGesture)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentview.alpha = 0.5}, completion: nil)
}
    
    @IBAction func Gallery(_ sender: Any) {
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = true
        //UIImagePickerController.SourceType.photoLibrary
        //self.present(image, animated: true)
        self.present(image, animated: true, completion: nil)
        onClickTransparentView()
 
    }
    
    @objc func onClickTransparentView() {
        MainView.isHidden = true
        transparentview.isHidden = true
        transparentview.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentview.alpha = 0
        }, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let Destination = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            //let chooseimage = image
            //dismiss(animated: true, completion: nil)
            //if let image = info[UIImagePickerController.InfoKey.originalImage]
//
           // let RawDataImage = image.jpegData(compressionQuality: 0.8)! as NSData
//            print("Edited Image")
//            let savePath = UUID().uuidString
//
      //      let path = saveObject(fileName: "\(savePath).png", object: image)
          //      print(path)
//            let randome = UUID().uuidString
//            let RawDataImage = image.pngData()
//            let datam = RawDataImage?.base64EncodedString()
//            let token = keychain.get(Keys.AccessToken)
//            let savedImage = saveObject(fileName: randome, object: image)
//            do{
//             //   let org = try String(contentsOfFile: savedImage as! String, encoding: String.Encoding.utf8)
//              //  print(org)
//                let saveImage = try NSKeyedArchiver.archivedData(withRootObject: RawDataImage!, requiringSecureCoding: false)
//                print(saveImage, "SavedDae")
//                let strnData =  String(data: savedImage as! Data, encoding: String.Encoding.utf8)
//                print(strnData!)
//                let response = auth.AddProfileImage(requestBody: ["avatar" : strnData!], requestHeaders: ["Authorization" : "Bearer \(token!)"])
//                guard let data = response.data?.message else{
//                    print(response.message!, response.name!)
//                    return
//                }
//                print(data)
//                print("Successful")
//
//            }
//            catch{
//                print("error occured")
//                debugPrint(error.localizedDescription)
//            }
        
    
        
//            let jpegData = image.jpegData(compressionQuality: 0.8)
//
//            saveObject(fileName: "Profile.png", object: image)
//            print(data, "Profile Image Saved Successfully")
//            if let savedImage = try? NSKeyedArchiver.archivedData(withRootObject: image, requiringSecureCoding: false){
//              UserDefaults.standard.set(savedImage, forKey: "Image")}
            saveObject(fileName: "Profile.png", object: image)
            Destination.imageView = image 
            self.navigationController?.pushViewController(Destination, animated: true)
            //self.dismiss(animated: true, completion: nil)
            //(info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil
            //            self.dismiss(animated: true, completion: nil)
        }
        
            
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
//            let RawDataImage: NSData = image.jpegData(compressionQuality: 0.8)! as NSData
//            let token = keychain.get(Keys.AccessToken)
//            let response = auth.AddProfileImage(requestBody: ["avatar" : RawDataImage], requestHeaders: ["Authorization" : "Bearer \(token!)"])
//            guard let data = response.data?.message else{
//                if let message = response.message, let errorName = response.name{
//                    let alert = UIAlertController(title: errorName, message: message, preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    present(alert, animated: true, completion: nil)
//                }
//                return
//            }
//            print("Original")
//            let RawDataImage = image.pngData()
//           let RawDataImageSet = RawDataImage?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
//            let token = keychain.get(Keys.AccessToken)
//            let response = auth.AddProfileImage(requestBody: ["avatar" : RawDataImageSet], requestHeaders: ["Authorization" : "Bearer \(token!)"])
//            guard let data = response.data?.message else{
//                print(response.message!, response.name!)
//                return
//            }
           // print(data, "Profile Image Saved Successfully")
         saveObject(fileName: "Profile.png", object: image)
            Destination.imageView = image
            self.navigationController?.pushViewController(Destination, animated: true)
            //            self.dismiss(animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    /* @IBAction func Gallery(_ sender: Any) {
        let imagePick = UIImagePickerController()
        imagePick.delegate = self
        imagePick.sourceType = .photoLibrary
        self.present(imagePick, animated: true, completion: nil)
    }
    
     
    @IBAction func camera(_ sender: Any) {
        let imagePick = UIImagePickerController()
        imagePick.delegate = self
        //imagePick.sourceType = .camera
        //self.present(imagePick, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //picture.isHidden = false
        start.isHidden = false
        //picture.image = image
        picker.dismiss(animated: true, completion: nil)
        
    }*/


}


//
//  SearchRecordsViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 01/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift
class ServiceTypeCell: UITableViewCell {}
class VehicleCell: UITableViewCell {}

class SearchRecordsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate{
    var keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let auth = ServiceInteractor()
    let riderAuth = RiderInteractor()
     // var ServiceRecords = [ComponentClass]()
    let minDate = Calendar.current.component(.day, from: Date())
    let minMonth = Calendar.current.component(.month, from: Date()) - 1
    let minYear = Calendar.current.component(.year, from: Date())
    
    let navigationControllerObject = CustomNavigationViewController()
    var VehicleTypeServiceRecords = [ComponentClass]()
    var ServiceTypeRecords = [ComponentClass]()
    var FinalRecords = [ComponentClass]()
    
    var Month:[String] = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
    var TestServiceRecords: [ComponentClass] = [ ComponentClass(MobileNumber: "8762908477", VehicleNumber: "Kl-7", VehicleType: "Royal Enfield Classic 350", ServiceType: "Free Service", SlotData: "16 June, 2019", Time: "9:00 am", Dealer: "One", City: "One", Comments: "One"), ComponentClass(MobileNumber: "8762", VehicleNumber: "kl-08", VehicleType: "Royal Enfield Himalayan", ServiceType: "General Service", SlotData: "26 June, 2020", Time: "9:00 am", Dealer: "two", City: "two", Comments: "two"), ComponentClass(MobileNumber: "879", VehicleNumber: "kl-06", VehicleType: "Royal Enfield Continental GT 650", ServiceType: "General Service", SlotData: "05 June, 2021", Time: "10:00 am", Dealer: "three", City: "three", Comments: "three"), ComponentClass(MobileNumber: "9631", VehicleNumber: "kl-11", VehicleType: "Royal Enfield Meteor 350",ServiceType: "Breakdown Assistance", SlotData: "25 June, 2022", Time: "11:00 am", Dealer: "for", City: "for", Comments: "for")]
    
    var CaseFlags:[Bool] = [false,false]
    var  defaults = UserDefaults.standard
    var RecordsView : [GetServiceResponse.Data] = []
    @IBOutlet var VehicleTypeLabel: UILabel!
    @IBOutlet var ServiceTypeLabel: UILabel!
    @IBOutlet var VehicleTypeTextField: UITextField!
    @IBOutlet var ServiceTypeTextfield: UITextField!
    @IBOutlet var Records: UICollectionView!
    var ServiceTableView = UITableView()
    var VehicleTableView = UITableView()
    let btn = UIButton()
    let btn2 = UIButton()
    var flag1 = false, flag2 = false, Scopeflag = 0
    var searching = false
    
    let ArrayVehicleType:[String] = ["Royal Enfield Classic 350","Royal Enfield Himalayan", "Royal Enfield Interceptor 650", "Royal Enfield Continental GT 650", "Royal Enfield Meteor 350"]
    let Controller = BookServiceViewController()
    var ArrayService = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RecordSpace()
        navigationControllerObject.navigationBarDesign(controller: self, title: "Service Records")
        let token = keychain.get(Keys.accessToken)
        let riderResponse = riderAuth.getProfile(requestHeaders: ["Authorization": "Bearer \(token!)"])
        guard let riderID = riderResponse.data?.services else {
            print(riderResponse.message!,  riderResponse.name!)
            return
        }
        let response = auth.getService(serviceID: riderID[0], requestHeaders: [ "Authorization": "Bearer \(token!)"])
        guard let data = response.data else {
            print("Error Occured")
            //print(response.message!, response.name!)
            return
        }
        print(data)
        self.RecordsView = [data]
        print(RecordsView)
        print(RecordsView[0].comments!)
        //self.tabBarController?.tabBar.isHidden = true
    }
    func RecordSpace() {
        let screenwidth = UIScreen.main.bounds.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 5, bottom: 20, right: 5)
        layout.itemSize = CGSize(width: 310, height: 100)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        Records.collectionViewLayout = layout
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    
        ServiceTableView.separatorStyle = .none
        VehicleTableView.separatorStyle = .none
        
        ArrayService = Controller.tablecell
        
        btn.frame = CGRect(x: 0, y: 0, width: ServiceTypeTextfield.frame.height * 0.7, height: ServiceTypeTextfield.frame.height * 0.7)
        btn2.frame = CGRect(x: 0, y: 0, width: VehicleTypeTextField.frame.height * 0.7, height: VehicleTypeTextField.frame.height * 0.7)

        btn.setImage(UIImage(named: "down"), for: .normal)
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        btn.addTarget(self, action: #selector(DropDownList), for: UIControl.Event.touchDown)
        ServiceTypeTextfield.rightViewMode = .always
        ServiceTypeTextfield.rightView = btn
        
        btn2.setImage(UIImage(named: "down"), for: .normal)
        btn2.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        btn2.addTarget(self, action: #selector(DropDownList(_:)), for: UIControl.Event.touchDown)
        VehicleTypeTextField.rightViewMode = .always
        VehicleTypeTextField.rightView = btn2
        
        VehicleTypeLabel.isHidden = true
        ServiceTypeLabel.isHidden = true
        
        Records.delegate = self
        Records.dataSource = self
        Records.layer.shadowRadius = 10
        Records.layer.shadowOpacity = 1
        Records.layer.shadowColor = UIColor.darkGray.cgColor
        Records.layer.shadowOffset = .zero
        VehicleTypeTextField.addbottom()
        ServiceTypeTextfield.addbottom()
        
        ServiceTableView.delegate = self
        ServiceTableView.dataSource = self
        ServiceTableView.register(ServiceCell.self, forCellReuseIdentifier: "cell")
        
        
        VehicleTableView.delegate = self
        VehicleTableView.dataSource = self
        VehicleTableView.register(VehicleCell.self, forCellReuseIdentifier: "cell")
        
        
        self.btn.tag = 1
        self.btn2.tag = 2
        ServiceTypeTextfield.tag = 1
        VehicleTypeTextField.tag = 2
        
        ServiceTableView.frame = CGRect(x: ServiceTypeTextfield.frame.origin.x, y: ServiceTypeTextfield.frame.origin.y + ServiceTypeTextfield.frame.height + 10, width: ServiceTypeTextfield.frame.width, height: 0)
        VehicleTableView.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
        
       
        ServiceTableView.addShadowAndBorder()
        self.view.addSubview(ServiceTableView)
        
        VehicleTableView.addShadowAndBorder()
        self.view.addSubview(VehicleTableView)
        
        ServiceTypeTextfield.inputView = UIView()
        VehicleTypeTextField.inputView = UIView()
        let gesture = UITapGestureRecognizer()
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
        
        VehicleTypeTextField.addTarget(self, action: #selector(DropDownList(_:)), for: UIControl.Event.allTouchEvents)
        ServiceTypeTextfield.addTarget(self, action: #selector(DropDownList(_:)), for: UIControl.Event.allTouchEvents)
        //        if let SavedGarageServiceRecord = defaults.object(forKey: "DetailsData") as? Data {
        //            if let DecodeGarageService = try?
        //                NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(SavedGarageServiceRecord) as? [ComponentClass] {
        //                ServiceRecords = DecodeGarageService!
        //            }
        //        }

    }
    
    @objc func transitionBackTo(){
//        let popVc = storyboard?.instantiateViewController(withIdentifier: "MainGarage") as! UINavigationController
//        self.present(popVc, animated: true, completion: nil)
          dismissDet()
    }
    
    func VehicleTextChange(_ textfield: UITextField){
        FinalRecords = FinalRecords.filter{!VehicleTypeServiceRecords.contains($0)}
        VehicleTypeLabel.isHidden = false
        textfield.placeholder = ""
        if (textfield.text?.count)! != 0 && (textfield.text?.trimmingCharacters(in: .whitespacesAndNewlines).count) != 0{
            VehicleTypeServiceRecords.removeAll()
            searching = true
            for Record in TestServiceRecords{
                if let SearchText = textfield.text?.trimmingCharacters(in: .whitespaces){
                    let range = Record.VehicleType.lowercased().range(of: SearchText, options: String.CompareOptions.caseInsensitive, range: nil, locale: nil)
                    if range != nil {
                        CaseFlags[0] = true
                        VehicleTypeServiceRecords.append(Record)
                        FinalRecordCall()
                    }
                    else{
                        searching = false
                        Records.reloadData()
                    }}}}
    }
    
    
    func ServiceTextChange(_ textfield: UITextField){
        FinalRecords = FinalRecords.filter{!ServiceTypeRecords.contains($0)}
        ServiceTypeLabel.isHidden = false
        textfield.placeholder = ""
        if (textfield.text?.count)! != 0 && (textfield.text?.trimmingCharacters(in: .whitespacesAndNewlines).count) != 0{
            ServiceTypeRecords.removeAll()
            searching = true
            for Record in TestServiceRecords{
                if let SearchText = textfield.text?.trimmingCharacters(in: .whitespaces){
                    let range = Record.ServiceType.lowercased().range(of: SearchText, options: String.CompareOptions.caseInsensitive, range: nil, locale: nil)
                    if range != nil {
                        CaseFlags[1] = true
                        ServiceTypeRecords.append(Record)
                        FinalRecordCall()
                    }
                    else{
                        searching = false
                        Records.reloadData()
                    }}}}
        
    }
    func FinalRecordCall(){
        searching = true
        switch  CaseFlags{
        case [true,false]:
            FinalRecords = VehicleTypeServiceRecords
            Records.reloadData()
        case [false, true]:
            FinalRecords = ServiceTypeRecords
            Records.reloadData()
            
        case [true,true]:
            for RecordVehicle in VehicleTypeServiceRecords{
                for RecordService in ServiceTypeRecords{
                    if RecordVehicle.VehicleType.lowercased() == RecordService.VehicleType.lowercased(){
                        if !FinalRecords.contains(RecordService){
                            FinalRecords.append(RecordService)
                        }
                    }
                }
            }
            Records.reloadData()
            
        default:
            searching = false
            
        }
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if ( (touch.view?.isDescendant(of: ServiceTableView))! || (touch.view?.isDescendant(of: VehicleTableView))! || (touch.view?.isDescendant(of: Records))! || (touch.view?.isDescendant(of: btn))! || (touch.view?.isDescendant(of: btn2))! || (touch.view?.isDescendant(of: VehicleTypeTextField))! || (touch.view?.isDescendant(of: ServiceTypeTextfield))!){return false}
        view.endEditing(true)
        ServiceTableView.frame = CGRect(x: ServiceTypeTextfield.frame.origin.x, y: ServiceTypeTextfield.frame.origin.y + ServiceTypeTextfield.frame.height + 10, width: ServiceTypeTextfield.frame.width, height: 0)
        VehicleTableView.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
        if flag1{flag1 = !flag1}
        if flag2{flag2 = !flag2}
        return true
    }
    @objc func DropDownList(_ sender: UITextField){
        if sender.tag == 1{
            flag1 = !flag1
            if flag1{
            ServiceTableView.frame = CGRect(x: ServiceTypeTextfield.frame.origin.x, y: ServiceTypeTextfield.frame.origin.y + ServiceTypeTextfield.frame.height + 10, width: ServiceTypeTextfield.frame.width, height: CGFloat(ArrayService.count * 45))
        }
            else{
                   ServiceTableView.frame = CGRect(x: ServiceTypeTextfield.frame.origin.x, y: ServiceTypeTextfield.frame.origin.y + ServiceTypeTextfield.frame.height + 10, width: ServiceTypeTextfield.frame.width, height: 0)
            }
        }
        else if sender.tag == 2{
            flag2 = !flag2
            if flag2{
                   VehicleTableView.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: CGFloat(ArrayVehicleType.count * 45))
        }
            else{
                
                 VehicleTableView.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
            }
        }
    
    }
    func TestCases() -> Bool{
        if CaseFlags.contains(true){
            return true
        }
        else{
            return false
        }
    }

    @IBAction func VehicleTypeBegin(_ sender: UITextField) {
        flag2 = !flag2
        self.VehicleTypeLabel.isHidden = false
        VehicleTypeTextField.placeholder = ""
        if flag2{
            VehicleTableView.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: CGFloat(ArrayVehicleType.count * 45))}
    }
    
//    @IBAction func VehicleTypeChange(_ sender: UITextField) {
//    }
//
//
//    @IBAction func ServiceTypeBegin(_ sender: UITextField) {
//        flag1 = !flag1
//        UILabel.animate(withDuration: 10, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseInOut, animations: {
//            self.ServiceTypeLabel.isHidden = false
//        }, completion: nil)
//        ServiceTypeTextfield.placeholder = ""
//        if flag1 {
//            ServiceTableView.frame = CGRect(x: ServiceTypeTextfield.frame.origin.x, y: ServiceTypeTextfield.frame.origin.y + ServiceTypeTextfield.frame.height + 100, width: ServiceTypeTextfield.frame.width, height: CGFloat(ArrayService.count * 60))}
//    }
//
//    @IBAction func ServiceTypeChange(_ sender: UITextField) {
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch TestCases() {
        case true :
            return FinalRecords.count
        default:
            return TestServiceRecords.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SearchRecordsCollectionViewCell
        cell.layer.cornerRadius = 5
        switch TestCases() {
        case true:
            Scopeflag = 0
            var SlotDate =  FinalRecords[indexPath.row].SlotDate
            SlotDate = SlotDate.components(separatedBy: .punctuationCharacters).joined()
            let DateArray:[String] = SlotDate.components(separatedBy: .whitespacesAndNewlines)
            let month = Month.firstIndex(of: DateArray[1])!
            cell.DateLabel.text = DateArray[0]
            
            cell.MonthLabel.text = DateArray[1]
            cell.Year.text = DateArray[2]
            
            cell.ServiceTypelabel.text = FinalRecords[indexPath.row].ServiceType
            
            
            if minYear >= Int(DateArray[2])! {
                if minMonth >=  month{
                    if minDate > Int(DateArray[0])!{
                        Scopeflag = 1
                    }}}
            
            if Scopeflag == 1{
                cell.Label1.backgroundColor = UIColor(rgb: 0x33CC66)
                cell.Label1.text = "Past"
                cell.DateLabel.textColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
                cell.MonthLabel.textColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
               cell.Year.textColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
            }
            else{
                cell.Label1.backgroundColor = UIColor.orange
                cell.Label1.text = "New"
                cell.DateLabel.textColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
                cell.MonthLabel.textColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
                cell.Year.textColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
            }
            
        default:
            Scopeflag = 0
            var SlotDate =  TestServiceRecords[indexPath.row].SlotDate
            SlotDate = SlotDate.components(separatedBy: .punctuationCharacters).joined()
            let DateArray:[String] = SlotDate.components(separatedBy: .whitespacesAndNewlines)
            let month = Month.firstIndex(of: DateArray[1])!
            cell.DateLabel.text = DateArray[0]
            cell.MonthLabel.text = DateArray[1]
            cell.Year.text = DateArray[2]
            cell.ServiceTypelabel.text = TestServiceRecords[indexPath.row].ServiceType
            
            switch minYear == Int(DateArray[2])!  {
            case true:
                switch minMonth == month {
                case true:
                    switch minDate >=  Int(DateArray[0])!{
                    case true:
                        Scopeflag = 1
                        break
                    case false:
                        Scopeflag = 0
                        break
                    }
                case false:
                    switch minMonth > month {
                    case true:
                        Scopeflag = 1
                        break
                    case false:
                        Scopeflag = 0
                        break
                    }
                }
            case false:
                switch minYear > Int(DateArray[2])! {
                case true:
                    Scopeflag = 1
                    break
                case false:
                    Scopeflag = 0
                    break
                }
            }
            
            if Scopeflag == 1{
                cell.Label1.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
                cell.Label1.text = "Past"
                cell.DateLabel.textColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
                cell.MonthLabel.textColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
                cell.Year.textColor = #colorLiteral(red: 0.1098039216, green: 0.7019607843, blue: 0.568627451, alpha: 1)
            }
            else{
                cell.Label1.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
                cell.Label1.text = "New"
                cell.DateLabel.textColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
                cell.MonthLabel.textColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
                cell.Year.textColor = #colorLiteral(red: 0.9294117647, green: 0.4980392157, blue: 0.1725490196, alpha: 1)
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Booking", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Booking") as! CustomNavigationViewController
        switch TestCases() {
        case true:
            ComponentClass.Data.PassArrayDetails = [FinalRecords[indexPath.row]]
        default:
            ComponentClass.Data.PassArrayDetails = [TestServiceRecords[indexPath.row]]
        }
        self.presentDet(vc)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ServiceTableView {
            return ArrayService.count
        }
        else{
            return ArrayVehicleType.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell = .init()
        if tableView == ServiceTableView{
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ServiceCell
            cell.textLabel?.text = ArrayService[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
            cell.textLabel?.textColor = UIColor(red: 79, green: 80, blue: 79)
        }
        else if tableView == VehicleTableView{
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VehicleCell
            cell.textLabel?.text = ArrayVehicleType[indexPath.row]
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
            cell.textLabel?.textColor = UIColor(red: 79, green: 80, blue: 79)
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        if tableView == ServiceTableView{
            ServiceTypeTextfield.text = cell.textLabel?.text
            ServiceTableView.frame = CGRect(x: ServiceTypeTextfield.frame.origin.x, y: ServiceTypeTextfield.frame.origin.y + ServiceTypeTextfield.frame.height + 10, width: ServiceTypeTextfield.frame.width, height: 0)
            ServiceTextChange(ServiceTypeTextfield)
            flag1 = !flag1
        }
        else if tableView == VehicleTableView{
            VehicleTypeTextField.text = cell.textLabel?.text
             VehicleTableView.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
            VehicleTextChange(VehicleTypeTextField)
            flag2 = !flag2
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//extension UITableView{
//    func addShadowAndBorder(){
//        self.layer.cornerRadius = 5
//        self.layer.shadowColor = UIColor.lightGray.cgColor
//        self.layer.shadowRadius = 5
//        self.layer.shadowOpacity = 0.75
//        self.layer.shadowOffset = .zero
//        self.layer.borderColor = UIColor.lightGray.cgColor
//        self.layer.borderWidth = 1
//}
//}

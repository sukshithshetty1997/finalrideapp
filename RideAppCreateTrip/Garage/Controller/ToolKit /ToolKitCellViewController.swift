//
//  ToolKitCellViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 03/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class ToolKitCellViewController: UIViewController {
    @IBOutlet var ToolDetailsView: UITextView!
    @IBOutlet var InnerView: UIView!
    @IBOutlet var TextField: UITextView!
    @IBOutlet var LabelText: UILabel!
    @IBOutlet var Close: UIButton!
    var LabelString:String = ""
    var TextString:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LabelText.text = LabelString
        self.InnerView.layer.cornerRadius = 10
        ToolDetailsView.isEditable = false

    }
    
    @IBAction func DismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

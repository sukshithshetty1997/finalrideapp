//
//  ToolKitViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 03/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
class ToolKit: UICollectionViewCell {
    
    @IBOutlet var ToolKitLabels: UILabel!
    @IBOutlet var ToolKitImage: UIImageView!
    @IBOutlet var Viewone: UIView!

}

class ToolKitViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    let navigationControllerObject = CustomNavigationViewController()
    var cell:ToolKit = ToolKit()
    @IBOutlet var ToolKitCollectionView: UICollectionView!
    @IBOutlet var Label: UILabel!
    @IBOutlet var TroubleTextField: UITextField!
    let search = UIButton()
    let List:[String] = ["Onsite Minor Repairs", "Vehicle Accident","Battery Drain", "Breakdown", "Loose Chain"]
    let Images:[UIImage] = [UIImage(named: "Break")!, UIImage(named: "Accident")!, UIImage(named: "Battery")!, UIImage(named: "Break")!, UIImage(named: "Chain")!]

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.backButtonLeftItem(self)
        ToolKitCollectionView.delegate = self
        ToolKitCollectionView.dataSource = self
        
        Label.isHidden = true
        TroubleTextField.addbottom()
        search.setImage(UIImage(named: "search"), for: .normal)
        search.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        TroubleTextField.rightViewMode = .always
        TroubleTextField.rightView = search
        search.frame = CGRect(x: 0, y: 5, width: TroubleTextField.frame.height * 0.65, height: TroubleTextField.frame.height * 0.5)
        TroubleTextField.addTarget(self, action: #selector(LabelUp), for: UIControl.Event.editingDidBegin)
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let screenwidth = UIScreen.main.bounds.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 40, bottom: 20, right: 40)
        layout.itemSize = CGSize(width: screenwidth - 80, height: 54)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        ToolKitCollectionView.collectionViewLayout = layout
        navigationControllerObject.navigationBarDesign(controller: self, title: "Tool Kit")
    }

    @objc func LabelUp(){
        TroubleTextField.placeholder = ""
        Label.isHidden = false
    }
    @objc func transitionBackTo(){
         self.dismissDet()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ToolKit
        cell.addCollectionViewCellShadow()
        cell.ToolKitLabels.text = List[indexPath.row]
        cell.ToolKitImage.image = Images[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "ToolKitCellViewController") as! ToolKitCellViewController
        dest.LabelString = List[indexPath.row]
        self.navigationController?.present(dest, animated: true, completion: nil)
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}


//
//  BookServiceViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 04/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
class ServiceCell: UITableViewCell{}
class BookServiceViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate{
    

    let navigationControllerObject = CustomNavigationViewController()
    var PassObject:[String:Any] = [:]
    let ServiceTypeTableView = UITableView()
    var tablecell:[String] = ["Free Service", "General Service", "Breakdown Assistance"]
    @IBOutlet var navigate: UINavigationItem!
    @IBOutlet var UITextView: UITextView!
    @IBOutlet var dealerButton: UIButton!
    @IBOutlet var VehicleType: UITextField!
    @IBOutlet var MobileNumber: UITextField!
    
    @IBOutlet var MobileNumLabel: UILabel!
    @IBOutlet var VehicleTypeLabel: UILabel!
    @IBOutlet var VehicleNumLabel: UILabel!
    @IBOutlet var ServiceTypeLabel: UILabel!

    var flag = false
    var checkIfFilled:[Bool] = [false, false, false, false]
    @IBOutlet var ServiceType: UITextField!
    @IBOutlet var VehicleNum: UITextField!
    let btn = UIButton()
    let btn2 = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ServiceTypeTableView.separatorStyle = .none
        
        MobileNumLabel.isHidden = true
        VehicleNumLabel.isHidden = true
        VehicleTypeLabel.isHidden = true
        ServiceTypeLabel.isHidden = true
        
        self.navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = true
        
        btn.frame = CGRect(x: 0, y: 0, width: ServiceType.frame.height * 0.7, height: ServiceType.frame.height * 0.7)
        btn2.frame = CGRect(x: 0, y: 0, width: MobileNumber.frame.height * 0.7, height: MobileNumber.frame.height * 0.7)
        
        self.MobileNumber.tag = 1
        self.VehicleType.tag  = 2
        
        dealerButton.isEnabled = false
        
        self.MobileNumber.delegate = self
        self.ServiceType.delegate = self
        self.VehicleNum.delegate = self
        self.VehicleType.delegate = self
        
        self.MobileNumber.addbottom()
        self.ServiceType.addbottom()
        self.VehicleType.addbottom()
        self.VehicleNum.addbottom()
        
       // dealerButton.layer.cornerRadius = dealerButton.frame.height/2
        dealerButton.layer.opacity = 0.5
        
        UITextView.layer.borderWidth = 1
        UITextView.layer.cornerRadius = 5
        UITextView.layer.borderColor = UIColor.gray.cgColor
 //       UITextView.layer.shadowRadius = 5
        
//        UITextView.layer.shadowColor = UIColor.gray.cgColor
//        UITextView.layer.shadowOffset = .zero
//        UITextView.layer.shadowOpacity = 1
//        UITextView.layer.masksToBounds = false
        
        ServiceTypeTableView.frame = CGRect(x: ServiceType.frame.origin.x, y: ServiceType.frame.origin.y , width: ServiceType.frame.width
            , height: 0)
        self.view.addSubview(ServiceTypeTableView)
        ServiceTypeTableView.layer.cornerRadius = 5
        
        let icon = UIImage(named: "icon")
        icon?.withRenderingMode(.alwaysTemplate)

        btn.setImage(UIImage(named: "down"), for: .normal)
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        btn.addTarget(self, action: #selector(DropDown), for: UIControl.Event.touchDown)
        ServiceType.rightViewMode = .always
        ServiceType.rightView = btn
        
        
        btn2.setImage(icon, for: .normal)
        btn2.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        btn.addTarget(self, action: #selector(Edit), for: UIControl.Event.touchDown)
        MobileNumber.rightViewMode = .always
        btn2.tintColor = UIColor(rgb: 0xA6A4A3)
        MobileNumber.rightView = btn2
        
        let tap = UITapGestureRecognizer()
        tap.delegate = self
        view.addGestureRecognizer(tap)
        ServiceTypeTableView.delegate = self
        ServiceTypeTableView.register(ServiceCell.self, forCellReuseIdentifier: "cell")
        ServiceTypeTableView.dataSource = self
        
        ServiceType.inputView = UIView()
    }
    
    @IBAction func MobileNum(_ sender: Any) {
        MobileNumLabel.isHidden = false
    }
    
    @IBAction func VehicleType(_ sender: Any) {
        VehicleTypeLabel.isHidden = false
        
    }
    @IBAction func VehicleNum(_ sender: Any) {
        VehicleNumLabel.isHidden = false
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == ServiceType{
            return false
        }
        else {
            return true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarDesign(controller: self, title: "Book a Service")
    }

    @IBAction func TouchDownServiceType(_ sender: Any) {
        flag = !flag
        ServiceTypeLabel.isHidden = false
        ServiceType.placeholder = ""

        if flag {
            // change
            self.ServiceTypeTableView.frame = CGRect(x: self.ServiceType.frame.origin.x, y: self.ServiceType.frame.origin.y + self.ServiceType.frame.height + 5
                , width: self.ServiceType.frame.width, height: CGFloat(self.tablecell.count * 60))
            btn.setImage(UIImage(named: "up"), for: .normal)
            ServiceType.rightView = btn
        }
        else {
            self.ServiceTypeTableView.frame = CGRect(x: self.ServiceType.frame.origin.x, y: self.ServiceType.frame.origin.y + self.ServiceType.frame.height + 5
                , width: self.ServiceType.frame.width, height: 0)
            btn.setImage(UIImage(named: "down"), for: .normal)
            ServiceType.rightView = btn
        }
    }
    
    @objc func transitionBackTo() {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func DropDown(){
        flag = !flag
        if flag {
            //change
            self.ServiceTypeTableView.frame = CGRect(x: self.ServiceType.frame.origin.x, y: self.ServiceType.frame.origin.y + self.ServiceType.frame.height + 5
                , width: self.ServiceType.frame.width, height: CGFloat(self.tablecell.count * 60))
            btn.setImage(UIImage(named: "up"), for: .normal)
            ServiceType.rightView = btn
        }
        else {
            self.ServiceTypeTableView.frame = CGRect(x: self.ServiceType.frame.origin.x, y: self.ServiceType.frame.origin.y + self.ServiceType.frame.height + 5
                , width: self.ServiceType.frame.width, height: 0)
            btn.setImage(UIImage(named: "down"), for: .normal)
            ServiceType.rightView = btn
        }
        
    }
    @objc func Edit(){
        print("Edit")
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if ((touch.view?.isDescendant(of: ServiceTypeTableView))!){
            return false
        }
        view.endEditing(true)
        //change
        self.ServiceTypeTableView.frame = CGRect(x: self.ServiceType.frame.origin.x, y: self.ServiceType.frame.origin.y + self.ServiceType.frame.height + 5, width: self.ServiceType.frame.width, height: 0)
        btn.setImage(UIImage(named: "down"), for: .normal)
        ServiceType.rightView = btn
        return true
    }
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func check() {
        if checkIfFilled.allSatisfy({$0 == true}){
            print(checkIfFilled)
            dealerButton.isEnabled = true
          //  dealerButton.layer.backgroundColor = UIColor(red: 237, green: 126, blue: 43).cgColor
            dealerButton.layer.opacity = 1
        }
        else{
            dealerButton.isEnabled = false
           // dealerButton.layer.backgroundColor = UIColor(red: 255, green: 202, blue: 161).cgColor
            dealerButton.layer.opacity = 0.5
            print(checkIfFilled)
            return}
    }
    
    @IBAction func MobileNumEndEdit(_ sender: Any) {
        guard  self.MobileNumber.text?.isEmpty ?? true
            else{
            checkIfFilled[0] = true
            check()
            return
        }
        checkIfFilled[0] = false
        check()
    }
    @IBAction func VehicleTypeEndEdit(_ sender: Any) {
        guard  self.VehicleType.text?.isEmpty ?? true
            else{
                checkIfFilled[1] = true
                check()
                return}
        checkIfFilled[1] = false
        check()
        }
    
    @IBAction func VehicleNumBeginEdit(_ sender: Any) {
        guard  self.VehicleNum.text?.isEmpty ?? true
            else{
                checkIfFilled[2] = true
                check()
            return
        }
        checkIfFilled[2] = false
        check()
    }
    
    @IBAction func Dealer(_ sender: Any) {
        PassObject = ["mobileNumber":MobileNumber.text!, "vehicleNumber":VehicleNum.text!, "serviceType":ServiceType.text!, "Comments": UITextView.text!, "vehicleType": VehicleType.text!]
        ComponentClass.Data.MobileNumber = MobileNumber.text!
        ComponentClass.Data.VehicleNumber = VehicleNum.text!
        ComponentClass.Data.ServiceType = ServiceType.text!
        ComponentClass.Data.Comments = UITextView.text!
        DealerCall()
    }
    }

extension UITextView{
        func addInnerShadow() {
        let innerShadow = CALayer()
        innerShadow.frame = self.bounds
        
        let radius = self.frame.size.height/2
        let path = UIBezierPath(roundedRect: innerShadow.bounds.insetBy(dx: -1, dy:-1), cornerRadius:radius)
        let cutout = UIBezierPath(roundedRect: innerShadow.bounds, cornerRadius:radius).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        // Shadow properties
        innerShadow.shadowColor = UIColor.black.cgColor
        innerShadow.shadowOffset = CGSize.zero
        innerShadow.shadowOpacity = 1
        innerShadow.shadowRadius = 3
        innerShadow.cornerRadius = self.frame.size.height/2
        self.layer.addSublayer(innerShadow)
        //layer.addSublayer(innerShadow)
    }
//    func drawShadow() {
//        
//        let size = self.frame.size
//        self.clipsToBounds = true
//        let layer: CALayer = CALayer()
//        layer.backgroundColor = UIColor.lightGray.cgColor
//        layer.position = CGPoint(x: size.width / 2, y: -size.height / 2 + 0.5)
//        layer.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
//        layer.shadowColor = UIColor.darkGray.cgColor
//        layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
//        layer.shadowOpacity = 0.8
//        layer.shadowRadius = 5.0
//        self.layer.addSublayer(layer)
//        
//    }
}
extension UITextField{
    func addbottom() {
        var bottomBorder = UIView()
        self.borderStyle = .none
        self.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        bottomBorder.backgroundColor = UIColor.gray
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(bottomBorder)
        
        bottomBorder.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        bottomBorder.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        bottomBorder.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    }


extension BookServiceViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tablecell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  tablecell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ServiceCell
        tablecell.textLabel?.text =  self.tablecell[indexPath.row]
        tablecell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
        tablecell.textLabel?.textColor = UIColor(red: 79, green: 80, blue: 79)
        tablecell.backgroundColor = UIColor.lightGray
        return  tablecell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        ServiceType.text = cell?.textLabel?.text
        checkIfFilled[3] = true
        check()
        self.ServiceTypeTableView.frame = CGRect(x: self.ServiceType.frame.origin.x, y: self.ServiceType.frame.origin.y + self.ServiceType.frame.height + 5, width: self.ServiceType.frame.width, height: 0)
        
        btn.setImage(UIImage(named: "down"), for: .normal)
        ServiceType.rightView = btn
        self.flag = false
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}
extension BookServiceViewController{
    func DealerCall(){
        let destVC = self.storyboard?.instantiateViewController(withIdentifier: "GarageDealerViewController") as! GarageDealerViewController
        destVC.assign(object: PassObject)
        self.navigationController?.pushViewController(destVC, animated: true)
        
    }
}

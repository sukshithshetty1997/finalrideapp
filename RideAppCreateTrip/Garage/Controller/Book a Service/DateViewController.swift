//
//  DateViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 29/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
protocol DismissDate {
    func dismissDate()
}

class DateViewController: UIViewController, DismissTime{
    var PassObj:[String:Any] = [:]
    @IBOutlet var year: UILabel!
    @IBOutlet var weekDayLabel: UILabel!
    @IBOutlet var DatePicker: UIDatePicker!
    var DismissDelegate: DismissDate?
    let date = Date()
    var CurrentDate:Int = .init()
    var Currentmonth:Int = .init()
    var Currentyear:Int = .init()
    var CurrentWeekDay:Int = .init()
    var minDate: Int = .init()
    var minMonth: Int = .init()
    var minYear: Int = .init()
    var minWeek: Int = .init()
    
    var Weeks:[String] = [ "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    var Month:[String] = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    func assign(object:[String:Any]){
        self.PassObj = object
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        DatePicker.datePickerMode = .date
        minDate = Calendar.current.component(.day, from: Date())
        minMonth = Calendar.current.component(.month, from: Date()) - 1
        minYear = Calendar.current.component(.year, from: Date())
        minWeek = Calendar.current.component(.weekday, from: Date()) - 1
        self.weekDayLabel.text = "\(Weeks[minWeek]), \(Month[minMonth]) \(minDate)"
        self.year.text = "\(minYear)"
        DatePicker.minimumDate = Date()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    @IBAction func valueChange(_ sender: Any) {

         CurrentDate = DatePicker.calendar.component(Calendar.Component.day, from: DatePicker.date)
         Currentmonth = DatePicker.calendar.component(Calendar.Component.month, from: DatePicker.date) - 1
         Currentyear = DatePicker.calendar.component(Calendar.Component.year, from: DatePicker.date)
         CurrentWeekDay = DatePicker.calendar.component(Calendar.Component.weekday, from: DatePicker.date) - 1
        weekDayLabel.text = "\(Weeks[CurrentWeekDay]), \(Month[Currentmonth]) \(CurrentDate)"
        self.year.text = "\(Currentyear)"
        
    }
    
    @IBAction func OkButton(_ sender: Any) {
        let pass = self.storyboard?.instantiateViewController(withIdentifier: "TimeViewController") as! TimeViewController
        pass.DismissDelegate = self
        CurrentDate = DatePicker.calendar.component(Calendar.Component.day, from: DatePicker.date)
        Currentmonth = DatePicker.calendar.component(Calendar.Component.month, from: DatePicker.date) - 1
        Currentyear = DatePicker.calendar.component(Calendar.Component.year, from: DatePicker.date)
        CurrentWeekDay = DatePicker.calendar.component(Calendar.Component.weekday, from: DatePicker.date) - 1
      //  ComponentClass.Data.SlotDate = "\(CurrentDate) \(Month[Currentmonth]), \(Currentyear)"
       // self.navigationController?.pushViewController(pass, animated: true)
        let saveDate = "\(CurrentDate)/\(Currentmonth)/\(Currentyear)"
        print(saveDate)
        let slotDate = "\(CurrentDate) \(Month[Currentmonth]), \(Currentyear)"
        PassObj.updateValue(saveDate, forKey: "SaveDate")
        PassObj.updateValue(slotDate, forKey: "slotDate")
        pass.assign(object: PassObj)
        presentingViewController?.presentSec(pass)
    }
    @IBAction func CancelButton(_ sender: UIBarButtonItem) {
              dismissDet()
     //  self.navigationController?.popViewController(animated: true)
    }
    
    func dismissTime() {
        self.DismissDelegate?.dismissDate()
    }

}

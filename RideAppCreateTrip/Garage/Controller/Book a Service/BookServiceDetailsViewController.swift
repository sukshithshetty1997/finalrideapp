//
//  BookServiceDetailsViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 29/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import CoreData
import KeychainSwift

class BookServiceDetailsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource{
    var PassObj:[String:Any] = [:]
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let navigationControllerObject = CustomNavigationViewController()
     let AppDelegate = UIApplication.shared.delegate as? AppDelegate
    let auth = ServiceInteractor()
    let dealerAuth = DealerInteractor()
    let collection:[String] = ["cell1","cell2","cell3","cell4","cell5","cell6","cell7","cell8"]

    var ArrayList:[Any] = []
    var defaults = UserDefaults.standard
    @IBOutlet var BookTableView: UITableView!
    @IBOutlet var Book: UIButton!
    func assign(object:[String:Any]){
        self.PassObj = object
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BookTableView.rowHeight = UITableView.automaticDimension
        BookTableView.estimatedRowHeight = 520
        Book.layer.cornerRadius = Book.frame.height/2
        BookTableView.delegate = self
        BookTableView.dataSource = self
        ArrayList = [PassObj["mobileNumber"]!, PassObj["vehicleNumber"]!, PassObj["serviceType"]!, PassObj["slotDate"]!, PassObj["time"]!, PassObj["dealer"]!, PassObj["city"]!, PassObj["Comments"]!]
        print(PassObj)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = true
        navigationControllerObject.navigationLeftRightDesign(controller: self, title: "Booking Details", rightImage: UIImage(named: "Edit")!)
        
    }
    
    @objc func PopToCreateTrip(){
        //let popVc = storyboard?.instantiateViewController(withIdentifier: "BookServiceViewController") as! BookServiceViewController
        //self.navigationController?.popToViewController(popVc, animated: true)
        dismissDet()
        
    }
    @objc func transitionBackTo() {
         dismissDet()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.collection[indexPath.item], for: indexPath)
        cell.detailTextLabel?.text = ArrayList[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item < 7 {
        return 60
        }
        else{
            return 80
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    @IBAction func Book(_ sender: UIButton) {
       // fetchData()
        let token = keychain.get(Keys.accessToken)
        let response = auth.addService(requestBody: ["phone" : PassObj["mobileNumber"]!, "slotDate": PassObj["SaveDate"]!, "slotTime": PassObj["SaveTime"]!, "vehicleNumber": PassObj["vehicleNumber"]!, "vehicleType": PassObj["vehicleType"]!, "serviceType": PassObj["serviceType"]!, "comments": PassObj["Comments"]!, "dealer": PassObj["id"]!], requestHeaders: ["Authorization": "Bearer \(token!)"])
        print(token!)
        
        guard let printData = response.data?.message else{
            print("error Occured")
            print(response.message!)
            print(response.name!)
            return
        }
        print(printData)
        print("Service is added Successfully")
        let destV = storyboard?.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
        self.navigationController?.pushViewController(destV, animated: true)
    }
    func savetoCoreData(){
        guard let context = AppDelegate?.persistentContainer.viewContext else {return}
        let managedContext:GarageServiceData = GarageServiceData.init(entity: NSEntityDescription.entity(forEntityName: "GarageServiceData", in: context)!, insertInto: context)
        managedContext.mobileNumber = PassObj["mobileNumber"]! as? String
        managedContext.vehicleNumber = PassObj["vehicleNumber"]! as? String
        managedContext.vehicleType = PassObj["vehicleType"]! as? String
        managedContext.serviceType = PassObj["serviceType"]! as? String
        managedContext.dealer = PassObj["dealer"]! as? String
        managedContext.city = PassObj["city"]! as? String
        managedContext.slotData = PassObj["slotDate"]! as? String
        managedContext.time = PassObj["time"]! as? String
        managedContext.comments = PassObj["Comments"]! as? String
        
        do {
            try context.save()
            print("Data has been saved")
        }
        catch{
            print(error.localizedDescription)
        }
    }
    
    func fetchData(){
        guard let context = AppDelegate?.persistentContainer.viewContext else {return}
        let request = NSFetchRequest<GarageServiceData>(entityName: "GarageServiceData")
        
        do{
            let changedData:[GarageServiceData] = try context.fetch(request)
            print("Data is Fetched")
            print(changedData.count)
            for item in changedData{
                print(item.mobileNumber!, item.vehicleNumber!, item.vehicleType!, item.serviceType!, item.dealer!, item.city!, item.slotData!, item.time!, item.comments!)
            }
        }
        catch{
            debugPrint(error.localizedDescription)
        }
    }

//    func saveDetails(){
//        let data = ComponentClass(MobileNumber: ComponentClass.Data.MobileNumber, VehicleNumber: ComponentClass.Data.VehicleNumber, VehicleType: ComponentClass.Data.VehicleType, ServiceType: ComponentClass.Data.ServiceType, SlotData: ComponentClass.Data.SlotDate, Time: ComponentClass.Data.Time, Dealer: ComponentClass.Data.Dealer, City: ComponentClass.Data.City, Comments: ComponentClass.Data.Comments)
//
//        ComponentClass.Data.DataArray.append(data)
//
//        if let DataDetails = try? NSKeyedArchiver.archivedData(withRootObject: ComponentClass.Data.DataArray, requiringSecureCoding: false){
//            defaults.set(DataDetails, forKey: "DetailsData")
//        }
//    }
}

class SuccessViewController: UIViewController {
    
    let navigationControllerObject = CustomNavigationViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBackButtonDesign(controller: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    @objc func transitionBackTo() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Done(_ sender: UIButton) {
       let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! MainTabBar
       // let dest = self.storyboard?.instantiateViewController(withIdentifier: "MainGarage") as! CustomNavigationViewController
        self.presentDet(dest)
//       let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
//        self.navigationController?.present(dest, animated: true, completion: nil)
    }
    
}

//
//  GarageDealerViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 07/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift
class CurrentLocationCell: UITableViewCell {}


class GarageDealerViewController: UIViewController ,UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource{
     var PassObject:[String:Any] = [:]
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let auth = DealerInteractor()
    func assign(object:[String:Any]){
        self.PassObject = object
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CurrentLocationCell
        cell.textLabel?.text = "Udupi"
        cell.textLabel?.frame.size = CGSize(width: 142, height: 16)
        cell.textLabel?.textColor = #colorLiteral(red: 0.4431372549, green: 0.4431372549, blue: 0.4431372549, alpha: 1)
        cell.imageView?.image = UIImage(named: "gps-fixed-indicator")!
        cell.imageView?.frame.size  = CGSize(width: 20, height: 20)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let cell = tableView.cellForRow(at: indexPath) as! CurrentLocationCell
        SearchBar.text = cell.textLabel?.text
        Search(SearchBar)
        self.CurrentLocationTableView.frame.size = CGSize(width: 0, height: 0)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48 }

    let navigationControllerObject = CustomNavigationViewController()

    var GarageInfo = [GaragedealerModel(ShopName: "Tune Motors", Location:"Udupi", Distance: 1.5, Number: 9113945238, RatingImage: UIImage(named: "Star-1")!, SilverImage: UIImage(named: "Star-5")!, GoldRating: 1, ViewImage: UIImage(named: "GaragePic")!, Address1: "217, NH 66, Belthangady, Santhekathe", Address2: "Udupi, Karnataka, 576117", id: "5ef2d6283256dc0017fd8e51"),
                      GaragedealerModel(ShopName: "Sai Motors Service", Location:"Udupi", Distance: 4.5, Number: 9876543200, RatingImage: UIImage(named: "Star-1")!, SilverImage: UIImage(named: "Star-5")!, GoldRating: 3, ViewImage: UIImage(named: "GaragePic")!, Address1: "217, NH 66, Belthangady, Santhekathe", Address2: "Udupi, Karnataka, 576117", id: "5eda547fd753500017c3e839"),
                      GaragedealerModel(ShopName: "Super Service Motors", Location:"Mangalore", Distance: 6, Number: 9876543210, RatingImage: UIImage(named: "Star-1")!, SilverImage: UIImage(named: "Star-5")!, GoldRating: 4, ViewImage: UIImage(named: "GaragePic")!,Address1: "217, NH 66, Belthangady, Santhekathe", Address2: "Udupi, Karnataka, 576117", id: "5eda5464d753500017c3e838"),
                      GaragedealerModel(ShopName: "Vaishali Services", Location:"Mangalore", Distance: 7, Number: 9876543210, RatingImage: UIImage(named: "Star-1")!, SilverImage: UIImage(named: "Star-5")!, GoldRating: 1, ViewImage: UIImage(named: "GaragePic")!, Address1: "217, NH 66, Belthangady, Santhekathe", Address2: "Udupi, Karnataka, 576117", id: "5ef2d2b93256dc0017fd8e4e"),
                      GaragedealerModel(ShopName: "Supreme Auto Dealers", Location:"Udupi", Distance: 5.5, Number: 9972990374, RatingImage: UIImage(named: "Star-1")!, SilverImage: UIImage(named: "Star-5")!, GoldRating: 5, ViewImage: UIImage(named: "GaragePic")!, Address1: "217, NH 66, Belthangady, Santhekathe", Address2: "Udupi, Karnataka, 576117", id: "5eda5458d753500017c3e837")]
    
    var GarageName = [GaragedealerModel]()
    var searching = false
    @IBOutlet var CurrentLocationTableView: UITableView!
    @IBOutlet var SearchLabel: UILabel!
    @IBOutlet var CollectionView1: UICollectionView!
    @IBOutlet var SearchBar: UITextField!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let screenwidth = UIScreen.main.bounds.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 40, bottom: 20, right: 40)
        layout.itemSize = CGSize(width: screenwidth - 80, height: 130)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        CollectionView1.collectionViewLayout = layout
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        CurrentLocationTableView.addShadowSetUp()
        navigationControllerObject.navigationBackButtonDesign(controller: self)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0xED7E2B)
        CurrentLocationTableView.delegate = self
        CurrentLocationTableView.dataSource = self
        self.CollectionView1.dataSource = self
        self.CollectionView1.delegate = self
        SearchBar.delegate = self
        self.SearchBar.addbottom()
        SearchBar.addTarget(self, action: #selector(Search(_ :)), for: .editingChanged)
        SearchBar.returnKeyType = UIReturnKeyType.search
       let tapped = UITapGestureRecognizer()
       tapped.delegate = self
       view.addGestureRecognizer(tapped)
        SearchLabel.isHidden = true
//        let token = keychain.get(Keys.AccessToken)
//        print("token", token!)
//        let response = auth.getDealers(requestQueries: [:], requestHeaders: ["Authorization" : "Bearer \(token!)"])
//    //    let response = auth.getDealers(requestQueries: ["city": "udupi", "page": "\(1)" , "sortby": "city", "orderby": "asc"], requestHeaders: ["Authorization": "Bearer \(token!)"])
//        print(response.data!.dealers!)
//
//        //print(response.data?.dealers?.count ?? 0)
//        guard let  data = response.data?.dealers else{
//            print("error")
//            print(response.message!, response.name!)
//            return
//        }
//        idForGarage = data
//        print(type(of: idForGarage[0]._id!))
        
    }
    
    @objc func transitionBackTo() {
        self.navigationController?.popViewController(animated: true)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: CollectionView1))! || ((touch.view?.isDescendant(of: CurrentLocationTableView))!){
            
            SearchBar.resignFirstResponder()
            SearchLabel.isHidden = true
            SearchBar.placeholder = "Search"
            return false
        }
        return true
    }
@objc func Search(_ textField: UITextField){
        SearchLabel.isHidden = false
        textField.placeholder = ""
        if (textField.text?.count)! != 0 && (textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).count) != 0{
            GarageName.removeAll()
            searching = true
            for location in GarageInfo{
                if let strSearch = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines){
                    let range = location.Location.lowercased().range(of: strSearch, options: .caseInsensitive, range: nil, locale: nil)
                    if range != nil{
                        self.GarageName.append(location)
                        CollectionView1.reloadData()
                    }
                    
                }
                
            }
            
        }
        else  {
            searching = false
            CollectionView1.reloadData()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        SearchBar.resignFirstResponder()
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching{
            return GarageName.count
        }
        else {
            return GarageInfo.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GarageCollectionViewCell
        if searching{
            let x = GarageName[indexPath.row].GoldRating
            cell.ShopName.text = GarageName[indexPath.row].ShopName + ","
            cell.Location.text = GarageName[indexPath.row].Location
            cell.Distance.text = String(GarageName[indexPath.row].Distance) + "km"
            cell.Number.text = String(GarageName[indexPath.row].Number)
            
            if x<4{
                for i in 0...(x-1){
                    cell.Ratings[i].image = GarageName[indexPath.row].RatingImage
                }
                    
                for j in (x)...4{
                    cell.Ratings[j].image = GarageName[indexPath.row].SilverImage
                    
                }
            }
                
            else if x == 4{
                for i in 0...3{
                    cell.Ratings[i].image = GarageName[indexPath.row].RatingImage
                    cell.Ratings[4].image = GarageName[indexPath.row].SilverImage
                  
                }
            }
            else {
                for i in 0...4{
                    cell.Ratings[i].image = GarageName[indexPath.row].RatingImage
        }
            }
        }
        else {
            
        let x = GarageInfo[indexPath.row].GoldRating
        cell.ShopName.text = GarageInfo[indexPath.row].ShopName + ","
        cell.Location.text = GarageInfo[indexPath.row].Location
        cell.Distance.text = String(GarageInfo[indexPath.row].Distance) + "km"
        cell.Number.text = String(GarageInfo[indexPath.row].Number)
        
        if x<4{
        for i in 0...(x-1){
            cell.Ratings[i].image = GarageInfo[indexPath.row].RatingImage
            
        }
        for j in (x)...4{
            cell.Ratings[j].image = GarageInfo[indexPath.row].SilverImage
         
        }
        }
        else if x == 4{
            for i in 0...3{
            cell.Ratings[i].image = GarageInfo[indexPath.row].RatingImage
            cell.Ratings[4].image = GarageInfo[indexPath.row].SilverImage
            
            }
            
        }
        else {
            for i in 0...4{
                cell.Ratings[i].image = GarageInfo[indexPath.row].RatingImage
              
            }
        }
        }
        cell.addShadowSetUp()
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        SearchBar.resignFirstResponder()
        let detVC = self.storyboard?.instantiateViewController(withIdentifier: "DealerProfileViewController") as! DealerProfileViewController
                if searching{
                    detVC.garageName = GarageName[indexPath.row].ShopName
                    detVC.City = GarageName[indexPath.row].Location
                    detVC.Location1 = GarageName[indexPath.row].Address1
                    detVC.Location2 = GarageName[indexPath.row].Address2
                    detVC.Area = GarageName[indexPath.row].Distance
                    detVC.MobileNumber = GarageName[indexPath.row].Number
                    detVC.goldRatingValue = GarageName[indexPath.row].GoldRating
                    detVC.GoldRating = GarageName[indexPath.row].RatingImage
                    detVC.SilverRating = GarageName[indexPath.row].SilverImage
                    PassObject.updateValue(GarageName[indexPath.row].ShopName, forKey: "dealer")
                    PassObject.updateValue(GarageName[indexPath.row].Location, forKey: "city")
                  //  PassObject.updateValue(GarageName[indexPath.row].GoldRating, forKey: "Ratings")
                    PassObject.updateValue(GarageName[indexPath.row].id, forKey: "id")
                    detVC.assign(object: PassObject)
                    self.navigationController?.pushViewController(detVC, animated: true)
                }
                else{
                    detVC.garageName = GarageInfo[indexPath.row].ShopName
                    detVC.City = GarageInfo[indexPath.row].Location
                    detVC.Location1 = GarageInfo[indexPath.row].Address1
                    detVC.Location2 = GarageInfo[indexPath.row].Address2
                    detVC.Area = GarageInfo[indexPath.row].Distance
                    detVC.MobileNumber = GarageInfo[indexPath.row].Number
                    detVC.goldRatingValue = GarageInfo[indexPath.row].GoldRating
                    detVC.GoldRating = GarageInfo[indexPath.row].RatingImage
                    detVC.SilverRating = GarageInfo[indexPath.row].SilverImage
                    PassObject.updateValue(GarageInfo[indexPath.row].ShopName, forKey: "dealer")
                    PassObject.updateValue(GarageInfo[indexPath.row].Location, forKey: "city")
                    //PassObject.updateValue(GarageInfo[indexPath.row].GoldRating, forKey: "Ratings")
                    PassObject.updateValue(GarageInfo[indexPath.row].id, forKey: "id")
                    detVC.assign(object: PassObject)
                    self.navigationController?.pushViewController(detVC, animated: true)
                }
    }

}


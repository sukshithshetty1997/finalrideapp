//
//  DealerProfileViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 27/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift

class DealerProfileViewController: UIViewController{
    let navigationControllerObject = CustomNavigationViewController()
    var PassObj:[String:Any] = [:]
    let auth = DealerInteractor()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    @IBOutlet var Ratings: [UIImageView]!
    @IBOutlet var MobileNum: UILabel!
    @IBOutlet var Distance: UILabel!
    @IBOutlet var Address2: UILabel!
    @IBOutlet var Address1: UILabel!
    @IBOutlet var GarageName: UILabel!
    @IBOutlet var CheckSlot: UIButton!
    
    var City:String!
    var garageName:String!
    var Location1:String!
    var Location2:String!
    var MobileNumber:Int!
    var Area:Double!
    var GoldRating = UIImage()
    var SilverRating = UIImage()
    var goldRatingValue:Int!
    var imageRating:[UIImage] = [UIImage(named: "Star-1")!, UIImage(named: "Star-5")!]
    func assign(object:[String:Any]){
        self.PassObj = object
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBackButtonDesign(controller: self)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(PassObj)
        CheckSlot.layer.cornerRadius = CheckSlot.frame.height/2
        self.GarageName.text = garageName
        self.MobileNum.text = String(MobileNumber)
        self.Address1.text = Location1
        self.Address2.text = Location2
        self.Distance.text = String(Area) + "km"
        let num = goldRatingValue
        for i in 0...(num!-1){
            self.Ratings[i].image = imageRating[0]
            if num!<5{
            for j in num!...4{
                self.Ratings[j].image = imageRating[1]
        }
      
    }

    }
    }
    
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func present(_ sender: Any) {
//        let token = keychain.get(Keys.AccessToken)
//        let response = auth.getDealer(dealerID: PassObj["id"] as! String, requestHeaders: ["Authorization" : "Bearer \(token!)"])
//        guard let data = response.data?._id else {
//            print("No id available")
//            print(response.message!, response.name!)
//            return
//        }
//        print("ID", data)
        let destVC = self.storyboard?.instantiateViewController(withIdentifier: "DateViewController") as! DateViewController
       // PassObj.updateValue(garageName, forKey: "dealer")
      //  PassObj.updateValue(City, forKey: "city")
       // ComponentClass.Data.Dealer = garageName
    //    ComponentClass.Data.City = City
        destVC.assign(object: PassObj)
        self.presentDet(destVC)
    }
    
   func dismissDate() {
    let desVC = self.storyboard?.instantiateViewController(withIdentifier: "BookServiceDetailsViewController") as! BookServiceDetailsViewController
    self.navigationController?.pushViewController(desVC, animated:  true)
}
}

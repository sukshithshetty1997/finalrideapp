//
//  TimeViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 29/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
protocol DismissTime {
    func dismissTime()
}

class TimeViewController: UIViewController {
    @IBOutlet var PM: UILabel!
    @IBOutlet var AM: UILabel!
    @IBOutlet var Time: UILabel!
    var DismissDelegate: DismissTime?
      var PassObj:[String:Any] = [:]
    var DraftTime:String = ""
    var hour:Int = .init()
    var min:Int = .init()
    var DateSlot:String = ""
    let formatter = DateFormatter()
    @IBOutlet var TimePicker: UIDatePicker!
    var fullNameArr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        TimePicker.datePickerMode = .time
        formatter.dateStyle = .full
        formatter.dateFormat = "hh:mm a"
        DraftTime = formatter.string(from: TimePicker!.date)
        fullNameArr = DraftTime.split{$0 == " "}.map(String.init)
        if fullNameArr[1] == "PM"{
            AM.textColor = UIColor.lightText
            PM.textColor = UIColor.white
        }
        else{
            AM.textColor = UIColor.white
            PM.textColor = UIColor.lightText
        }
        formatter.dateFormat = "hh:mm"
        Time.text = formatter.string(from: TimePicker!.date)
        
    }
    func assign(object:[String:Any]){
        self.PassObj = object
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBAction func TimeChanged(_ sender: Any) {
        formatter.dateFormat = "hh:mm a"
        DraftTime = formatter.string(from: TimePicker!.date)
        fullNameArr = DraftTime.split{$0 == " "}.map(String.init)
        if fullNameArr[1] == "PM"{
            AM.textColor = UIColor.lightText
            PM.textColor = UIColor.white
        }
        else{
            AM.textColor = UIColor.white
            PM.textColor = UIColor.lightText
        }
        formatter.dateFormat = "hh:mm"
        Time.text = formatter.string(from: TimePicker!.date)
    }
    
    @IBAction func OKTime(_ sender: Any) {
       // ComponentClass.Data.Time =  formatter.string(from: TimePicker!.date) + fullNameArr[1].lowercased()
        let time = formatter.string(from: TimePicker!.date) + " " + fullNameArr[1]
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "h:mm a"
        let date = dateformatter.date(from: time)!
        dateformatter.dateFormat = "HH:mm"
        let Date24 = dateformatter.string(from: date)
        print("24 hour format", Date24)
        PassObj.updateValue(Date24, forKey: "SaveTime")
        PassObj.updateValue(time, forKey: "time")
        let desVC = self.storyboard?.instantiateViewController(withIdentifier: "GarageDetailsNavigate") as! GarageDetailsNavigate
        desVC.assign(object: PassObj)
        presentingViewController?.presentSec(desVC)
//        let desVC = self.storyboard?.instantiateViewController(withIdentifier: "BookServiceDetailsViewController") as!BookServiceDetailsViewController
//        self.navigationController?.pushViewController(desVC, animated:  true)
    }
    @IBAction func CancelTime(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
       // self.navigationController?.popViewController(animated: true)
    }
}
class GarageDetailsNavigate: UINavigationController {
    var PassObj:[String:Any] = [:]
    
    func assign(object:[String:Any]){
        self.PassObj = object
    }
    override func viewDidLoad() {
        let desVC = self.storyboard?.instantiateViewController(withIdentifier: "BookServiceDetailsViewController") as!BookServiceDetailsViewController
        desVC.assign(object: PassObj)
        self.viewControllers = [desVC]
        self.setViewControllers([desVC], animated: false)
    }
    
}

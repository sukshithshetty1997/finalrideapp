//
//  ViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 03/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class MainGarageViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource{
    
    var navigationControllerObject = CustomNavigationViewController()
    
    @IBOutlet var Days: UILabel!
    var str:[String] = ["Book a Service", "Service Records","Owners Manual", "Tool Kit", "Accessories"]
    var image:[UIImage] = [UIImage(named: "telemarketer")!, UIImage(named: "folder")!, UIImage(named: "notebook")!, UIImage(named: "tOLS")!, UIImage(named: "helmet")!]
    
    @IBOutlet var RecordTable: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        navigationControllerObject.hideNavigationBar(controller: self)
        //self.tabBarController?.tabBar.unselectedItemTintColor = UIColor(rgb: 0xFED2AE)
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        RecordTable.delegate = self
        RecordTable.dataSource = self
        RecordTable.allowsSelectionDuringEditing = true
        RecordTable.allowsSelection = true
        self.Days.text = "\(15) Days"

    }
    
    @objc func transitionBackTo() {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        self.presentDet(dest)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.navigationBar.isTranslucent = false
        navigationControllerObject.showNavigationBar(controller: self)
        super.viewWillDisappear(animated)
    }
    override func viewDidLayoutSubviews() {
        RecordTable.tableFooterView = UIView(frame: .zero)
        RecordTable.frame.size.height = CGFloat(str.count * 80)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return str.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = RecordTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel!.text = str[indexPath.row]
        cell.textLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 14)
        cell.textLabel?.textColor = #colorLiteral(red: 0.3176470588, green: 0.3215686275, blue: 0.3176470588, alpha: 1)
        cell.imageView?.image = image[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            //  let destVC = self.storyboard?.instantiateViewController(withIdentifier: "BookServiceNavigate") as! UINavigationController
            let destVC = self.storyboard?.instantiateViewController(withIdentifier: "BookServiceViewController") as! BookServiceViewController
            // self.present(destVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(destVC, animated: true)
            
        case 1:
            let destVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchRecordsNavigate") as! UINavigationController
            self.presentDet(destVC)
        case 2:
            let destVC = self.storyboard?.instantiateViewController(withIdentifier: "OwnersManualNavigate") as! UINavigationController
            self.presentDet(destVC)
            
        case 3:
            let destVC = self.storyboard?.instantiateViewController(withIdentifier: "ToolKitNavigate") as! UINavigationController
             self.presentDet(destVC)
        case 4:
            //let destVC = self.storyboard?.instantiateViewController(withIdentifier: "AccessoriesNavigate") as! UINavigationController
            //self.present(destVC, animated: true, completion: nil)
            let destVC = self.storyboard?.instantiateViewController(withIdentifier: "AccessoriesViewController") as! AccessoriesViewController
            self.navigationController?.pushViewController(destVC, animated: true)
        default:
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


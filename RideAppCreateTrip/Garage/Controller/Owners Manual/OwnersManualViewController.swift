//
//  OwnersManualViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 03/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import CoreData

class OwnersManualViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UITextFieldDelegate{
    let AppDelegate = UIApplication.shared.delegate as? AppDelegate
    let navigationControllerObject = CustomNavigationViewController()
    
    @IBOutlet var VehicleTypeLabel: UILabel!
    @IBOutlet var VehicleTypeTextField: UITextField!
    let Array = SearchRecordsViewController()
    var ArrayList = [String]()
    let table = UITableView()
    let down = UIButton()
    var flag = false
    var Selected = false
    let PersonList:[String] = ["KA 01 A234", "Sukshith Shetty", "33-129, officers colony", "Mangalore", "KA", "576117", "8762908477", "sukshith.shetty1997@gmail.com"]
    let BikeList:[String] = ["261828292", "chsis23", "exide123", "4562", "2020", "Black", "3445TY"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarDesign(controller: self, title: "Owners Manual")}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PersonalDataSave()
        BikeDataSave()
     //   OwnerManualData.Data.PersonalDetails = PersonList
      //  OwnerManualData.Data.BikeDetails = BikeList
        
        down.setImage(UIImage(named: "down"), for: .normal)
        down.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        down.addTarget(self, action: #selector(DropDownList), for: UIControl.Event.touchDown)
        VehicleTypeTextField.rightViewMode = .always
        VehicleTypeTextField.rightView = down
        VehicleTypeTextField.addbottom()
        down.frame = CGRect(x: 0, y: 0, width: VehicleTypeTextField.frame.height * 0.7, height: VehicleTypeTextField.frame.height * 0.7)
        VehicleTypeTextField.inputView = UIView()
        
        VehicleTypeLabel.isHidden = true
        ArrayList = Array.ArrayVehicleType
        let gesture = UITapGestureRecognizer()
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
        table.delegate = self
        table.dataSource = self
        table.register(ServiceCell.self, forCellReuseIdentifier: "cell")
        VehicleTypeTextField.delegate = self
        table.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
        VehicleTypeTextField.addTarget(self, action: #selector(DropDownList), for: UIControl.Event.editingDidBegin)
        self.view.addSubview(table)
        table.addCorner()

    }
    
    @objc func transitionBackTo(){
       // self.navigationController?.popViewController(animated: true)
     self.dismissDet()
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: table))! || (touch.view?.isDescendant(of: down))!{
            return false
        }
        view.endEditing(true)
        table.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
        if flag{ flag = !flag }
        return true
}
    func PersonalDataSave(){
        guard let context = AppDelegate?.persistentContainer.viewContext else {return}
        let managedContext:PersonalDetails = PersonalDetails.init(entity: NSEntityDescription.entity(forEntityName: "PersonalDetails", in: context)!, insertInto: context)
        managedContext.license = PersonList[0]
        managedContext.name = PersonList[1]
        managedContext.door = PersonList[2]
        managedContext.city = PersonList[3]
        managedContext.state = PersonList[4]
        managedContext.pincode = PersonList[5]
        managedContext.mobile = PersonList[6]
        managedContext.email = PersonList[7]
        
        do {
            try context.save()
            print("Personal Data has been saved")
        }
        catch{
            print(error.localizedDescription)
        }
    }
    func BikeDataSave(){
        guard let context = AppDelegate?.persistentContainer.viewContext else {return}
        let managedContext:BikeDetails = BikeDetails.init(entity: NSEntityDescription.entity(forEntityName: "BikeDetails", in: context)!, insertInto: context)
        managedContext.engine = BikeList[0]
        managedContext.frame = BikeList[1]
        managedContext.battery = BikeList[2]
        managedContext.regNo = BikeList[3]
        managedContext.model = BikeList[4]
        managedContext.color = BikeList[5]
        managedContext.dealerCode = BikeList[6]
        
        do {
            try context.save()
            print("Bike Data has been saved")
        }
        catch{
            print(error.localizedDescription)
        }
    }
    
    @objc func DropDownList(){
        flag = !flag
        VehicleTypeLabel.isHidden = false
        VehicleTypeTextField.placeholder = ""
        if flag {
            table.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: CGFloat(ArrayList.count * 45))}
            else{
                   table.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
            }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ServiceCell
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        cell.backgroundColor = UIColor.lightGray
        cell.textLabel?.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.regular)
        cell.textLabel?.textColor = UIColor(red: 79, green: 80, blue: 79)
        cell.textLabel?.text = ArrayList[indexPath.item]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
    
        VehicleTypeTextField.text = cell?.textLabel?.text
        table.frame = CGRect(x: VehicleTypeTextField.frame.origin.x, y: VehicleTypeTextField.frame.origin.y + VehicleTypeTextField.frame.height + 10, width: VehicleTypeTextField.frame.width, height: 0)
        flag = !flag
        Selected = true
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func GoAction(_ sender: Any) {
        if Selected{
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "OwnersManualDetailsViewController") as! OwnersManualDetailsViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
    }
}

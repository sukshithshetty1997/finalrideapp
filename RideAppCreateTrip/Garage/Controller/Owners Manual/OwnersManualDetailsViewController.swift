//
//  OwnersManualDetailsViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 06/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import CoreData

class OwnerManualData: Any{
    static let Data = OwnerManualData()
    private init(){}
    var PersonalDetails:[String] = []
    var BikeDetails:[String] = []
    
}
class OwnersManualDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SaveChanges{
    let appdelegate = UIApplication.shared.delegate as? AppDelegate
    let navigationControllerObject = CustomNavigationViewController()
    var PersonList:[String] = []
    var BikeList:[String] = []

    @IBOutlet var DetailsTable: UITableView!
    @IBOutlet var PersonalDetailsButton: UIButton!
    @IBOutlet var BikeDetailsButton: UIButton!
    let PersonalDetails:[String] = ["Licence No.:", "Name:","Door no.:", "City:","State:","Pincode:","Mobile:","Email:"]
    //    let PersonList:[String] = ["KA 01 A234", "Sukshith Shetty", "33-129, officers colony", "Mangalore", "KA", "576117", "8762908477", "sukshith.shetty1997@gmail.com"]
    var BikeDetails:[String] = ["Engine:", "Frame No:","Battery make:","Reg No.:", "Model:", "Color:", "Dealer code:"]
    //    let BikeList:[String] = ["261828292", "chsis23", "exide123", "4562", "2020", "Black", "3445TY"]
    var OwnersManualSection = 0

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarLeftRightDesign(controller: self, title: "Owners Manual", rightImages: [UIImage(named: "share")!, UIImage(named: "Edit")!])
        PersonList = PersonFromCoreData()
        BikeList = BikeFromCoreData()
        DetailsTable.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        DetailsTable.reloadData()
        PersonalDetailsButton.BottomBorder(color: UIColor.orange)
        BikeDetailsButton.RemoveBorder()
        DetailsTable.delegate = self
        DetailsTable.dataSource = self
    }
    
    
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }

    @objc func navigationRightItem1(){
        let Data = generatePDF()
        let activity = UIActivityViewController(activityItems: [Data], applicationActivities: nil)
        present(activity, animated: true, completion: nil)
    }
    
    @objc func navigationRightItem2(){
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "PersonalEditViewController") as! PersonalEditViewController
        dest.SaveDelegate = self 
        let object1 = PersonFromCoreData()
        let object2 = BikeFromCoreData()
        dest.setValuesToArrays(object1: object1, object2: object2, Value: OwnersManualSection)
        self.navigationController?.pushViewController(dest, animated: true)    }
    
    func SaveTheEdits() {
        PersonList = PersonFromCoreData()
        BikeList = BikeFromCoreData()
        self.DetailsTable.reloadInputViews()
        self.DetailsTable.reloadData()
    }
    
    func generatePDF() -> NSData {
        let pdfWrittenPath = DetailsTable.exportAsPdfFromView()
        let Data = NSData(contentsOfFile: pdfWrittenPath)!
        return Data
    }
    
    override func viewDidLayoutSubviews() {
        self.DetailsTable.addCorner()
        self.DetailsTable.addShadow()
        DetailsTable.reloadData()
        DetailsTable.reloadInputViews()
        if OwnersManualSection == 0{
        DetailsTable.frame = CGRect(x: DetailsTable.frame.origin.x, y: DetailsTable.frame.origin.y, width: DetailsTable.frame.size.width, height: CGFloat(PersonalDetails.count * 80))
        DetailsTable.reloadData()
        }
        else if OwnersManualSection == 1 {
            DetailsTable.frame = CGRect(x: DetailsTable.frame.origin.x, y: DetailsTable.frame.origin.y, width: DetailsTable.frame.size.width, height: CGFloat(BikeDetails.count * 80))
            DetailsTable.reloadData()
        }
    }
    func PersonFromCoreData() -> [String]{
        let context = appdelegate?.persistentContainer.viewContext
        let fetch = NSFetchRequest<PersonalDetails>(entityName: "PersonalDetails")
        
        do {
            let data = try context!.fetch(fetch)
            PersonList = [data[0].license!, data[0].name!, data[0].door!, data[0].city!, data[0].state!, data[0].pincode!, data[0].mobile!, data[0].email!]
            print("Data Stored in array sucessfully")
            
        }
        catch{
            debugPrint(error.localizedDescription)
        }
        return PersonList
        
    }
    
    func BikeFromCoreData() -> [String]{
        let context = appdelegate?.persistentContainer.viewContext
        let fetch = NSFetchRequest<BikeDetails>(entityName: "BikeDetails")
        
        do {
            let data = try context!.fetch(fetch)
            BikeList = [data[0].engine!, data[0].frame!, data[0].battery!, data[0].regNo!, data[0].model!, data[0].color!,data[0].dealerCode!]
            print("Data Stored in array sucessfully")
            
        }
        catch{
            debugPrint(error.localizedDescription)
        }
        return BikeList
        
    }
    
    @IBAction func PersonalActions(_ sender: UIButton) {
        OwnersManualSection = 0
        PersonalDetailsButton.BottomBorder(color: UIColor.orange)
        BikeDetailsButton.RemoveBorder()
        DetailsTable.frame = CGRect(x: DetailsTable.frame.origin.x, y: DetailsTable.frame.origin.y, width: DetailsTable.frame.size.width, height: CGFloat(PersonalDetails.count * 80))
        DetailsTable.reloadData()
    }
    
    @IBAction func BikeActions(_ sender: UIButton) {
        OwnersManualSection = 1
        BikeDetailsButton.BottomBorder(color: UIColor.orange)
        PersonalDetailsButton.RemoveBorder()
        DetailsTable.frame = CGRect(x: DetailsTable.frame.origin.x, y: DetailsTable.frame.origin.y, width: DetailsTable.frame.size.width, height: CGFloat(BikeDetails.count * 80))
        DetailsTable.reloadData()
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var tablecount = 0
        switch OwnersManualSection {
        case 0:
            tablecount = PersonalDetails.count
        case 1:
            tablecount = BikeDetails.count
        default:
            return 0
        }
        return tablecount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tablecell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if OwnersManualSection == 0{
      //  tablecell.textLabel?.text = PersonalDetails[indexPath.row]
      //  tablecell.detailTextLabel?.text = OwnerManualData.Data.PersonalDetails[indexPath.row]
            tablecell.textLabel?.text = PersonalDetails[indexPath.row]
            tablecell.detailTextLabel?.text = PersonList[indexPath.row]
        }
        else if OwnersManualSection == 1{
            tablecell.textLabel?.text = BikeDetails[indexPath.row]
            tablecell.detailTextLabel?.text = BikeList[indexPath.row]
        }
  
        return tablecell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}
extension UIButton{
    func BottomBorder(color:UIColor){
        let lineView = UIView(frame: CGRect(x: 0, y: self.frame.size.height, width: self.frame.size.width/4, height: 2))
        lineView.backgroundColor = color
        self.setTitleColor(UIColor.orange, for: UIControl.State.normal)
        self.addSubview(lineView)
    }
    func RemoveBorder(){
        let lineView = UIView(frame: CGRect(x: 0, y: self.frame.size.height, width: self.frame.size.width/4, height: 2))
        lineView.backgroundColor = UIColor.white
        self.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
        self.addSubview(lineView)
    }
}
extension UITableView {
    //Needs Change
    func addCorner(){
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
    }
    // Needs Change
    func addShadow(){
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowRadius = 15
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = .zero
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
}

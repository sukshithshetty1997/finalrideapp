//
//  PersonalEditViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 06/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import CoreData

protocol SaveChanges: class {
    func SaveTheEdits()
}
class EdiTableCell: UITableViewCell, UITextFieldDelegate{

    var selection:Int = 0
    @IBOutlet var ContentCell: UIView!
     @IBOutlet var EditLabel: UILabel!
     @IBOutlet var EditTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        EditTextField.delegate = self
    }
}

class StaticTableCell: UITableViewCell{
    @IBOutlet var StaticTableCell: UIView!
}

class PersonalEditViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIDocumentInteractionControllerDelegate, UIDocumentPickerDelegate{
    let AppDelegate = UIApplication.shared.delegate as? AppDelegate
    let navigationControllerObject = CustomNavigationViewController()
     weak var SaveDelegate: SaveChanges?
    var OwnersManual = OwnersManualDetailsViewController()
    var OwnerManualPersonalDetails:[String] = []
    var OwnerManualPersonalList:[String] = []
    var BikeDetails:[String] = []
    var BikeList:[String] = []
    var Selection:Int!
    var tag:Int = 0
 
    @IBOutlet var contentView: UIView!
    @IBOutlet var StaticVew: UIView!
    @IBOutlet var DetailView: UIView!
    @IBOutlet var EditTable: UITableView!
    @IBOutlet var StaticTable: UITableView!
    
    @IBOutlet var HeaderEditLabel: UILabel!
    @IBOutlet var StaticLabel: UILabel!
    
    @IBOutlet var scrollview: UIScrollView!
    
    func setValuesToArrays(object1:[String], object2:[String], Value: Int){
        self.OwnerManualPersonalList = object1
        self.BikeList = object2
        self.Selection = Value
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        BikeDetails = OwnersManual.BikeDetails
        OwnerManualPersonalDetails = OwnersManual.PersonalDetails
        
//        
//        EditTable.register(EdiTableCell.self, forCellReuseIdentifier: "cell")
//        StaticTable.register(StaticTableCell.self, forCellReuseIdentifier: "cell")
        
        EditTable.delegate = self
        EditTable.dataSource = self
        StaticTable.delegate = self
        StaticTable.dataSource = self

        EditTable.layer.borderColor = UIColor.lightGray.cgColor
        EditTable.layer.borderWidth = 1
        StaticTable.layer.borderColor = UIColor.lightGray.cgColor
        StaticTable.layer.borderWidth = 1
        self.scrollview.contentSize = CGSize(width: self.contentView.frame.size.width, height: 1100)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationLeftRightDesign(controller: self, title: "Owners Manual", rightImage: UIImage(named: "share")!)
    }
    
    @objc func PopToCreateTrip() {
        let Data = generatePDF()
        let activity = UIActivityViewController(activityItems: [Data], applicationActivities: nil)
        present(activity, animated: true, completion: nil)
    }
    
    func generatePDF() -> NSData {
        let pdfWrittenPath = EditTable.exportAsPdfFromView()
        let Data = NSData(contentsOfFile: pdfWrittenPath)!
        return Data
    }

    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)}
    
    @IBAction func EditCompleted(_ sender: Any) {
        WriteToCoreData()
        SaveDelegate?.SaveTheEdits()
        self.navigationController?.popViewController(animated: true)}
    
    override func viewDidLayoutSubviews() {
        EditTable.addShadow()
        EditTable.addCorner()
        StaticTable.addCorner()
        StaticTable.addShadow()
        
       self.scrollview.contentSize = CGSize(width: self.contentView.frame.size.width, height: 1100)
        //self.scrollview.contentSize = self.contentView.bounds.size;
        if Selection == 0{
            HeaderEditLabel.text = "Personal Details"
            StaticLabel.text = "Bike Details"
            setPersonalDetailsDynamicHeight()
            StaticTable.reloadData()
            EditTable.reloadData()
        }
            else if Selection == 1{
            HeaderEditLabel.text = "Bike Details"
            StaticLabel.text = "Personal Details"
            setBikeDetailsDynamicHeight()
            StaticTable.reloadData()
            EditTable.reloadData()
            }
    }
    func setPersonalDetailsDynamicHeight(){
        let EditHeight = DetailView.frame.size.height + 10
        let StaticHeight = StaticVew.frame.size.height + 10
        
        EditTable.frame = CGRect(x: EditTable.frame.origin.x, y: EditTable.frame.origin.y, width: EditTable.frame.size.width, height: CGFloat(OwnerManualPersonalList.count * 60) + EditHeight)
        StaticTable.frame = CGRect(x: EditTable.frame.origin.x, y: EditTable.frame.origin.y + 15 + EditTable.frame.size.height
            , width: EditTable.frame.size.width, height: CGFloat(BikeDetails.count * 60) + StaticHeight)
        
    }
    func setBikeDetailsDynamicHeight(){
        let StaticHeight = DetailView.frame.size.height + 10
        let EditHeight = StaticVew.frame.size.height + 10
        EditTable.frame = CGRect(x: EditTable.frame.origin.x, y: EditTable.frame.origin.y, width: EditTable.frame.size.width, height: CGFloat(BikeDetails.count * 60) + EditHeight)
        StaticTable.frame = CGRect(x: EditTable.frame.origin.x, y: EditTable.frame.origin.y + 15 + EditTable.frame.size.height
            , width: EditTable.frame.size.width, height: CGFloat(OwnerManualPersonalList.count * 60) + StaticHeight)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Selection {
        case 0:
            switch tableView {
            case EditTable:
                return OwnerManualPersonalList.count
            case StaticTable:
                return BikeDetails.count
            default:
                return 0
            }
        case 1:
            switch tableView {
            case EditTable:
                return BikeDetails.count
            case StaticTable:
                return OwnerManualPersonalList.count
            default:
                return 0
            }
        default:
            return 0
        }
    }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Selection {
        case 0:
            switch tableView{
        case EditTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EdiTableCell
            cell.EditTextField.tag = tag
            tag = tag + 1
            cell.EditTextField.addbottom()
            cell.EditLabel.text = OwnerManualPersonalDetails[indexPath.row]
            cell.EditTextField.text = OwnerManualPersonalList[indexPath.row]
            return cell
        case StaticTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = BikeDetails[indexPath.row]
            cell.detailTextLabel?.text = BikeList[indexPath.row]
            return cell
        default:
            return .init()
        }
        case 1:
        switch tableView {
        case EditTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EdiTableCell
          //  cell.EditDelegate = self
            cell.EditTextField.tag = tag
            tag = tag + 1
//            cell.selection = 1
            cell.EditTextField.addbottom()
       //     cell.EditTextField.addTarget(self, action: #selector(ChangeText(_:sender:)), for: UIControl.Event.editingDidEnd)
            cell.EditLabel.text = BikeDetails[indexPath.row]
            cell.EditTextField.text = BikeList[indexPath.row]
            return cell
        case StaticTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = OwnerManualPersonalDetails[indexPath.row]
            cell.detailTextLabel?.text = OwnerManualPersonalList[indexPath.row]
            return cell
        default:
            return .init()
        }
        default:
            return .init()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func WriteToCoreData(){
        guard let context = AppDelegate?.persistentContainer.viewContext else {return}
        switch Selection {
        case 0:
            var ArrayElements:[String] = []
            let fetch = NSFetchRequest<PersonalDetails>(entityName: "PersonalDetails")
            do{
                let data:[PersonalDetails] = try context.fetch(fetch)
                for item in 0...(OwnerManualPersonalList.count - 1){
                    let indexPath = IndexPath(row: item, section: 0)
                    let cell = EditTable.cellForRow(at: indexPath) as! EdiTableCell
                    ArrayElements.append((cell.EditTextField.text!))
                }
                data[0].license = ArrayElements[0]
                data[0].name = ArrayElements[1]
                data[0].door = ArrayElements[2]
                data[0].city = ArrayElements[3]
                data[0].state = ArrayElements[4]
                data[0].pincode = ArrayElements[5]
                data[0].mobile = ArrayElements[6]
                data[0].email = ArrayElements [7]
                
                try context.save()
                print("Data Saved")
            }
            catch {
                debugPrint(error.localizedDescription)
            }
        case 1:
            var ArrayElements:[String] = []
            let fetch = NSFetchRequest<BikeDetails>(entityName: "BikeDetails")
            
            do{
                let data:[BikeDetails] = try context.fetch(fetch)
                for item in 0...(BikeList.count - 1){
                    let indexPath = IndexPath(row: item, section: 0)
                    let cell = EditTable.cellForRow(at: indexPath) as! EdiTableCell
                    ArrayElements.append((cell.EditTextField.text!))
                }
                data[0].engine = ArrayElements[0]
                data[0].frame = ArrayElements[1]
                data[0].battery = ArrayElements[2]
                data[0].regNo = ArrayElements[3]
                data[0].model = ArrayElements[4]
                data[0].color = ArrayElements[5]
                data[0].dealerCode = ArrayElements[6]
                try context.save()
                print("Data Saved")
            }
            catch {
                debugPrint(error.localizedDescription)
            }
        default:
            print("Error")
            break
        }
    }
    
//        func changeValues(sender: EdiTableCell) {
//            switch Selection {
//            case 0:
//                //let state = OwnerManualData.Data.PersonalDetails.count
//                //for i in 0...(state-1){
//                OwnerManualData.Data.PersonalDetails[sender.EditTextField.tag] = sender.EditTextField.text!
//              //  }
//            case 1:
//                //let state = OwnerManualData.Data.BikeDetails.count
//               // for i in 0...(state-1){
//                    OwnerManualData.Data.BikeDetails[sender.EditTextField.tag] = sender.EditTextField.text!
//               // }
//            default:
//                return
//            }
//        }
    
    
}


//
//  PaymentModeViewController.swift
//  RideAppCreateTrip
//
//  Created by Sukshith Shetty on 06/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class PaymentModeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    let PaymentModes:[String] = ["Net Banking", "Debit Card", "Credit Card", "Bhim UPI", "Cash on Delivery(COD)"]
    let ShippingAddress = UITextView()
    let navigationControllerObject = CustomNavigationViewController()
    @IBOutlet var PaymentButton: DesignableButtons!
    @IBOutlet var PaymentTableView: UITableView!
    var lines:Int!
    var confirmationPin:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBarDesign(controller: self, title: "PAYMENT MODE SELECTION")
        //self.navigationItem.title = "Payment Mode Selection"
        PaymentTableView.delegate = self
        PaymentTableView.dataSource = self
        ShippingAddress.textAlignment = .left
        ShippingAddress.textColor = #colorLiteral(red: 0.4470588235, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        ShippingAddress.isEditable = false
        ShippingAddress.layer.masksToBounds = true
        ShippingAddress.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        if let Text = UserDefaults.standard.object(forKey: "Address") as? String{
            ShippingAddress.text = "The Shipping Address:" + "\n" + Text
            lines = Int(ShippingAddress.contentSize.height / ShippingAddress.font!.lineHeight)
            let layoutManager:NSLayoutManager = ShippingAddress.layoutManager
            let numberOfGlyphs = layoutManager.numberOfGlyphs
            var numberOfLines = 0
            var index = 0
            var lineRange:NSRange = NSRange()
            while (index < numberOfGlyphs) {
                layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
                index = NSMaxRange(lineRange);
                numberOfLines = numberOfLines + 1
            }
            lines = numberOfLines + 3
        }
        else{
            ShippingAddress.text = "You need to mention Shipping Address"
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        PaymentTableView.frame.size.height = CGFloat(PaymentModes.count * 60)
        ShippingAddress.frame = CGRect(x: PaymentTableView.frame.origin.x, y: PaymentTableView.frame.origin.y + PaymentTableView.frame.size.height + 30, width: PaymentTableView.frame.size.width, height: CGFloat(lines! * 14))
        self.view.addSubview(ShippingAddress)
        PaymentButton.frame = CGRect(x: ShippingAddress.frame.origin.x, y: ShippingAddress.frame.origin.y + ShippingAddress.frame.size.height + 30, width: ShippingAddress.frame.size.width, height: 42)
        self.view.addSubview(PaymentButton)
        ShippingAddress.addShadowSetUp()
        PaymentTableView.addShadowSetUp()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PaymentModes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = PaymentModes[indexPath.row]
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        cell.textLabel?.textColor = #colorLiteral(red: 0.3176470588, green: 0.3215686275, blue: 0.3176470588, alpha: 1)
        return cell
    }
    
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        confirmationPin = true
        print(cell?.textLabel?.text! ?? "Empty string")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }


    @IBAction func ProceedtoCompletion(_ sender: Any) {
        if confirmationPin{
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "SuccessAccessories") as! SuccessAccessories
            self.navigationController?.pushViewController(dest, animated: true)
        }
        else{
            let alert = UIAlertController(title: "Mode of Payment", message: "Please Select a Mode of Payment", preferredStyle: UIAlertController.Style.actionSheet)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    
    }
}

class SuccessAccessories: UIViewController{
    let navigationControllerObject = CustomNavigationViewController()
    override func viewDidLoad() {
        print("Purchase Done:")
        navigationControllerObject.navigationBackButtonDesign(controller: self)
    }
    @IBAction func PuchaseDone(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension UITextView{
    
    func numberOfLines() -> Int{
        if let fontUnwrapped = self.font{
            return Int(self.contentSize.height / fontUnwrapped.lineHeight)
        }
        return 0
    }
    
}


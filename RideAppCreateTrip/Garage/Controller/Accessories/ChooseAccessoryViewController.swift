//
//  ChooseAccessoryViewController.swift
//  RideAppCreateTrip
//
//  Created by Sukshith Shetty on 06/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class AccessoryData:NSObject{
    static let Data = AccessoryData()
    private override init() {}
    var ItemName:String!
    var ItemCost:String!
    var ItemImage:UIImage!
}
class ChooseAccessoryViewController: UIViewController {
    @IBOutlet var Stock: UILabel!
    @IBOutlet var ItemCost: UILabel!
    @IBOutlet var ItemName: UILabel!
    @IBOutlet var ItemImage: UIImageView!
    var choice:Int = 0
    let navigationControllerObject = CustomNavigationViewController()
    var Booked:Int!
    let Stocks:[String] = ["In Stock", "Out of Stock"]
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBarDesign(controller: self, title: "ITEMS")
        //self.navigationItem.title = "Items"
        let range = Int.random(in: 0...1)
        Stock.text = Stocks[range]
        if Booked == 0{
            choice = 0
            Stock.textColor = UIColor.green
        }
        else if Booked == 1{
            choice = 1
            Stock.textColor = UIColor.red
        }
        ItemCost.text = AccessoryData.Data.ItemCost
        ItemName.text = AccessoryData.Data.ItemName
        ItemImage.image = AccessoryData.Data.ItemImage!
    }
    
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func BuyItem(_ sender: Any) {
        if choice == 0{
        let dest  = self.storyboard?.instantiateViewController(withIdentifier: "AddressDetailsViewController") as! AddressDetailsViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
        else if choice == 1{
            let alert = UIAlertController(title: "Out of Stock", message: "The Item is currently out of Stock. We cannot proceed with the Payment. Sorry for the Inconvenience", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
}

//
//  AccessoriesViewController.swift
//  Garage
//
//  Created by Sukshith Shetty on 03/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class AccessoriesCollectionCell: UICollectionViewCell{

    @IBOutlet var ItemName: UILabel!
    @IBOutlet var PriceTag: UILabel!
    @IBOutlet var LikeButton: UIButton!
    @IBOutlet var ItemImage: UIImageView!
    @IBOutlet var DateLabel: UILabel!
    
}

class AccessoriesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource, UIGestureRecognizerDelegate{

    let navigationControllerObject = CustomNavigationViewController()
    
    var Items = [AccessoriesModel(Date: "22 SEP", ItemImage: UIImage(named: "Bitmap")!, ItemName: "Probiker Gloves", Price: 189.00), AccessoriesModel(Date: "28 SEP", ItemImage: UIImage(named: "Bitmap-1")!, ItemName: "Mike Gloves", Price: 219.00), AccessoriesModel(Date: "29 SEP", ItemImage: UIImage(named: "Bitmap-2")!, ItemName: "Adidas Tubular Shadow", Price: 189.00), AccessoriesModel(Date: "30 SEP", ItemImage: UIImage(named: "Bitmap-3")!, ItemName: "Nike Air Max 1", Price: 189.00)]
    @IBOutlet var Label: UILabel!
    @IBOutlet var AccessoriesCollectionView: UICollectionView!
    @IBOutlet var ItemsTextField: UITextField!
    var Booking:[Int] = [0,0,1,1]
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer()
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
        AccessoriesCollectionView.delegate = self
        AccessoriesCollectionView.dataSource = self
        ItemsTextField.addbottom()
    
       let search = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
       let image = UIImage(named: "search")
       search.image = image
       search.contentMode = .scaleAspectFill
        ItemsTextField.rightViewMode = .always
        ItemsTextField.rightView = search
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        navigationControllerObject.navigationBarDesign(controller: self, title: "Accessories")
    }
    
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: AccessoriesCollectionView))!{
            view.endEditing(true)
            return false
        }
        ItemsTextField.resignFirstResponder()
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectioncell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AccessoriesCollectionCell
        collectioncell.DateLabel.text = Items[indexPath.row].Date
        collectioncell.ItemImage.image = Items[indexPath.row].ItemImage
        collectioncell.ItemName.text = Items[indexPath.row].ItemName
        collectioncell.PriceTag.text = "₹" + String(Items[indexPath.row].Price)
        collectioncell.layer.borderColor = UIColor.orange.cgColor
        collectioncell.layer.borderWidth = 0.25
        return collectioncell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AccessoryData.Data.ItemName = Items[indexPath.row].ItemName
        AccessoryData.Data.ItemCost = "₹" + String(Items[indexPath.row].Price)
        AccessoryData.Data.ItemImage = Items[indexPath.row].ItemImage
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "ChooseAccessoryViewController") as! ChooseAccessoryViewController
        dest.Booked = Booking[indexPath.row]
        self.navigationController?.pushViewController(dest, animated: true)
    }

}


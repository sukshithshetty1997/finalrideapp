//
//  AddressDetailsViewController.swift
//  RideAppCreateTrip
//
//  Created by Sukshith Shetty on 06/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class AddressDetailsViewController: UIViewController {
    let defaults = UserDefaults.standard
    let SaveAddress = UIButton(type: .system)
    @IBOutlet var AddressText: UITextView!
    @IBOutlet var ChangeAddress: DesignableButtons!
    @IBOutlet var PaymentDone: DesignableButtons!
    var Address:String!
    var lines:Int! = nil
    let navigationControllerObject = CustomNavigationViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBarDesign(controller: self, title: "DELIVERY ADDRESS")
        //self.navigationItem.title = "Delivery Address"
        AddressText.isEditable = false
        AddressText.addShadowSetUp()
        if let Text = defaults.object(forKey: "Address") as? String{
            AddressText.text = Text
        }
        else{
        AddressText.text = AddAddress()
        }
        SaveAddress.addTarget(self, action: #selector(ClickAddress), for: UIControl.Event.touchUpInside)
    }
    func AddAddress() -> String{
         Address = "Sukshith Shetty" + "\n" + "Flat: 103, 2nd Floor" + "\n" + "Sai Panchami Residency" + "\n" + "Devi Nagar, Jodhuraste" + "\n" + "Kukkundoor, Jodhuraste" + "Karkala Taluk, Udupi" + "\n" + "576117" + "\n" +  "Karnataka, INDIA"
        return Address
    }
    override func viewDidLayoutSubviews() {
        let layoutManager:NSLayoutManager = AddressText.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var numberOfLines = 0
        var index = 0
        var lineRange:NSRange = NSRange()
        while (index < numberOfGlyphs) {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange);
            numberOfLines = numberOfLines + 1
        }
        lines = numberOfLines + 3
        AddressText.frame.size.height = CGFloat(lines * 14)
        
        ChangeAddress.frame.origin.y = AddressText.frame.origin.y + AddressText.frame.size.height  + 20
        ChangeAddress.frame.size.height = 42
        
        PaymentDone.frame.origin.y = ChangeAddress.frame.origin.y + ChangeAddress.frame.size.height + 20
        PaymentDone.frame.size.height = 42
    }
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ChangeAddress(_ sender: Any) {
        AddressText.isEditable = true
        SaveAddress.frame = CGRect(x: PaymentDone.frame.origin.x, y: PaymentDone.frame.origin.y + PaymentDone.frame.size.height + 20, width: PaymentDone.frame.size.width, height:  PaymentDone.frame.size.height)
        SaveAddress.layer.cornerRadius = 21
        SaveAddress.setTitle("SAVE ADDRESS CHANGES", for: UIControl.State.normal)
        SaveAddress.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.4941176471, blue: 0.168627451, alpha: 1)
        SaveAddress.titleLabel?.textAlignment = .center
        SaveAddress.setTitleColor(UIColor.white, for: UIControl.State.normal)
        SaveAddress.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        self.view.addSubview(SaveAddress)
    }
    
    @IBAction func PaymentMode(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "PaymentModeViewController") as! PaymentModeViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
    
    @objc func ClickAddress(){
        defaults.set(AddressText.text, forKey: "Address")
        AddressText.isEditable = false
        SaveAddress.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        self.view.addSubview(SaveAddress)
        
    }
    
}

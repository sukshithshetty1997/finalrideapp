//
//  RiderInteractor.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class RiderInteractor {
    
    let networkURL = NetworkURL()
    
    // This will get current rider's profile
    func getProfile(requestHeaders : [String : Any]) -> GetRiderResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getProfile()
        let responseData : GetRiderResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
    // This will get other rider's profile
    func getRiderProfile(requestHeaders: [String : Any], riderID: String) -> GetRiderResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getRiderProfile(riderID)
        let responseData : GetRiderResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
    // This will update the current rider's profile
    func updateRiderProfile(requestHeaders: [String : Any]) -> UpdateRiderResponse {
        let method = "PUT"
        let headers = requestHeaders
        let url = networkURL.updateRiderProfile()
        let responseData : UpdateRiderResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    func AddProfileImage(requestBody : [String : Any], requestHeaders : [String : Any]) -> PostProfileImageResponse {
        let method = "POST"
        let body = requestBody
        let headers = requestHeaders
        let url: URL = networkURL.addRiderProfileImage()
       let responseData : PostProfileImageResponse = NetworkManager.request(url: url, method: method, body: body, headers: headers)!
        return responseData
    }
}

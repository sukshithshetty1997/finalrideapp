//
//  GetRiderResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class GetRiderResponse: Codable {
    
    struct Data : Codable {
        var _id: String!
        var name: String!
        var email: String!
        var phone: String!
        var joinedTrips: Array<String>!
        var services: Array<String>!
        var status: String!
        var avatar: String?
        var createdAt: String!
        var updatedAt: String!
    }
    
    var data : Data?
    var message : String?
    var name : String?
}

//
//  ResetPasswordResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 27/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class ResetPasswordResponse : Codable {
    struct Data : Codable {
        var message : String?
    }
    var data : Data?
    var message : String?
    var name : String?
}

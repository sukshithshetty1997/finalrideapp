//
//  RefreshTokenResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 27/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

// This is a type of response received after making a login call to the server
class RefreshTokenResponse : Codable {
    struct Data : Codable {
        var accessToken : String?
        var refreshToken : String?
    }
    var data : Data?
    var message : String?
    var name : String?
}

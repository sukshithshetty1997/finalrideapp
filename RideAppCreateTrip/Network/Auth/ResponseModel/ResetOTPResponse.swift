//
//  ResetOTPResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 26/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

// This is a type of response received after making a VerifyOTP call to the server
class ResetOTPResponse : Codable {
    struct Data : Codable {
        var otpVerificationID : String?
    }
    var data : Data?
    var message : String?
    var name : String?
}

//
//  Authentication-Interactor.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 23/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

// This is an Interactor class for Authentication Module
// You can call methods in this class like, AuthenticationInteractor().loginUser(params)
// params should be passed in the form of dictionary eg : [username : "username", password : "password"]
class AuthenticationInteractor {
    
    let networkURL = NetworkURL()
    
    // Login User
    func loginUser(requestBody : [String : Any]) -> LoginResponse {
        let method = "POST"
        let body = requestBody
        let url: URL = networkURL.login()
        let responseData : LoginResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    // Register Rider
    func registerUser(requestBody : [String : Any]) -> RegisterResponse {
        let method = "POST"
        let body = requestBody
        let url: URL = networkURL.register()
        let responseData : RegisterResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    // Forgot Password
    func forgotPassword(requestBody : [String : Any]) -> ForgotPasswordResponse {
        let method = "POST"
        let body = requestBody
        let url: URL = networkURL.forgotPassword()
        let responseData : ForgotPasswordResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    // Reset Password
    func resetPassword(requestBody : [String : Any], requestHeaders : [String : Any]) -> ResetPasswordResponse {
        let method = "POST"
        let body = requestBody
        let headers = requestHeaders
        let url: URL = networkURL.resetPassword()
        let responseData : ResetPasswordResponse = NetworkManager.request(url: url, method: method, body: body, headers: headers)!
        return responseData
    }
    
    // Verify OTP
    func verifyOTP(requestBody : [String : Any]) -> VerifyOTPResponse {
        let method = "POST"
        let body = requestBody
        let url = networkURL.verifyOTP()
        let responseData : VerifyOTPResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    // Reset OTP
    func resetOTP(requestBody : [String : Any]) -> ResetOTPResponse {
        let method = "POST"
        let body = requestBody
        let url = networkURL.resetOTP()
        let responseData : ResetOTPResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    // Refresh Token
    func refreshTokens(requestBody : [String : Any]) -> RefreshTokenResponse {
        let method = "POST"
        let body = requestBody
        let url = networkURL.refreshTokens()
        let responseData : RefreshTokenResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    // Terminate Account
    func terminateAccount(requestBody : [String : Any]) -> TerminateAccountResponse {
        let method = "POST"
        let body = requestBody
        let url = networkURL.terminateAccount()
        let responseData : TerminateAccountResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
}

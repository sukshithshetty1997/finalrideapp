//
//  NetworkURL.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 09/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class NetworkURL {
    let baseURL: URL = URL(string: "https://rideapp-robosoft.herokuapp.com/api")!
}

// Authentication Module
extension NetworkURL {
    
    func login() -> URL {
        return URL(string: "\(self.baseURL)/auth/login")!
    }
    
    func register() -> URL {
        return URL(string: "\(self.baseURL)/auth/register")!
    }
    
    func forgotPassword() -> URL {
        return URL(string: "\(self.baseURL)/auth/forgot-password")!
    }
    
    func resetPassword() -> URL {
        return URL(string: "\(self.baseURL)/auth/reset-password")!
    }
    
    func verifyOTP() -> URL {
        return URL(string: "\(self.baseURL)/auth/otp/verify")!
    }
    
    func resetOTP() -> URL {
        return URL(string: "\(self.baseURL)/auth/otp/reset")!
    }
    
    func refreshTokens() -> URL {
        return URL(string: "\(self.baseURL)/auth/oauth/refresh")!
    }
    
    func terminateAccount() -> URL {
        return URL(string: "\(self.baseURL)/auth/terminate")!
    }
}

// Trips Module
extension NetworkURL {
    func addTrip() -> URL {
        return URL(string: "\(self.baseURL)/trip")!
    }
    
    func getTrips(_ requestQueries: [String: Any]) -> URL {
        var url = "\(self.baseURL)/trip?"
        for (key, value) in requestQueries {
            url.append(contentsOf: "\(key)=\(value)&&")
        }
        return URL(string: url)!
    }
    
    func getTrip(_ tripID: String) -> URL {
        return URL(string: "\(self.baseURL)/trip/\(tripID)")!
    }
    
    func editTrip(_ tripID: String) -> URL {
        return URL(string: "\(self.baseURL)/trip/\(tripID)")!
    }
    
    func deleteTrip(_ tripID: String) -> URL {
        print(tripID)
        return URL(string: "\(self.baseURL)/trip/\(tripID)")!
    }
    
}

// Rider's Profile
extension NetworkURL {
    
    func getProfile() -> URL {
        return URL(string: "\(self.baseURL)/rider")!
    }
    
    func getRiderProfile(_ riderID: String) -> URL {
        return URL(string: "\(self.baseURL)/rider?_id=\(riderID)")!
    }
    
    func updateRiderProfile() -> URL {
        return URL(string: "\(self.baseURL)/rider")!
    }
    func addRiderProfileImage() -> URL{
        return URL(string: "\(self.baseURL)/rider/avatar")!
    }
}

// Dealer's Module
extension NetworkURL {
    
    func addDealer() -> URL {
        return URL(string: "\(self.baseURL)/dealer")!
    }
    
    func getDealer(_ dealerID: String) -> URL {
        return URL(string: "\(self.baseURL)/dealer/\(dealerID)")!
    }
    
    func getDealers(_ requestQueries: [String: Any]) -> URL {
        var url = "\(self.baseURL)/dealer?"
        for (key, value) in requestQueries {
            url.append(contentsOf: "\(key)=\(value)&&")
        }
        return URL(string: url)!
    }
    
}

// Service Module
extension NetworkURL {
    
    func addService() -> URL {
        return URL(string: "\(self.baseURL)/service")!
    }
    
    func getService(_ serviceID: String) -> URL {
        return URL(string: "\(self.baseURL)/service/\(serviceID)")!
    }
    
}

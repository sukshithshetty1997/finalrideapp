//
//  NetworkManager.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 23/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class NetworkManager {
    
    static let BASE_URL = "https://rideapp-robosoft.herokuapp.com/api"
    static private let jsonDecoder = JSONDecoder()
    
    static func request<T : Codable>(url: URL, method : String, body : [String : Any]?, headers : [String : Any]?) -> T? {
        
        // Prepare the HTTP Session
        let httpSession = URLSession.shared
        var httpRequest = URLRequest(url: url)
        var httpResponseData = Data()
        httpRequest.httpMethod = method
        httpRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        httpRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let dispatchSemaphore = DispatchSemaphore(value: 0)
        
        // Safely unwrap headers and append the data to URL
        if let headers = headers {
            print(headers)
            for (key,value) in headers {
                httpRequest.setValue(value as? String, forHTTPHeaderField: key)
            }
        }
        
        // Safely unwrap body and append the data to httpBody
        if let body = body {
            do {
                httpRequest.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
                return nil
            }
        }
        
        print(httpRequest)
        let task = httpSession.dataTask(with: httpRequest as URLRequest) { data, response, error in
            guard error == nil else { print(error!.localizedDescription); return }
            print(response!)
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print("Dictionary")
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print("Array")
                    print(object)
                } else {
                    print("JSON is invalid")
                }
            }
            catch {
                
                debugPrint(error.localizedDescription)
                }
            guard data != nil else { print("no data was fetched"); return }
            httpResponseData = data!
            dispatchSemaphore.signal()
        
        }
        
        task.resume()
        _ = dispatchSemaphore.wait(timeout: DispatchTime.distantFuture)
        
        // Parse the received JSON to required type and send back the data
      let parsedJson : T = try! jsonDecoder.decode(T.self, from: httpResponseData)
        return parsedJson
    }
}

//
//  ServiceInteractor.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class ServiceInteractor {
    
    let networkURL = NetworkURL()
    
    func addService(requestBody : [String : Any], requestHeaders: [String : Any]) -> AddServiceResponse {
        let method = "POST"
        let body = requestBody
        let headers = requestHeaders
        let url = networkURL.addService()
        let responseData : AddServiceResponse = NetworkManager.request(url: url, method: method, body: body, headers: headers)!
        return responseData
    }
    
    func getService(serviceID : String, requestHeaders : [String : Any]) -> GetServiceResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getService(serviceID)
        let responseData : GetServiceResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
}

//
//  GetServiceResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class GetServiceResponse: Codable {
    struct Data : Codable {
        var phone: String!
        var slotDate: String!
        var slotTime: String!
        var vehicleNumber: String!
        var serviceType: String!
        var comments: String!
        var dealer: String!
    }
    var data : Data?
    var message : String?
    var name : String?
}

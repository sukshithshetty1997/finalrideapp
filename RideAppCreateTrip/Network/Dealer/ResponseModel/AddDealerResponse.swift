//
//  AddDealerResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class AddDealerResponse: Codable {
    
    struct Data : Codable {
        var message: String!
    }
    
    var data : Data?
    var message : String?
    var name : String?
}

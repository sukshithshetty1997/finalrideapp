//
//  GetDealersResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class GetDealersResponse: Codable{
    
    struct Dealer: Codable {
        var _id: String?
        var city: String?
        var name: String?
        var phone: String?
        var ratings: Double?
    }
    
    struct Data : Codable{
        var count : Int?
        var dealers: [Dealer]?
    }
    var data : Data?
    var message : String?
    var name : String?
}

//
//  GetDealerResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class GetDealerResponse: Codable {
    
    struct Data : Codable {
        var _id: String?
        var name: String?
        var address: String?
        var phone: String?
        var city: String?
        var state: String?
        var ratings: Double?
    }
    
    var data : Data?
    var message : String?
    var name : String?
}

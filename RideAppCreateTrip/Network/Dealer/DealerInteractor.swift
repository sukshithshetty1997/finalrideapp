//
//  DealerInteractor.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class DealerInteractor {
    
    let networkURL = NetworkURL()
    
    func addDealer(requestBody : [String : Any], requestHeaders: [String : Any]) -> AddDealerResponse {
        let method = "POST"
        let body = requestBody
        let url = networkURL.addDealer()
        let responseData : AddDealerResponse = NetworkManager.request(url: url, method: method, body: body, headers: nil)!
        return responseData
    }
    
    func getDealer(dealerID: String, requestHeaders: [String : Any]) -> GetDealerResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getDealer(dealerID)
        let responseData : GetDealerResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
    func getDealers(requestQueries: [String : Any], requestHeaders: [String : Any]) -> GetDealersResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getDealers(requestQueries)
        let responseData : GetDealersResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
   
}

//
//  EditTripResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 28/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class EditTripResponse : Codable {
    struct Data : Codable {
        var message : String?
    }
    var data : Data?
    var message : String?
    var name : String?
}

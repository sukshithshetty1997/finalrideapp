//
//  GetTripsResponse.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 28/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

class GetTripsResponse : Codable {
    struct Data : Codable {
        var _id : String
        var name : String
        var startDate : String
        var endDate : String
    }
    var data : Data?
    var message : String?
    var name : String?
}


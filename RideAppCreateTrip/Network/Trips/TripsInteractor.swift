//
//  Authentication-Interactor.swift
//  RideAppCreateTrip
//
//  Created by Velan Salis on 23/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation

// This is an Interactor class for Trips Module
// You can call methods in this class like, TripsInteractor().addTrip(params)
// params should bepassed in the form of dictionary eg : [username : "username", password : "password"]
class TripsInteractor {
    
    let networkURL = NetworkURL()
    
    func addTrip(requestBody : [String : Any], requestHeaders: [String : Any]) -> AddTripResponse {
        let method = "POST"
        let body = requestBody
        let headers = requestHeaders
        let url = networkURL.addTrip()
        let responseData : AddTripResponse = NetworkManager.request(url: url, method: method, body: body, headers: headers)!
        return responseData
    }
    
    func getTrips(requestQueries : [String : Any], requestHeaders : [String : Any]) -> GetTripsResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getTrips(requestQueries)
        let responseData : GetTripsResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
    func getTrip(tripID : String, requestHeaders : [String : Any]) -> GetTripResponse {
        let method = "GET"
        let headers = requestHeaders
        let url = networkURL.getTrip(tripID)
        let responseData : GetTripResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
    func editTrip(tripID : String, requestHeaders : [String : Any]) -> EditTripResponse {
        let method = "PUT"
        let headers = requestHeaders
        let url = networkURL.editTrip(tripID)
        let responseData : EditTripResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
    func deleteTrip(tripID : String, requestHeaders : [String : Any]) -> DeleteTripResponse {
        let method = "DELETE"
        let headers = requestHeaders
        let url = networkURL.deleteTrip(tripID)
        let responseData : DeleteTripResponse = NetworkManager.request(url: url, method: method, body: nil, headers: headers)!
        return responseData
    }
    
}

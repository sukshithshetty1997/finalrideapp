//
//  PersonCell.swift
//  Gallery
//
//  Created by Sukshith Shetty on 20/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
protocol PhotoCellDelegate: class {
    func delete(cell: PersonCell)
}

class PersonCell: UICollectionViewCell {
    
    @IBOutlet var delete: UIVisualEffectView!
    @IBOutlet var imageView: UIImageView!
    weak var delegate: PhotoCellDelegate?

    @IBAction func Deletebutton(_ sender: Any) {
        delegate?.delete(cell: self)}}

//
//  TripInfoModel.swift
//  Gallery
//
//  Created by Sukshith Shetty on 10/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class TripInfoModel: NSObject {
    static let TripInfo = TripInfoModel(TripName: "", Date: "", Home: "", Destination: "")
    var TripName:String;
    var Date:String;
    var Home:String;
    var Destination:String;
    init(TripName:String, Date:String, Home:String, Destination:String){
        self.TripName = TripName
        self.Date = Date
        self.Home = Home
        self.Destination = Destination
    }
}

//
//  ViewController.swift
//  Gallery
//
//  Created by Sukshith Shetty on 20/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class GalleryViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate{

    let navigationControllerObject = CustomNavigationViewController()
    
    let userdefaults = UserDefaults.standard

    var people = [Person]()
    var  cell1 = PersonCell()
    
    var tag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let savepeople = userdefaults.object(forKey: "people") as? Data {
            if let decodepeople = try?
                NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(savepeople) as? [Person] {
                people = decodepeople!
            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarDesign(controller: self, title: "Gallery")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(add))
        navigationItem.rightBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return people.count
        
    }

    @objc func tapped(){
        if let indexPaths = collectionView?.indexPathsForVisibleItems{
            for indexPath in indexPaths {
                if let cell = collectionView?.cellForItem(at: indexPath) as? PersonCell {
                    cell.delete.isUserInteractionEnabled = false
                    cell.delete.isHidden = true
                }
            }
        }
        navigationItem.leftBarButtonItem = navigationItem.backBarButtonItem
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
  
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Person", for: indexPath) as? PersonCell else {
            fatalError("an Error occured")
        }
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(longgpress(sender: )))
        cell1 = cell
        cell.addGestureRecognizer(longpress)
        let person = people[indexPath.item]
        self.save()
        let path = getDocumentsDirectory().appendingPathComponent(person.image)
        cell.imageView.image = UIImage(contentsOfFile: path.path)
        cell.delete.layer.cornerRadius = cell.delete.bounds.width / 2
        cell.delete.layer.masksToBounds = true
        cell.delegate = self
        cell.delete.isUserInteractionEnabled = false
        cell.delete.isHidden = true
        return cell}
    
    
    @objc func longgpress(sender: UILongPressGestureRecognizer){
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(tapped))
        navigationItem.rightBarButtonItem?.isEnabled = false
        if let indexPaths = collectionView?.indexPathsForVisibleItems{
            for indexPath in indexPaths {
                if let cell = collectionView?.cellForItem(at: indexPath) as? PersonCell {
                    cell.delete.isUserInteractionEnabled = true
                    cell.delete.isHidden = false
                }
            }
        }
    }


    
    @objc func add(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }

    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let Image = info[.editedImage] as? UIImage else { return }
        let imageName = UUID().uuidString
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
        if let jpegData = Image.jpegData(compressionQuality: 0.8){
            try? jpegData.write(to: imagePath)
        }
        let person = Person(image: imageName)
        people.append(person)
        save()
        collectionView.reloadData()
        dismiss(animated: true)
        
    }
    func getDocumentsDirectory() -> URL
    {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func save(){
        if let savedData = try? NSKeyedArchiver.archivedData(withRootObject: people, requiringSecureCoding: false){
            userdefaults.set(savedData, forKey: "people")
        }
    }
    
    @objc func transitionBackTo(){
        self.dismiss(animated: true, completion: nil)
    }
    
//    @objc func cancel(){
//
//    }
//
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        let person = people[indexPath.item]
        let path = getDocumentsDirectory().appendingPathComponent(person.image)
      //  cell.imageView.image = UIImage(contentsOfFile: path.path)
        let myImageViewPage = self.storyboard?.instantiateViewController(withIdentifier: "DisplayImage") as! DisplayImage
        myImageViewPage.selectedImage =  UIImage(contentsOfFile: path.path)!
        self.navigationController?.pushViewController(myImageViewPage, animated: true)
       // self.navigationController?.pushViewController(DisplayImage, animated: <#T##Bool#>)
        //self.performSegue(withIdentifier: "DispayImage", sender: self)
        
    }
}

extension GalleryViewController : PhotoCellDelegate{
    func delete(cell: PersonCell) {
        if let indexPath = collectionView?.indexPath(for: cell){

            people.remove(at: indexPath.item)
            collectionView?.deleteItems(at: [indexPath])
            
        }
    }
}


//
//  Person.swift
//  Gallery
//
//  Created by Sukshith Shetty on 21/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit


class Person: NSObject , NSCoding{
  
    required init?(coder aDecoder: NSCoder) {
        image = aDecoder.decodeObject(forKey: "image") as? String ?? ""
    }
    
    var image: String
    init(image: String) {
        self.image = image
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(image, forKey: "image")
    }
    
}

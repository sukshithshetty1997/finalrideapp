//
//  DisplayImage.swift
//  Gallery
//
//  Created by Sukshith Shetty on 21/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit


class StoreComment: Any {
    static let Data = StoreComment()
    private init(){}
    var StringData:String = ""
}
class DisplayImage: UIViewController {
    
    var navigationControllerObject = CustomNavigationViewController()
    @IBOutlet var CommentTextField: UITextField!

    @IBOutlet var Comment: UIButton!
    @IBOutlet var display: UIImageView!
    @IBOutlet var Liked: UIButton!
    var selectedImage = UIImage()
    var likecount = false
    var flag = false
    var LikeButton:UIBarButtonItem!
    var ShareButton:UIBarButtonItem!
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        print(StoreComment.Data.StringData)
//        backbutton.transparent(controller: self)
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let LikeImage = UIImage(named: "like-3")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        Liked.setImage(LikeImage, for: UIControl.State.normal)
        Liked.tintColor = UIColor.darkGray
        Liked.setTitle("", for: UIControl.State.normal)
        
        
        let btn = UIButton()
        btn.setImage(UIImage(named: "send-button"), for: .normal)
        btn.frame.size = CGSize(width: 30, height: 30)
        btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 3)
        btn.addTarget(self, action: #selector(Send), for: UIControl.Event.touchDown)
        CommentTextField.rightViewMode = .always
        CommentTextField.rightView = btn
        
        CommentTextField.isHidden = true
        
        self.display.image = selectedImage

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarLeftRightDesign(controller: self, title: "Galary", rightImages: [UIImage(named: "share")!, UIImage(named: "like-4")!])
        navigationControllerObject.transparent(controller: self)
        
        self.navigationController?.navigationBar.backgroundColor = UIColor(rgb: 0x141414)
        self.navigationController?.navigationBar.alpha = 0.5
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(rgb: 0x141414)
        UIApplication.shared.statusBarView?.alpha = 0.5
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.alpha = 1.0
        UIApplication.shared.statusBarView?.alpha = 0.0
    }
    
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }

    @objc func navigationRightItem2() {
        liked()
    }
    
    @objc func navigationRightItem1() {
        print("Share")
        let image = selectedImage
        let activity = UIActivityViewController(activityItems: [image], applicationActivities: nil)
        present(activity, animated: true, completion: nil)
    }
    
    func liked() {
        likecount = !likecount
        let LikeImage = UIImage(named: "like-3")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)

        Liked.setImage(LikeImage, for: UIControl.State.normal)

        if likecount {
            Liked.tintColor = UIColor.red
            Liked.setTitle("Liked", for: UIControl.State.normal)
            Liked.setTitleColor(UIColor.blue, for: UIControl.State.normal)
        }
        else{
            Liked.tintColor = UIColor.darkGray
            Liked.setTitle("", for: UIControl.State.normal)
        }
    }
    
    @objc func Send(){
        StoreComment.Data.StringData = CommentTextField.text!
        CommentTextField.text = ""
        CommentButton(self)
    }

    @IBAction func likeButtonTapped(_ sender: Any) {
        liked()
    }
    
//    @objc func Share(){
//        let image = selectedImage
//        let activity = UIActivityViewController(activityItems: [image], applicationActivities: nil)
//        present(activity, animated: true, completion: nil)
//        }
//
    @IBAction func CommentButton(_ sender: Any) {
   
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: UIView.AnimationOptions.transitionFlipFromBottom, animations: {
                self.CommentTextField.isHidden = !self.CommentTextField.isHidden
                }, completion: nil)
    }
}

// Not used //
extension UIImage {
    

    static func coloredImage(image: UIImage?, color: UIColor) -> UIImage? {
        
        guard let image = image else {
            return nil
        }
        
        let backgroundSize = image.size
        UIGraphicsBeginImageContextWithOptions(backgroundSize, false, UIScreen.main.scale)
        
        let ctx = UIGraphicsGetCurrentContext()!
        
        var backgroundRect=CGRect()
        backgroundRect.size = backgroundSize
        backgroundRect.origin.x = 0
        backgroundRect.origin.y = 0
        
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        ctx.setFillColor(red: r, green: g, blue: b, alpha: a)
        ctx.fill(backgroundRect)
        
        var imageRect = CGRect()
        imageRect.size = image.size
        imageRect.origin.x = (backgroundSize.width - image.size.width) / 2
        imageRect.origin.y = (backgroundSize.height - image.size.height) / 2
        
        // Unflip the image
        ctx.translateBy(x: 0, y: backgroundSize.height)
        ctx.scaleBy(x: 1.0, y: -1.0)
        
        ctx.setBlendMode(.destinationIn)
        ctx.draw(image.cgImage!, in: imageRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    static func multiplyImageByConstantColor(image:UIImage,color:UIColor) -> UIImage{
        
        let backgroundSize = image.size
        UIGraphicsBeginImageContext(backgroundSize)
        
        guard let ctx = UIGraphicsGetCurrentContext() else {return image}
        
        var backgroundRect=CGRect()
        backgroundRect.size = backgroundSize
        backgroundRect.origin.x = 0
        backgroundRect.origin.y = 0
        
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        ctx.setFillColor(red: r, green: g, blue: b, alpha: a)
        
        // Unflip the image
        ctx.translateBy(x: 0, y: backgroundSize.height)
        ctx.scaleBy(x: 1.0, y: -1.0)
        ctx.clip(to: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height), mask: image.cgImage!)
        ctx.fill(backgroundRect)
        
        
        var imageRect=CGRect()
        imageRect.size = image.size
        imageRect.origin.x = (backgroundSize.width - image.size.width)/2
        imageRect.origin.y = (backgroundSize.height - image.size.height)/2
        
        
        ctx.setBlendMode(.multiply)
        ctx.draw(image.cgImage!, in: imageRect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}

//
//  TripViewController.swift
//  Gallery
//
//  Created by Sukshith Shetty on 25/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class TripViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{
  
    let navigationControllerObject = CustomNavigationViewController()
    
    var tripCoreData = [TripData]()
    var createdTripInfo: TripData?
    var mileStoneData = [MileStone]()
    var ridersData = [Riders]()
    var selectedCellTripIndex = Int()
    //var TripInfo = TripInfoModel(TripName: "Shimla Walk", Date: "15 May, 2018", Home: "Udupi", Destination: "Shimla")

    @IBOutlet var Date: UILabel!
    @IBOutlet var TripName: UILabel!
    @IBOutlet var Destination: UILabel!
    @IBOutlet var Home: UILabel!
    @IBOutlet var TripView: CustomView!
    @IBOutlet var UICollectionView1: UICollectionView!
    @IBOutlet var Map: MKMapView!
    var DestinationMap:String = SingletonClass.Data.Destination
    var SourceMap:String = SingletonClass.Data.Home
    var locationManger = CLLocationManager()
    var sourcepin:MKAnnotation!
    var destinationpin:MKAnnotation!
    var route:MKRoute!
    var LineView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBarDesign(controller: self, title: "Trip Summary")
        dataStorageInCoreData()
        UICollectionView1.delegate = self
        UICollectionView1.dataSource = self
        TripName.text = createdTripInfo?.name
        Destination.text = createdTripInfo?.destination
        Home.text = createdTripInfo?.location
        Date.text = createdTripInfo?.startDate
    
    let tapMap = UITapGestureRecognizer(target: self, action: #selector(OpenMap))
    self.Map.addGestureRecognizer(tapMap)
        MapOn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarDesign(controller: self, title: "Trip Summary")
    }
    
    func dataStorageInCoreData() {
        print(selectedCellTripIndex)
//        selectedCellTripIndex = tripViewControllerObject.selectedCellTripIndex
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
        createdTripInfo = tripCoreData[selectedCellTripIndex]
        
        guard let trip = createdTripInfo
            else { return }
       
        if createdTripInfo?.riders?.allObjects != nil {
            ridersData = createdTripInfo?.riders?.allObjects as! [Riders]
        }
    }
    
    @objc func OpenMap(){
        let MapDest = self.storyboard?.instantiateViewController(withIdentifier: "MapDirectionsViewController") as! MapDirectionsViewController
        self.navigationController?.pushViewController(MapDest, animated: true)
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "MapNavigationController") as! UINavigationController
//        self.navigationController?.present(dest, animated: true, completion: nil)
    }
    @objc func transitionBackTo(){
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
//        self.navigationController?.present(dest, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func EnterGallery(_ sender: Any) {
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
//        self`.navigationController?.pushViewController(dest, animated: true)
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "GalleryNavigate") as! UINavigationController
        self.navigationController?.present(dest, animated: true, completion: nil)
    }
    override func viewDidLayoutSubviews() {
        LineView.frame = CGRect(x: Home.frame.origin.x + 5 + Home.frame.size.width , y: Home.frame.size.height/2 + Home.frame.origin.y, width: Destination.frame.origin.x - 35 - LineView.frame.origin.x, height: 1)
        LineView.backgroundColor = UIColor.darkGray
        self.TripView.addSubview(LineView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ridersData.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Contacts", for: indexPath) 
        return cell
    }
 
}
extension TripViewController: CLLocationManagerDelegate, MKMapViewDelegate{
    func MapOn(){
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        locationManger.distanceFilter = kCLDistanceFilterNone
        Map.showsUserLocation = true
        Map.delegate = self
        getAddress()
    }
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString((self.createdTripInfo?.destination)!) { (placemarks, error) in
            guard let placemarks = placemarks, let destination = placemarks.first?.location
                else {
                    print("No Location Found")
                    return
            }
            geoCoder.geocodeAddressString((self.createdTripInfo?.location)!) { (placemarks, error) in
                guard let placemarks = placemarks, let source = placemarks.first?.location
                    else {
                        print("No Location Found")
                        return
            }
            self.mapThis(destinationCord: destination.coordinate, sourcrCord: source.coordinate)
        }
    }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
    
    func mapThis(destinationCord : CLLocationCoordinate2D, sourcrCord: CLLocationCoordinate2D) {

        //let souceCordinate = (locationManger.location?.coordinate)!
        sourcepin = customPin(pinTitle: (createdTripInfo?.location)!, pinSubTitle: (createdTripInfo?.location)!, location: sourcrCord)
        destinationpin = customPin(pinTitle: (createdTripInfo?.destination)!, pinSubTitle: (createdTripInfo?.destination)!, location: destinationCord)
        Map.removeAnnotation(destinationpin)
        self.Map.addAnnotation(sourcepin)
        self.Map.addAnnotation(destinationpin)
        let soucePlaceMark = MKPlacemark(coordinate: sourcrCord
        )
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
        
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print("Something is wrong", error)
                }
                return
            }
            if self.route != nil{
                self.Map.removeOverlay(self.route.polyline)
            }
            self.route = response.routes[0]
            self.Map.addOverlay(self.route.polyline)
            self.Map.setVisibleMapRect(self.route.polyline.boundingMapRect, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        render.lineWidth = 5
        return render
    }
    
    
}


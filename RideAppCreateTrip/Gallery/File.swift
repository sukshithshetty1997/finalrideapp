//
//  File.swift
//  Gallery
//
//  Created by Sukshith Shetty on 25/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CustomView: UIView {
    @IBInspectable var CornerRadius :CGFloat = 0
    @IBInspectable var shadowRadius : CGFloat = 0
    @IBInspectable var shadowColor : UIColor = UIColor.clear
    @IBInspectable var shadowOpacity: Float = 1
    @IBInspectable var bordercolor: UIColor = .clear
    @IBInspectable var borderwidth: CGFloat =  0
    @IBInspectable var shadowOffset : CGSize = .zero
    //@IBInspectable var borderWidth : CGFloat = 0
    //@IBInspectable var borderColor : UIColor = UIColor.clear
    override func awakeFromNib() {
        super.awakeFromNib()
        styleView()
    }
    
    override func prepareForInterfaceBuilder() {
        styleView()
    }
    func styleView() {
        layer.cornerRadius = CornerRadius
        layer.shadowRadius = shadowRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.borderColor = bordercolor.cgColor
        layer.borderWidth = borderwidth
    }
    
    
}

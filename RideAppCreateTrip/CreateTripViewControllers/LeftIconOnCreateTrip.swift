//
//  LeftIconOnCreateTrip.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 15/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//


import UIKit

class ServerStore {
    static var shareInstance = ServerStore()
    var name = ""
    var source = [Double]()
    var destination = [Double]()
    var milestones = [[Double]]()
    var startDate = ""
    var startTime = ""
    var endDate = ""
    var invitedUsers = [String]()
}

extension CreateTripViewController: UITextFieldDelegate {
    
    var textFieldButton: UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "gps-fixed-indicator-2"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(currentLocationTextField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(40), height: CGFloat(30))
        //button.addTarget(self, action: #selector(self.retrieveCurrentLocation), for: .touchUpInside)
        return button
    }
    
    
    var textFieldStartDateButton: UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "Shape"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(startDateTextField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(40), height: CGFloat(30))
        button.addTarget(self, action: #selector(self.popUpStartDatePicker), for: .touchUpInside)
        
        return button
    }
    var textFieldEndDateButton: UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "Shape"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(endDateTextField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(40), height: CGFloat(30))
        button.addTarget(self, action: #selector(self.popUpEndDatePicker), for: .touchUpInside)
        
        return button
    }
    var textFieldStartTimeButton: UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "clock"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(startTimeTextField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(40), height: CGFloat(30))
        button.addTarget(self, action: #selector(self.popUpTimePicker), for: .touchUpInside)
        
        return button
    }
    
    func popUpPicker() {
        picker.backgroundColor = UIColor.gray
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolbar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 250, width: UIScreen.main.bounds.size.width, height: 50))
        toolbar.barStyle = .blackTranslucent
        toolbar.tintColor = UIColor(rgb: 0xED7F2C)
        self.view.addSubview(toolbar)
    }
    
    @objc func cancel(){
        toolbar.removeFromSuperview()
        picker.removeFromSuperview()
    }
    
    @objc func popUpStartDatePicker() {
        cancel()
        picker.datePickerMode = UIDatePicker.Mode.date
        popUpPicker()
        //toolbar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(setStartDate))]
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(setStartDate)), UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))], animated: true)
    }
    
    @objc func setStartDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.startDateTextField.text = dateFormatter.string(from: picker.date)
        cancel()
    }
    
    @objc func popUpEndDatePicker() {
        cancel()
        picker.datePickerMode = UIDatePicker.Mode.date
        popUpPicker()
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(setEndDate)), UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))], animated: true)
    }
 
    @objc func setEndDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.endDateTextField.text = dateFormatter.string(from: picker.date)
        cancel()
    }
    
    @objc func popUpTimePicker() {
        cancel()
        picker.datePickerMode = UIDatePicker.Mode.time
        popUpPicker()
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(setTime)), UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancel))], animated: true)
    }
    
    @objc func setTime() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        self.startTimeTextField.text = dateFormatter.string(from: picker.date)
        cancel()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 10 {
            popUpStartDatePicker()
        } else if textField.tag == 11 {
            popUpEndDatePicker()
        } else if textField.tag == 12 {
            popUpTimePicker()
        } else if textField.tag == 1 {
            let navigationCallVc = storyboard?.instantiateViewController(withIdentifier: "DestinationSearchViewController") as! DestinationSearchViewController
            navigationCallVc.delegate = self
            self.present(navigationCallVc, animated: true, completion: nil)
            
        }
    }

//    @objc func retrieveCurrentLocation() {
//        print("Current Location")
//        let navigationCallVc = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//        self.navigationController?.pushViewController(navigationCallVc, animated: true)
//
//    }
    
    func leftAndRightViews() {
        currentLocationTextField.leftView = textFieldButton
        currentLocationTextField.leftViewMode = .always
        milestoneLocationTextField.leftView = textFieldButton
        milestoneLocationTextField.leftViewMode = .always
        
        startDateTextField.rightView = textFieldStartDateButton
        endDateTextField.rightView = textFieldEndDateButton
        startTimeTextField.rightView = textFieldStartTimeButton
        
        startDateTextField.rightViewMode = .always
        endDateTextField.rightViewMode = .always
        startTimeTextField.rightViewMode = .always
    }
}

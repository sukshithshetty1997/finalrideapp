//
//  StoredCreateTripData.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 17/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation


struct CreateTripData {
    var destinationPlace: String
    var currentLocation: String
    var nameOfTrip: String
    var startDate: String
    var endDate: String
    var startTime: String
    var invitedPeople: [String] = []
    var addedMilestone: [MilestoneViewData] = []
    
}

struct MilestoneViewData {
    var fromPlace: String
    var toPlace: String 
}

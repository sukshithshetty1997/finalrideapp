//
//  WelComeAboardViewController.swift
//  RideAppCreateTrip
//
//  Created by Sukshith Shetty on 12/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class WelComeAboardViewController: UIViewController {
    
    let navigationControllerObject = CustomNavigationViewController()
    let defaults = UserDefaults.standard
    override func viewWillAppear(_ animated: Bool) {
    navigationControllerObject.navigationBackButtonDesign(controller: self)
        self.tabBarController?.tabBar.isHidden = false
        //self.tabBarController?.tabBar.unselectedItemTintColor = UIColor(rgb: 0xFED2AE)
        super.viewWillAppear(animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func transitionBackTo(){
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        self.presentLeft(dest)
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
//        self.navigationController?.present(dest, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.navigationBar.isTranslucent = false
        super.viewWillDisappear(animated)
    }

    @IBAction func CreateTrip(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripViewController") as! CreateTripViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
    

}

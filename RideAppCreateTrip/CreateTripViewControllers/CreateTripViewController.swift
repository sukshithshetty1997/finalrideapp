//
//  CreateTripViewController.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 16/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation



class CreateTripViewController: UIViewController, FromHomeContact, DestinationPlaceName {
    
    //MARK: Outlets
    @IBOutlet weak var whereToGoTextField: UITextField!
    @IBOutlet weak var inviteMilestoneTableView: UITableView!
    @IBOutlet weak var nameOfTripTextField: UITextField!
    @IBOutlet weak var currentLocationTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var startTimeTextField: UITextField!
    
    @IBOutlet weak var mileStoneView: UIView!
    @IBOutlet weak var topSpaceOfTableView: NSLayoutConstraint!
    @IBOutlet weak var recommendationView: UIView!
    
    @IBOutlet weak var tableViewHeightLayout: NSLayoutConstraint!
    
    @IBOutlet weak var milestoneFromTextField: UITextField!
    @IBOutlet weak var milestoneLocationTextField: UITextField!
    @IBOutlet weak var milestoneToTextFeild: UITextField!
    
    @IBOutlet weak var milestoneCountLabel: UILabel!
    @IBOutlet weak var milestoneViewTrailingLayout: NSLayoutConstraint!
    @IBOutlet weak var milestoneViewLeadingLayout: NSLayoutConstraint!
    @IBOutlet weak var milestoneViewTopLayout: NSLayoutConstraint!
    
    //MARK: Variable Declarations
    let navigationControllerObject = CustomNavigationViewController()
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var tripCoreData: [TripData]?
    var createdTripInfo: TripData?
    var riderdata = [Riders]()
    var milestoneDict = [[String: String]]()
    var riderDict = [[String: String]]()
    var toolbar = UIToolbar()
    var picker = UIDatePicker()
    
    var cityName: String = ""
    var countryName: String = ""
    var selectedContacts = [String]()
    var selectedIndex: IndexPath?
    var fromHomeContact = false
    var milestoneCellClicked = false
    var line: UIView?
    var constraint: NSLayoutConstraint?
    var closeButtonTapped: Bool = false
    var invitedImageView: [UIImageView] = []
    var closeButton: [UIButton] = []
    var removeContact = false
    var removeRiderIndex: [Int]? = []
    var milestoneCount: Int = 0
    var indexPositionOfContact: Int = 0
    var ridersAreInvited = false
    var milestoneClickView = false
    var destination: String?
    var sourceCoordinate: CLLocationCoordinate2D?
    var destinationCoordinate: CLLocationCoordinate2D?
    var modifyTrip = String()
    var milestoneData = [MilestoneViewData]()
    var createTripData = [CreateTripData]()
    let list = ["Invite Other riders", "Add a milestone"]
    let img = ["user(3)", "Group"]
    
    //MARK: View Life Cycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        SingletonClass.Data.Home = "Haryana"
        SingletonClass.Data.Destination = "Mumbai"
        navigationControllerObject.navigationBarDesign(controller: self, title: "Create a Trip")
        self.tabBarController?.tabBar.isHidden = true
        
        //LeftIconOnCreateTrip.swift
        leftAndRightViews()
    
        topSpaceOfTableView.constant = 19
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fromTextField.text = cityName
        navigationControllerObject.navigationBarDesign(controller: self, title: "Create a Trip")
        mileStoneView.setGradientBackground(colorTop: UIColor(rgb: 0xF99C56), colorBottom: UIColor.white)
    }
    
    @IBAction func createTripDoneButtonClicked(_ sender: Any) {
        
        //storing all data to pass to next view controller
        if saveUserEnteredData(){
            let tripSummDest = self.storyboard?.instantiateViewController(withIdentifier: "TripSummaryViewController") as! TripSummaryViewController
            tripSummDest.milestoneDict = self.milestoneDict
            tripSummDest.riderDict = self.riderDict
            tripSummDest.removeRiderIndex = self.removeRiderIndex ?? []
            tripSummDest.modifyTrip = self.modifyTrip
            self.navigationController?.pushViewController(tripSummDest, animated: true)
        }
    }
    
    @IBAction func startTimeClicked(_ sender: Any) {
        recommendationView.isHidden = false
        topSpaceOfTableView.constant = 132
        self.inviteMilestoneTableView.reloadData()
        tableViewHeightLayout.constant = 87
        self.inviteMilestoneTableView.separatorColor = UIColor.clear
        separatorLineForTableView()
    }
    
    @IBAction func milestoneCloseButtonTapped(_ sender: Any) {
        //calculating distance of each milestone
        if milestoneToTextFeild.text != "" {
//            let dataMilestone = MilestoneViewData(fromPlace: fromLocation, toPlace: toLocation)
//            self.milestoneData.append(dataMilestone)
            locationLatitudeLongitude()
        } else {
            milestoneCount -= 1
        }
        
        mileStoneView.isHidden = true
        closeButtonTapped = true
        milestoneCellClicked = false
        self.inviteMilestoneTableView.reloadData()
    }
}

//MARK: Function Implementation
extension CreateTripViewController {
    
    func showAlertMessage(_ message: String) {
        let alert = UIAlertController(title: "Alert!!", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveUserEnteredData() -> Bool {
        guard
            let destinationPlace = destination,
            let currentLocation = fromTextField.text?.capitalized,
            let nameOfTrip = nameOfTripTextField.text?.capitalized,
            let startDate = startDateTextField.text,
            let endDate = endDateTextField.text,
            let startTime = startTimeTextField.text
            else {
                return false
        }
        if currentLocation == "" || nameOfTrip == "" || startDate == "" || endDate == "" || startTime == "" {
            showAlertMessage("Please Enter all the Fields!!!")
            return false
        } else {
            //Using Core Data
            let dict = ["destination": destinationPlace, "location": currentLocation, "name": nameOfTrip, "startDate": startDate, "endDate": endDate, "startTime": startTime]
            
            if modifyTrip == "modifyTrip" {
                guard let trip = createdTripInfo
                    else {
                    return false
                }
                CoreDataBaseHelper.shareInstance.modifyTrip(object: dict, trip: trip)
            } else {
                CoreDataBaseHelper.shareInstance.saveTrip(object: dict)
            }
            ServerStore.shareInstance.name = nameOfTrip
            ServerStore.shareInstance.startDate = startDate
            ServerStore.shareInstance.startTime = startTime
            ServerStore.shareInstance.endDate = endDate
            return true
        }
    }
    
    func setTableView(fromHomeContact: Bool, selectedContacts: [String]) {
        riderDict = []
        removeRiderIndex = []
        self.fromHomeContact = fromHomeContact
        self.selectedContacts = selectedContacts
        print(self.fromHomeContact, self.selectedContacts)
        
        for rider in selectedContacts {
            riderDict.append(["name": rider])
        }
        print(riderDict)
        tableViewHeightLayout.constant = 177
        self.inviteMilestoneTableView.reloadData()
    }
    
    func selectedDestination(place: String) {
        print(place)
        whereToGoTextField.text = place
        let complete_string: String = place
        let string_arr =  complete_string.characters.split {$0 == ","}.map(String.init)
        destination = string_arr.last!
        destination = destination?.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    @objc func transitionBackTo() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func separatorLineForTableView() {
        let px = 1 / UIScreen.main.scale
        let frame = CGRect(x: 0, y: 0, width: self.inviteMilestoneTableView.frame.size.width, height: px )
        line = UIView(frame: frame)
        self.inviteMilestoneTableView.tableHeaderView = line
        self.inviteMilestoneTableView.separatorColor = UIColor(rgb: 0xB4B3B3)
        line?.backgroundColor = self.inviteMilestoneTableView.separatorColor
    }
    
    func milestoneCellTapped() {
        mileStoneView.isHidden = false
        line?.backgroundColor = UIColor.clear
        //        guard let count = milestoneCount else {
        //            return
        //        }
        milestoneCountLabel.text = "Milestone \(milestoneCount)"
        //        milestoneFromTextField.text = cityName
        //        milestoneLocationTextField.text = cityName
        //        print("Clicking Milestone \(milestoneCellClicked)")
        self.inviteMilestoneTableView.reloadData()
    }
    
    func getCoordinateFrom(address: String, completion: @escaping(_ coordinate: CLLocationCoordinate2D?, _ error: Error?) -> () ) {
        CLGeocoder().geocodeAddressString(address) { completion($0?.first?.location?.coordinate, $1) }
    }
    
    func locationLatitudeLongitude() {
        guard
            let fromLocation = milestoneFromTextField.text,
            let toLocation = milestoneToTextFeild.text
            else {
                return
        }
        getCoordinateFrom(address: fromLocation) { coordinate, error in
            if error != nil {
                return
            } else {
                //at this point `coordinate ` contains a valid coordinate.
                //Put your code to do something with it here
                self.sourceCoordinate = coordinate
                print("resulting coordinate = (\(String(describing: self.sourceCoordinate?.latitude)),\(self.sourceCoordinate!.longitude))")
                _ = self.distanceBetweenLocations()
            }
        }
        getCoordinateFrom(address: toLocation) { coordinate, error in
            if error != nil {
                return
            } else {
                //at this point `coordinate ` contains a valid coordinate.
                //Put your code to do something with it here
                self.destinationCoordinate = coordinate
                print("resulting coordinate = (\(String(describing: self.destinationCoordinate?.latitude)),\(String(describing: self.destinationCoordinate?.longitude)))")
                let milestoneArray = [coordinate?.latitude, coordinate?.longitude] as! [Double]
                ServerStore.shareInstance.milestones.append(milestoneArray)
                print(ServerStore.shareInstance.milestones)
                let distance = self.distanceBetweenLocations()
                
                //CoreData Store
                let time = self.timeToTravel(distance: distance)
                
                let count: String = String(self.milestoneCount)
                let dict = ["milestoneNumber": count, "fromLocation": fromLocation, "toLocation": toLocation, "distance": "\(String(format: "%d", arguments: [distance]))km", "time": time]
                self.milestoneDict.append(dict)
                print(self.milestoneDict)
            }
        }
    }
    
    func  timeToTravel(distance: Int) -> String {
        var time = ""
        var hour: Int = distance / 60
        let hourMins: Float = Float(Float(distance) / 60.0)
        hour = Int(hourMins)
        let minPoints = hourMins.truncatingRemainder(dividingBy: 1)
        let minutes: Int = Int(roundf(minPoints * 60))
        time = "\(hour)h \(minutes)mins"
        return time
    }
    
    func distanceBetweenLocations() -> Int {
        guard let sourceCoordinate = self.sourceCoordinate,
            let destinationCoordinate = self.destinationCoordinate
            else {
                return 0
        }
        let firstLocation = CLLocation(latitude: sourceCoordinate.latitude, longitude: sourceCoordinate.longitude)
        let secondLocation = CLLocation(latitude: destinationCoordinate.latitude, longitude: destinationCoordinate.longitude)
        let distance = Int(firstLocation.distance(from: secondLocation) / 1000)
        //        print("Distance \(String(format: "%d", arguments: [distance])) km")
        return distance
    }
    
    @objc func removeContactFromList(sender: UIButton) {
        let riderPos = closeButton.firstIndex(of: sender)
        removeContact = true
        guard let pos = riderPos else {
            return
        }
        invitedImageView[pos].removeFromSuperview()
        closeButton[pos].removeFromSuperview()
        invitedImageView.remove(at: pos)
        closeButton.remove(at: pos)
        
        selectedContacts.remove(at: pos)
        removeRiderIndex? += [pos]
        removeRiderIndex = removeRiderIndex ?? [pos]
        self.inviteMilestoneTableView.reloadData()
    }
    
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,
                       error)
        }
    }
}

//MARK: extracting CurrentLocation
extension CreateTripViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locationManager.location
        let coordinate: CLLocationCoordinate2D? = currentLocation?.coordinate
        self.sourceCoordinate = coordinate
        ServerStore.shareInstance.source = [currentLocation?.coordinate.latitude, currentLocation?.coordinate.longitude] as! [Double]
//        print(ServerStore.shareInstance.source)
//        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
//        let region = MKCoordinateRegion(center: coordinate!, span: span)
        
        guard let location: CLLocation = manager.location else { return }
        fetchCityAndCountry(from: location) { city, country, error in
            //guard let city = city, let country = country, error == nil else { return }
            if error == nil {
                self.currentLocationTextField.text = city!
                self.milestoneFromTextField.text = city
                self.milestoneLocationTextField.text = city
                self.cityName = city!
            }
            else {
                return
            }
        }
    }
}

//MARK: TableView delegate and datasource
extension CreateTripViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if milestoneCellClicked == true && indexPath.row == 0 {
            //return 260
            return UITableView.automaticDimension
        } else if fromHomeContact == true && indexPath.row == 1 {
            return 87
        } else if recommendationView.isHidden == false && indexPath.row == 1 {
            return 0
        } else {
            return 87
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return(list.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InviteMilestoneTableViewCell
        //cell.textLabel?.text = list[indexPath.row]
        //cell.imageView?.image = UIImage(named: img[indexPath.row])
        cell.cellLabel.text = list[indexPath.row]
        cell.cellImageView.image = UIImage(named: img[indexPath.row])
        
        if ridersAreInvited == false && indexPath.row == 1 {
            cell.isUserInteractionEnabled = false
        } else {
            cell.isUserInteractionEnabled = true
        }
        
        if removeContact == true && indexPath.row == 0 {
            
            if selectedContacts.count == 0 {
                cell.cellLabel.isHidden = false
            }
            
            //            invitedImageView[indexPositionOfContact].removeFromSuperview()
            //            closeButton[indexPositionOfContact].removeFromSuperview()
            
        } else if fromHomeContact == true && indexPath.row == 0 {
            if selectedContacts.count == 0 {
                cell.cellLabel.isHidden = false
            } else {
                cell.cellLabel.isHidden = true
                
                var userPosition = 0
                var closePosition = 36
                let countSeq = Int(selectedContacts.count)
                for i in 0..<countSeq {
                    invitedImageView.append(UIImageView(frame: CGRect(x: userPosition, y: 0, width: 46, height: 46)))
                    invitedImageView[i].image = UIImage(named: "Rider 1")
                    invitedImageView[i].contentMode = .scaleAspectFill
                    
                    /*
                     let closeImageView = UIImageView(frame: CGRect(x: closePosition, y: 6, width: 16, height: 16))
                     closeImageView.image = UIImage(named: "Group-2")
                     closeImageView.contentMode = .scaleAspectFill
                     cell.invitedView.addSubview(closeImageView)
                     */
                    
                    closeButton.append(UIButton(frame: CGRect(x: closePosition, y: 6, width: 16, height: 16)))
                    closeButton[i].setBackgroundImage(UIImage(named: "Group-2"), for: .normal)
                    closeButton[i].contentMode = .scaleAspectFill
                    indexPositionOfContact = i
                    
                    cell.invitedView.addSubview(invitedImageView[i])
                    cell.invitedView.addSubview(closeButton[i])
                    
                    closeButton[indexPositionOfContact].addTarget(self, action: #selector(self.removeContactFromList(sender:)), for: .touchUpInside)
                    
                    userPosition = userPosition + 57
                    closePosition += 57
                }
            }
        } else if fromHomeContact == false && indexPath.row == 0 {
            for view in cell.invitedView.subviews {
                if view != cell.cellLabel {
                    view.removeFromSuperview()
                }
            }
            cell.cellLabel.isHidden = false
        }
        
        let imageLabelTopLayout = cell.imageLabelCellView.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 19)
        let milestoneContainerBottomLayout = cell.milestoneContainerView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -18)
        
        if closeButtonTapped == true && indexPath.row == 0 {
            cell.milestoneHeightLayout.constant = 0
            tableViewHeightLayout.constant = 177
            
            //imageLabelTopLayout.isActive = false
            //milestoneContainerBottomLayout.isActive = false
            //cell.imageLabelVerticalLayout.isActive = true
        }
        
        if milestoneCellClicked == true && indexPath.row == 0 {
            cell.milestoneHeightLayout.constant = 246
            if milestoneCount == 1 && removeContact == false && milestoneClickView == false {
                
                cell.imageLabelVerticalLayout.isActive = false
                imageLabelTopLayout.isActive = true
                milestoneContainerBottomLayout.isActive = true
                
                milestoneViewTopLayout.isActive = false
                milestoneViewLeadingLayout.isActive = false
                milestoneViewTrailingLayout.isActive = false
                cell.milestoneContainerView.addSubview(mileStoneView)
                
                mileStoneView.topAnchor.constraint(equalTo: cell.milestoneContainerView.topAnchor).isActive = true
                mileStoneView.bottomAnchor.constraint(equalTo: cell.milestoneContainerView.bottomAnchor).isActive = true
                mileStoneView.leadingAnchor.constraint(equalTo: cell.milestoneContainerView.leadingAnchor).isActive = true
                mileStoneView.trailingAnchor.constraint(equalTo: cell.milestoneContainerView.trailingAnchor).isActive = true
                mileStoneView.widthAnchor.constraint(equalToConstant: 306).isActive = true
                mileStoneView.heightAnchor.constraint(equalToConstant: 246).isActive = true
                
                milestoneClickView = true
                
            } else if milestoneCount > 1 || milestoneClickView == true {
                //cell.milestoneHeightLayout.constant = 246
                //tableViewHeightLayout.constant = 440
                mileStoneView.isHidden = false
            }
            
            //mileStoneView.removeConstraints(self.mileStoneView.constraints)
            tableViewHeightLayout.constant = 440
        }
        
        removeContact = false
        return(cell)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            ridersAreInvited = true
            self.fromHomeContact = false
            self.invitedImageView.removeAll()
            self.closeButton.removeAll()
            self.inviteMilestoneTableView.reloadData()
            let navigationCallVc = storyboard?.instantiateViewController(withIdentifier: "InviteOtherRidersViewController") as! InviteOtherRidersViewController
            navigationCallVc.delegate = self
            self.navigationController?.pushViewController(navigationCallVc, animated: true)
        } else if indexPath.row == 1 {
            milestoneCount += 1
            selectedIndex = indexPath
            milestoneCellClicked = true
            milestoneCellTapped()
        }
        //inviteMilestoneTableView.reloadData()
    }
    
}





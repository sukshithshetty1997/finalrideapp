//
//  TripSummaryViewController.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 29/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import KeychainSwift

class TripSummaryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    let navigationControllerObject = CustomNavigationViewController()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let tripInteractorObject = TripsInteractor()
    var tripCoreData = [TripData]()
    var createdTripInfo: TripData?
    var mileStoneData = [MileStone]()
    var ridersData = [Riders]()
    var milestoneDict = [[String: String]]()
    var riderDict = [[String: String]]()
    var removeRiderIndex = [Int]()
    var modifyTrip = String()
    var selectedCellTripIndex = Int()
    
    @IBOutlet var Home: UILabel!
    @IBOutlet var Destination: UILabel!
    @IBOutlet var Date: UILabel!
    @IBOutlet var TripName: UILabel!
    @IBOutlet var Time: UILabel!
//    var TripInfo = CreateTripInfoModel(TripName: "Haryana GO", Date: "15-22 Nov, 2019", Home: "Mumbai", Destination: "Haryana", Time: "8:00 am")
    @IBOutlet var UICollectionView1: UICollectionView!
    @IBOutlet var UICollectionView2: UICollectionView!
    @IBOutlet var UICollectionView3: UICollectionView!
    @IBOutlet var Map: MKMapView!
    @IBOutlet weak var mileStoneCollectionViewHeight: NSLayoutConstraint!
    var LineView:UIView = UIView()
    var MileStoneInfo = [Milestone(MilestoneNumber: 1, From: "Mumbai", To: "Pune", Distance: 160, Time: "6 h 55 min"),Milestone(MilestoneNumber: 2, From: "Pune", To: "Raipur", Distance: 350, Time: "8 h 56 min"), Milestone(MilestoneNumber: 3, From: "Raipur", To: "Haryana", Distance: 200, Time: "5 h 19 min"), Milestone(MilestoneNumber: 3, From: "Raipur", To: "Haryana", Distance: 200, Time: "5 h 19 min")]
    var Recommendations:[String] = ["Riding Gear", "Winter Wear", "Drinking Water"]
    @IBOutlet var TripView: ViewClass!
    var DestinationMap:String = "Delhi"
    var locationManger = CLLocationManager()
    var sourcepin:MKAnnotation!
    var destinationpin:MKAnnotation!
    var route:MKRoute!
    var TripInfo: CreateTripInfoModel?
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationLeftRightDesign(controller: self, title: "TripSummary", rightImage: UIImage(named: "Edit")!)
        //navigationControllerObject.navigationBarDesign(controller: self, title: "Trip Summary")
        
        UICollectionView1.reloadData()
        UICollectionView2.reloadData()
        UICollectionView3.reloadData()
        viewDidLayoutSubviews()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // hide the default back buttons
        //self.navigationItem.leftBarButtonItems = []
        self.navigationItem.hidesBackButton = true
        self.mileStoneCollectionViewHeight.constant = 0
        
        //Core data assignament
        dataStorageInCoreData()
     
        TripName.text = TripInfo!.TripName
        Date.text = TripInfo!.Date
        Home.text = TripInfo!.Home
        Destination.text = TripInfo!.Destination
        Time.text = TripInfo!.Time
        
        UICollectionView1.delegate = self
        UICollectionView1.dataSource = self
        UICollectionView2.delegate = self
        UICollectionView2.dataSource = self
        UICollectionView3.delegate = self
        UICollectionView3.dataSource = self
        
        viewDidLayoutSubviews()
        CallMap(MapView: Map)
        MapOn()
    
    }
    
    func dataStorageInCoreData() {
        
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
        selectedCellTripIndex = tripCoreData.endIndex - 1
        createdTripInfo = tripCoreData[selectedCellTripIndex]
        TripInfo = CreateTripInfoModel(TripName: (createdTripInfo?.name)!, Date: (createdTripInfo?.startDate)!, Home: (createdTripInfo?.location)!, Destination: (createdTripInfo?.destination)!, Time: (createdTripInfo?.startTime)!)
        
        guard let trip = createdTripInfo
            else { return }
        
        if modifyTrip == "modifyTrip" {
            CoreDataBaseHelper.shareInstance.deleteAllRider(trip: trip)
            CoreDataBaseHelper.shareInstance.deleteAllMilestone(trip: trip)
        }
        
        for milestone in milestoneDict {
            CoreDataBaseHelper.shareInstance.saveMilestone(object: milestone, trip: trip)
        }
        
        for rider in riderDict {
            CoreDataBaseHelper.shareInstance.saveRiders(object: rider, trip: trip)
        }
        
        if createdTripInfo?.mileStones?.allObjects != nil {
            mileStoneData = createdTripInfo?.mileStones?.allObjects as! [MileStone]
        }
        //        mileStoneData = CoreDataBaseHelper.shareInstance.getMilestoneData()
        
        if removeRiderIndex != [] {
            for index in removeRiderIndex {
                CoreDataBaseHelper.shareInstance.deleteRider(index: index, trip: trip)
            }
        }
        if createdTripInfo?.riders?.allObjects != nil {
            ridersData = createdTripInfo?.riders?.allObjects as! [Riders]
        }
        print(ridersData)
    }
    
    @objc func transitionBackTo(){
        self.PopToCreateTrip()
    }
    
    @objc func PopToCreateTrip(){
        let a = self.navigationController?.viewControllers[1] as! CreateTripViewController
        modifyTrip = "modifyTrip"
        a.modifyTrip = modifyTrip
        a.createdTripInfo = self.createdTripInfo
        self.navigationController?.popToViewController(a, animated: true)
    }
    func CallMap(MapView: MKMapView) {
        let tapMap = UITapGestureRecognizer(target: self, action: #selector(OpenMap))
        tapMap.numberOfTapsRequired = 1
        MapView.addGestureRecognizer(tapMap)
    }
    @objc func OpenMap(){
        let dest = storyboard?.instantiateViewController(withIdentifier: "MapDirectionsViewController") as! MapDirectionsViewController
        dest.selectedCellTripIndex = self.selectedCellTripIndex
       self.navigationController?.pushViewController(dest, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case UICollectionView1:
            //return MileStoneInfo.count
            return mileStoneData.count
        case UICollectionView2:
            return Recommendations.count
        case UICollectionView3:
            return ridersData.count
        default:
            return 0
        }
    }

    @IBAction func CreateTripSuccess(_ sender: Any) {
        tripServerInteractor()
    }
    
    func tripServerInteractor() {
        let name = ServerStore.shareInstance.name
        let source = ServerStore.shareInstance.source
        let destination = ServerStore.shareInstance.destination
        let startDate = ServerStore.shareInstance.startDate
        let startTime = ServerStore.shareInstance.startTime
        let endDate = ServerStore.shareInstance.endDate
        let invitedUsers: [String] = []
        let milestones = ServerStore.shareInstance.milestones
        guard let token = keychain.get(Keys.accessToken)
            else { return }
        if keychain.lastResultCode != noErr {
            print(keychain.lastResultCode)
        }
        let tripRespose = tripInteractorObject.addTrip(requestBody: ["name": name, "source": source, "destination": destination, "milestones": milestones, "startDate": startDate, "startTime": startTime, "endDate": endDate, "invitedUsers": invitedUsers], requestHeaders: ["Authorization": "Bearer \(token)"])

        if let data = tripRespose.data?.message {
            print(data)
            print("Succesful in adding Trip")
            let dest = self.storyboard?.instantiateViewController(withIdentifier: "SuccessTripViewController") as! SuccessTripViewController
            self.navigationController?.pushViewController(dest, animated: true)
        } else if let error = tripRespose.name, let message = tripRespose.message {
            print(error)
            print(message)
            let alert = UIAlertController(title: error, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
    }
    
    
    
    @IBAction func GoToAdContacts(_ sender: Any) {
        
    }
    override func viewDidLayoutSubviews() {
        LineView.frame = CGRect(x: Home.frame.origin.x + 10 + Home.frame.size.width, y: Home.frame.origin.y + self.Home.frame.size.height/2, width: Destination.frame.origin.x - 10 - Home.frame.origin.x - Home.frame.size.width, height: 1)
        LineView.backgroundColor = UIColor.lightGray
        self.TripView.addSubview(LineView)
        
        //Dynamic collection view height
        let height = UICollectionView1.collectionViewLayout.collectionViewContentSize.height
        mileStoneCollectionViewHeight.constant = height
        self.view.layoutIfNeeded()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case UICollectionView1:
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "Milestone", for: indexPath) as! MileStoneCollectionViewCell
            print(indexPath.row)
            
            for milestoneInfo in mileStoneData {
                guard let count = Int(milestoneInfo.milestoneNumber ?? "0")
                    else { return cell1 }
                if count != indexPath.row + 1 {
                } else {
                    cell1.MilStoneLabel.text = "Milestone " +  milestoneInfo.milestoneNumber!
                    cell1.FromLabel.text = milestoneInfo.fromLocation
                    cell1.ToLabel.text = milestoneInfo.toLocation
                    let distanceTime = milestoneInfo.distance! + " - " + milestoneInfo.time!
                    cell1.Dist_TimeLabel.text = distanceTime
                    break
                }
            }
            return cell1
            
        case UICollectionView2:
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "Stuff", for: indexPath) as! RecommendationCollectionViewCell
            cell2.RecommendationCell!.text = Recommendations[indexPath.row]
            return cell2
        case UICollectionView3:
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "Contacts", for: indexPath)
            return cell3
        default:
            return UICollectionViewCell.init()
        }
    }
    
    
}
extension TripSummaryViewController: CLLocationManagerDelegate, MKMapViewDelegate{
    func MapOn(){
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        locationManger.distanceFilter = kCLDistanceFilterNone
        Map.showsUserLocation = true
        Map.delegate = self
        getAddress()
    }
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(DestinationMap) { (placemarks, error) in
            guard let placemarks = placemarks, let destination = placemarks.first?.location
                else {
                    print("No Location Found")
                    return
            }
            geoCoder.geocodeAddressString((self.createdTripInfo?.destination)!) { (placemarks, error) in
                guard let placemarks = placemarks, let Source = placemarks.first?.location
                    else{
                        print("No location found")
                        return
                }
                self.mapThis(destinationCord: destination.coordinate, sourceCord: Source.coordinate)
        }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
    
    func mapThis(destinationCord : CLLocationCoordinate2D, sourceCord: CLLocationCoordinate2D) {
//        if destinationpin != nil{
//            Map.removeAnnotation(destinationpin)
//        }
      //  let souceCordinate = (locationManger.location?.coordinate)!
        sourcepin = customPin(pinTitle: (self.createdTripInfo?.location)!, pinSubTitle: (self.createdTripInfo?.destination)!, location: sourceCord)
        destinationpin = customPin(pinTitle: (self.createdTripInfo?.destination)!, pinSubTitle: (self.createdTripInfo?.destination)!, location: destinationCord)
        Map.removeAnnotation(destinationpin)
        self.Map.addAnnotation(sourcepin)
        self.Map.addAnnotation(destinationpin)
        let soucePlaceMark = MKPlacemark(coordinate: sourceCord)
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
        
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print("Something is wrong", error)
                }
                return
            }
            if self.route != nil{
                self.Map.removeOverlay(self.route.polyline)
            }
            self.route = response.routes[0]
            self.Map.addOverlay(self.route.polyline)
            self.Map.setVisibleMapRect(self.route.polyline.boundingMapRect, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        render.lineWidth = 5
        return render
    }
    
    
}
extension UICollectionView{
    func addCorner(){
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
    }
    func addborder(){
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
    }
}

//
//  InviteOtherRidersViewController.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 21/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
import  Contacts

protocol FromHomeContact {
    func setTableView(fromHomeContact: Bool, selectedContacts: [String])
}

class InviteOtherRidersViewController: UIViewController {
    
    let navigationControllerObject = CustomNavigationViewController()
    
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var contactSearchBar: UISearchBar!
    
    var selectedContacts = [String]()
    var delegate: FromHomeContact?
    
    //let names: [String] = ["Alice", "Bob", "Daniel"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationControllerObject.navigationBarDesign(controller: self, title: "Invite People")
        
        navigationItem.title = "Invite People"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.leftBarButtonItem?.tintColor = .white
        
        contactSearchBar.changeSearchBarColor(color: UIColor.white)
        
        // contactTableView.register(ContactInfoTableViewCell.self, forCellReuseIdentifier: "cell")
        
        //contactTableView.register(IndividualContactTableViewCell(), forCellReuseIdentifier: "cell")
        contactTableView.register(UINib.init(nibName: "IndividualContactTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        fetchContacts()
        
    }
    
    func someContactNameTapped(sender: UITableViewCell) {
        
        var indexPathTapped = contactTableView?.indexPath(for: sender)
        //print(indexPathTapped!)
        
        let name = favoritableContacts[(indexPathTapped?.row)!]
        //print(name)
        //let selectedName = name.contact.givenName
        //print(selectedName)
        let hasChecked = name.hasChecked
        favoritableContacts[(indexPathTapped?.row)!].hasChecked = !hasChecked
        
        //selectedContacts.append(selectedName)
        //print(selectedContacts)
        
        self.contactTableView.reloadRows(at: [indexPathTapped!], with: .automatic)
    }
    
    
    func contactFavoritized(cell: UITableViewCell) {
        var indexPathTapped = contactTableView?.indexPath(for: cell)
        print(indexPathTapped!)
        
        let name = favoritableContacts[(indexPathTapped?.row)!]
        //print(name)
        //let selectedName = name.contact.givenName
        //print(selectedName)
        let hasFavorited = name.hasFavorited
        favoritableContacts[(indexPathTapped?.row)!].hasFavorited = !hasFavorited
        
        //selectedContacts.append(selectedName)
        //print(selectedContacts)
        
        self.contactTableView.reloadRows(at: [indexPathTapped!], with: .automatic)
    }
    
    
    //let twoDimensionalArray = [
    //    ["Alice", "Bob", "Daniel"],
    //   ["Max", "John"]]
    
    var twoDimensionalArray = [ExpandableNames]()
    var favoritableContacts = [FavoritableContact]()
    var searchContact = [FavoritableContact]()
    
    func fetchContacts() {
        print("Attempting to access contacts")
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, err) in
            if let err = err {
                print("Failed to request access", err)
                return
            }
            if granted {
                print("Access granted")
                let keys =  [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    
                    
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointerIfYouWantToStopEnumerating) in
                        //print(contact.givenName)
                        //print(contact.familyName)
                        //print(contact.phoneNumbers.first?.value.stringValue ?? "")
                        
                        self.favoritableContacts.append(FavoritableContact(contact: contact, hasFavorited: false, hasChecked: false))
                    })
                    
                    let names = ExpandableNames(isExpanded: true, names: self.favoritableContacts)
                    self.twoDimensionalArray = [names]
                    
                    self.contactTableView.reloadData()
                } catch let err {
                    print("Failed to enumerate contacts", err)
                }
                self.contactTableView.reloadData()
                self.searchContact = self.favoritableContacts
            }else {
                print("Access Denied")
            }
        }
    }
    
    @objc func transitionBackTo() {
        let fromHomeContact = true
        
        //let successView = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripViewController") as! CreateTripViewController
        //successView.selectedContacts = selectedContacts
        
        delegate?.setTableView(fromHomeContact: fromHomeContact, selectedContacts: selectedContacts)
        self.navigationController?.popViewController(animated: true)
    }
}


extension InviteOtherRidersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return twoDimensionalArray.count
        //return favoritableContacts.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return(twoDimensionalArray[section].names.count)
        return searchContact.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! IndividualContactTableViewCell
        //print("***", cell)
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! IndividualContactTableViewCell
        //cell.selectionStyle = .none
        
        cell.link = self
        
        //cell.textLabel?.text = twoDimensionalArray[indexPath.section][indexPath.row]
        
        //let favoritableContact = twoDimensionalArray[indexPath.section].names[indexPath.row]
        //print("**", searchContact[indexPath.row])
        
        let favoritableContact = searchContact[indexPath.row]
        //print(favoritableContact.contact.givenName)
        
        //cell.textLabel?.text = favoritableContact.contact.givenName + " " + favoritableContact.contact.familyName
        
        cell.nameLabel?.text = favoritableContact.contact.givenName + " " + favoritableContact.contact.familyName
        //cell.setIndividualCell(cellInfo: favoritableContact)
        
        //cell.starButton.imageView!.image = UIImage(named: "star")
        
        //cell.starButton.tintColor = .white
        //cell.checkMarkButton.addTarget(InviteOtherRidersViewController.self, action: #selector(InviteOtherRidersViewController.someContactNameTapped(sender: )), for: .touchUpInside)
        //FavoritableContact.hasChecked ? checkMarkButton.setImage(UIImage(named: "Group-1"), for: .normal) : checkMarkButton.setImage(UIImage(named: "Group 3"), for: .normal)
        
        //print("Again table view", favoritableContacts[indexPath.row].hasChecked)
        
        if favoritableContacts[indexPath.row].hasChecked {
            cell.checkMarkButton.setImage(UIImage(named: "Group-1"), for: .normal)
            selectedContacts.append(favoritableContacts[indexPath.row].contact.givenName)
            /*if selectedContacts.contains(favoritableContacts[indexPath.row].contact.givenName) {
             //do nothing
             } else {
             selectedContacts.append(favoritableContacts[indexPath.row].contact.givenName)
             }*/
        } else {
            cell.checkMarkButton.setImage(UIImage(named: "Group 3"), for: .normal)
            let removeContact = favoritableContacts[indexPath.row].contact.givenName
            if let index = selectedContacts.index(of: removeContact) {
                selectedContacts.remove(at: index)
            }
            /*for contact in selectedContacts {
             if contact == removeContact {
             selectedContacts.removeAtIndex(selectedContacts.indexOf(removeContact))
             }
             }*/
            //selectedContacts.remove(at: favoritableContacts[indexPath.row].contact.givenName)
        }
        
        if favoritableContacts[indexPath.row].hasFavorited {
            cell.starButton.tintColor = UIColor(rgb: 0xEF4481)
        } else {
            cell.starButton.tintColor = UIColor(rgb: 0xFFFFFF)
        }
        //print(selectedContacts)
        return(cell)
    }
    
}

extension InviteOtherRidersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else { searchContact = favoritableContacts ; contactTableView.reloadData(); return}
        searchContact = favoritableContacts.filter({ favoritableContacts -> Bool in
            //favoritableContacts.contact.givenName.lowercased().contains(searchText.lowercased())
            favoritableContacts.contact.givenName.prefix(searchText.count).contains(searchText)
            
        })
        
        contactTableView.reloadData()
        
    }
}

extension UISearchBar {
    func changeSearchBarColor(color: UIColor) {
        let size = CGSize(width: self.frame.size.width, height: 45)
        UIGraphicsBeginImageContext(self.frame.size)
        color.setFill()
        UIBezierPath(rect: self.frame).fill()
        let bgImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.setSearchFieldBackgroundImage(bgImage, for: .normal)
        
    }
}



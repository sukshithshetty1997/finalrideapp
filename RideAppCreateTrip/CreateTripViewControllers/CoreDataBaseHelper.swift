//
//  CoreDataBaseHelper.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 14/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataBaseHelper {
    
    static var shareInstance = CoreDataBaseHelper()
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    func saveTrip(object: [String: String]) {
        let trip = NSEntityDescription.insertNewObject(forEntityName: "TripData", into: context!) as! TripData
        trip.destination = object["destination"]
        trip.location = object["location"]
        trip.name = object["name"]
        trip.startDate = object["startDate"]
        trip.endDate = object["endDate"]
        trip.startTime = object["startTime"]
        
        do {
            try context?.save()
        } catch {
            print("Dats is not saved")
        }
    }
    
    func getTripData() -> [TripData]{
        var trip = [TripData]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "TripData")
        
        do {
            trip = try context?.fetch(fetchRequest) as! [TripData]
        } catch {
            print("Can not get data")
        }
        return trip
    }
    
    func deleteTrip(index: Int) -> String {
        print("Delete Trip")
        
        var tripData = self.getTripData()
//        let tripId = tripData[index].objectID
        let tripId = NSString(format: "%p", address(o: &tripData))
        context?.delete(tripData[index])
        tripData.remove(at: index)
        do {
            try context?.save()
        } catch let err {
            print("Deleted: \(err.localizedDescription)")
        }
        return "\(tripId)"
    }
    
    func saveMilestone(object: [String: String], trip: TripData) {
        let milestone = NSEntityDescription.insertNewObject(forEntityName: "MileStone", into: context!) as! MileStone
        
        milestone.milestoneNumber = object["milestoneNumber"]
        milestone.fromLocation = object["fromLocation"]
        milestone.toLocation = object["toLocation"]
        milestone.distance = object["distance"]
        milestone.time = object["time"]
        milestone.trip = trip
        
        do {
            try context?.save()
        } catch {
            print("Data is not saved")
        }
    }
    
//    func getMilestoneData() -> [MileStone]{
//        var mileStone = [MileStone]()
//        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MileStone")
//
//        do {
//            mileStone = try context?.fetch(fetchRequest) as! [MileStone]
//        } catch {
//            print("Can not get data")
//        }
//        return mileStone
//    }
    
    func saveRiders(object: [String: String], trip: TripData) {
        let rider = NSEntityDescription.insertNewObject(forEntityName: "Riders", into: context!) as! Riders
        
        rider.name = object["name"]
        rider.tripRiders = trip
//        rider.objectID
        ServerStore.shareInstance.invitedUsers.append("\(rider.objectID)")
        print(ServerStore.shareInstance.invitedUsers)
        do {
            try context?.save()
        } catch {
            print("Data is not saved")
        }
    }
    
    func getRiderData() -> [Riders]{
        var rider = [Riders]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Riders")
        
        do {
             rider = try context?.fetch(fetchRequest) as! [Riders]
        } catch {
            print("Can not get data")
        }
        return rider
    }
    
    func deleteRider(index: Int, trip: TripData) {
        var riderData = [Riders]()
        if trip.riders?.allObjects != nil {
            riderData = trip.riders?.allObjects as! [Riders]
        }
        
        context?.delete(riderData[index])
        riderData.remove(at: index)
        do {
            try context?.save()
        } catch let err {
            print("Deleted: \(err.localizedDescription)")
        }
    }
    
    func deleteAllRider(trip: TripData) {
        var riderData = [Riders]()
        if trip.riders?.allObjects != nil {
            riderData = trip.riders?.allObjects as! [Riders]
            for managedObject in riderData
            {
                let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                context?.delete(managedObjectData)
            }
        }
        do {
            try context?.save()
        } catch let err {
            print("Deleted: \(err.localizedDescription)")
        }
    }
    
    func deleteAllMilestone(trip: TripData) {
        var milestoneData = [MileStone]()
        if trip.mileStones?.allObjects != nil {
            milestoneData = trip.mileStones?.allObjects as! [MileStone]
            for managedObject in milestoneData
            {
                let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                context?.delete(managedObjectData)
            }
        }
        do {
            try context?.save()
        } catch let err {
            print("Deleted: \(err.localizedDescription)")
        }
    }
    
    
    func modifyTrip(object: [String: String], trip: TripData) {
        trip.destination = object["destination"]
        trip.location = object["location"]
        trip.name = object["name"]
        trip.startDate = object["startDate"]
        trip.endDate = object["endDate"]
        trip.startTime = object["startTime"]
        do {
            try context?.save()
        } catch {
            print("Data is not saved")
        }
    }
    
    func modifyRider(object: [String: String], rider: Riders) {
        rider.name = object["name"]
        do {
            try context?.save()
        } catch {
            print("Data is not saved")
        }
    }
    
    func modifyMilestone(object: [String: String], milestone: MileStone) {
        
        do {
            try context?.save()
        } catch {
            print("Data is not saved")
        }
    }
    func address(o: UnsafePointer<Void>) -> Int {
        return unsafeBitCast(o, to: Int.self)
    }
}

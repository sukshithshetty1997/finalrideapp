//
//  DestinationSearchViewController.swift
//  PastTrip12.0
//
//  Created by Zeeta Andrade on 01/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

protocol DestinationPlaceName {
    func selectedDestination(place: String)
}

class DestinationSearchViewController: UIViewController{

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var locationSearchTable: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var delegate: DestinationPlaceName?
    
    var matchingItems: [MKMapItem] = []
    var regionValue: MKCoordinateRegion?

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var searchText: String = ""
    
    var sourceLatitude: CLLocationDegrees?
    var sourceLongitude: CLLocationDegrees?
    var destinationLatitude: CLLocationDegrees?
    var destinationLongitude: CLLocationDegrees?
    var distance: Int = 0
    var line: UIView?
    
    
    var textFieldButton: UIButton {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor(rgb: 0x767373)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.frame = CGRect(x: CGFloat(searchTextField.frame.size.width - 40), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.clearSearchField), for: .touchUpInside)
        return button
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        tableViewHeightConstraint.constant = 0
        
        searchTextField.rightView = textFieldButton
        searchTextField.rightViewMode = .always
    }
    
    @objc func clearSearchField() {
        searchTextField.text = ""
    }
    
    func parseAddress(selectedItem:MKPlacemark) -> String {
        
        // put a space between "4" and "Melrose Place"
        let firstSpace = (selectedItem.subThoroughfare != nil &&
            selectedItem.thoroughfare != nil) ? " " : ""
        
        // put a comma between street and city/state
        let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) &&
            (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
        
        // put a space between "Washington" and "DC"
        let secondSpace = (selectedItem.subAdministrativeArea != nil &&
            selectedItem.administrativeArea != nil) ? " " : ""
        
        let addressLine = String(
            format:"%@%@%@%@%@%@%@",
            // street number
            selectedItem.subThoroughfare ?? "",
            firstSpace,
            // street name
            selectedItem.thoroughfare ?? "",
            comma,
            // city
            selectedItem.locality ?? "",
            secondSpace,
            // state
            selectedItem.administrativeArea ?? "",
            selectedItem.postalCode ?? ""
        )
        return addressLine
    }
    
    func distanceFromSourceToDestination() -> Int {
        let firstLocation = CLLocation(latitude: sourceLatitude!, longitude: sourceLongitude!)
        let secondLocation = CLLocation(latitude: destinationLatitude!, longitude: destinationLongitude!)
        distance = Int(firstLocation.distance(from: secondLocation) / 1000)
//        print("Distance \(String(format: "%d", arguments: [distance])) km")
        return distance
    }
    
}

extension DestinationSearchViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locationManager.location
        let coordinate: CLLocationCoordinate2D? = currentLocation?.coordinate
//        print(coordinate!.latitude)
//        print(coordinate!.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: coordinate!, span: span)
        regionValue = region
        sourceLatitude = region.center.latitude
        sourceLongitude = region.center.longitude
        
    }
}

extension DestinationSearchViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if string.isEmpty
        {
            searchText = String(searchText.characters.dropLast())
        }
        else
        {
            searchText = textField.text! + string
        }
        
        guard let regionValue = regionValue else { return false }
        let searchBarText = searchText
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        request.region = regionValue
        let search = MKLocalSearch(request: request)
        
        search.start { response, _ in
            guard let response = response else {
                return
            }
            self.matchingItems = response.mapItems
            self.tableViewHeightConstraint.constant = self.locationSearchTable.contentSize.height
            self.locationSearchTable.reloadData()
        }
        return true
    }
}

extension DestinationSearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 94
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DestinationSearchCell
        let selectedItem = matchingItems[indexPath.row].placemark
        
        destinationLatitude = selectedItem.coordinate.latitude
        destinationLongitude = selectedItem.coordinate.longitude
        let distaceValue = distanceFromSourceToDestination()
        
        var name = selectedItem.name ?? ""
        name.append(", ")
        name.append(selectedItem.locality ?? "")
        cell.placeLabel.text = name
        //let address = parseAddress(selectedItem: selectedItem)
        var discriptionText = selectedItem.locality ?? ""
        if selectedItem.postalCode != nil {
            discriptionText.append(" - ")
        }
        discriptionText.append(selectedItem.postalCode ?? "")
        cell.descriptionLabel.text = discriptionText
        cell.distanceLabel.text = "\(String(format: "%d", arguments: [distaceValue])) km"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = matchingItems[indexPath.row].placemark
        let destinationArray = [selectedItem.coordinate.latitude, selectedItem.coordinate.longitude]
        ServerStore.shareInstance.destination = destinationArray
        var placeName = ""
        guard let name = selectedItem.name
            else { return }
        placeName.append(name)
        placeName.append(", ")
        if let locality = selectedItem.locality {
            placeName.append(locality)
        } else {
            placeName.append(name)
        }
        delegate?.selectedDestination(place: placeName)
        dismiss(animated: true, completion: nil)
    }
}


class DestinationSearchCell: UITableViewCell {
    
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
}

//
//  MainTabBar.swift
//  RideAppCreateTrip
//
//  Created by Sukshith Shetty on 17/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class MainTabBar: UITabBarController {
     var defaults = UserDefaults.standard
    override func viewDidAppear(_ animated: Bool) {
        defaults.set("TabVC", forKey: "Initial")
    }

    override func viewDidLoad() {
        self.tabBar.unselectedItemTintColor = UIColor(rgb: 0xFED2AE)
        
        self.tabBar.tintColor = UIColor(rgb: 0xFFFFFF)
        self.tabBar.barTintColor = UIColor(rgb: 0xEE802C)
        self.tabBar.isTranslucent = true
        self.tabBar.alpha = 1.0
        
    }
}

class HomeTabBar: UITabBar {

    override func layoutSubviews() {
        super.layoutSubviews()

        var verticalOffset: CGFloat = 6.0

        if #available(iOS 11.0, *), traitCollection.horizontalSizeClass == .regular {
            verticalOffset = 0.0
        }

        let imageInset = UIEdgeInsets(
            top: verticalOffset,
            left: 0.0,
            bottom: -verticalOffset,
            right: 0.0
        )

        for tabBarItem in items ?? [] {
            tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(rgb: 0xFFFFFF)], for: .selected)
            tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
            
            tabBarItem.imageInsets = imageInset
        }
    }
}

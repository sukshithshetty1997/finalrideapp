//
//  buttonGradient.swift
//  RideATC
//
//  Created by Royston Vishal Rodrigues on 10/03/20.
//  Copyright © 2020 Robosoft. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class buttonGradient: UIButton{
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.layer.masksToBounds = false
    }
    @IBInspectable public var ShadowRadius:CGFloat = 0{
        didSet{
            self.layer.shadowRadius = self.ShadowRadius
        }
    }
    @IBInspectable public var ShadowColor: UIColor = UIColor.white{
        didSet{
            self.layer.shadowColor = self.ShadowColor.cgColor
        }
    }
    @IBInspectable public var ShadowOpacity: Float = 1{
        didSet{
            self.layer.shadowOpacity = self.ShadowOpacity
        }
    }
    
    @IBInspectable public var ShadowOffset: CGSize = .zero{
        didSet{
            self.layer.shadowOffset = self.ShadowOffset
        }
    }
    @IBInspectable public var CornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = CornerRadius
            
        }
    }
    
    
    @IBInspectable public var firstColor:UIColor = UIColor.white{
        didSet{
            updateView()
        }
    }
    @IBInspectable public var secondColor: UIColor = UIColor.white{
        didSet{
            updateView()
        }
    }
    @IBInspectable public var horizontalGradient: Bool = false{
        didSet{
            updateView()
        }
    }
    
    override class var layerClass: AnyClass{
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView(){
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        
        if horizontalGradient {
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x: 1.0, y: 1.0)
        }
        else{
            layer.startPoint = CGPoint(x: 0.0, y: 0.0)
            layer.endPoint = CGPoint(x: 0.0, y: 1.0)
        }
    }
    
}


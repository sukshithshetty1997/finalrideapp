//
//  CollectionViewCell.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 26/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import Foundation
import UIKit
protocol Close: class {
    func close(cell: CustomViewCell)
}
@IBDesignable
class CustomViewCell: UICollectionViewCell {
    var delegate: Close?
    
    @IBInspectable var CornerRadius :CGFloat = 0
    @IBInspectable var shadowRadius : CGFloat = 0
    @IBInspectable var shadowColor : UIColor = UIColor.clear
    @IBInspectable var shadowOpacity: Float = 1
    @IBInspectable var shadowOffset : CGSize = .zero
    override func awakeFromNib() {
        super.awakeFromNib()
        styleView()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        styleView()
    }
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
        styleView()
    }
    override func prepareForInterfaceBuilder() {
        styleView()
    }
    func styleView() {
        layer.cornerRadius = CornerRadius
        layer.shadowRadius = shadowRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
    }
    
    @IBOutlet var TripName: UILabel!
    @IBOutlet var BackgroundImage: UIImageView!
    @IBOutlet var Date: UILabel!
    @IBOutlet var Status: UILabel!

    @IBAction func Close(_ sender: Any) {
        delegate!.close(cell: self)
        
    }
    
}



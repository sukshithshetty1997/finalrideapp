//
//  View.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 26/04/20.
//  Copyright © 2020 Sukshith Shetty. A

import Foundation
import UIKit

@IBDesignable
class ViewClass: UIView {
    @IBInspectable var CornerRadius :CGFloat = 10
    @IBInspectable var shadowRadius : CGFloat = 5
    @IBInspectable var shadowColor : UIColor = UIColor.lightGray
    @IBInspectable var shadowOpacity: Float = 1
    @IBInspectable var shadowOffset : CGSize = CGSize(width: 0, height: 1)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        styleView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        styleView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        styleView()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        styleView()
    }
    
    override func prepareForInterfaceBuilder() {
        styleView()
    }
    func styleView() {
        layer.cornerRadius = CornerRadius
        layer.shadowRadius = shadowRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.masksToBounds = false
    
    }
}

//
//  CustomSearchBar .swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 26/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CustomSearchBar: UISearchBar{
    @IBInspectable var CornerRadius :CGFloat = 0
    @IBInspectable var shadowRadius : CGFloat = 0
    @IBInspectable var shadowColor : UIColor = UIColor.clear
    @IBInspectable var shadowOpacity: Float = 1
    @IBInspectable var shadowOffset : CGSize = .zero
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        styleView()
    }
    
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
        styleView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        styleView()
    }
    
    override func prepareForInterfaceBuilder() {
        styleView()
    }
    func styleView() {
        layer.cornerRadius = CornerRadius
        layer.shadowRadius = shadowRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
    
    }
}

//
//  MileStoneCollectionViewCell.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 29/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

@IBDesignable
class MileStoneCollectionViewCell: UICollectionViewCell {
    @IBInspectable var shawdowColor : UIColor = UIColor.clear
    @IBInspectable var shadowRadius : CGFloat = 0
    @IBInspectable var cornerRadius : CGFloat = 0
    @IBInspectable var borderWidth : CGFloat = 0
    @IBInspectable var borderColor : UIColor = UIColor.clear
    @IBInspectable var shadowOffset : CGSize = .zero
    @IBInspectable var shadowOpacity : Float = 0
    var LineView:UIView = UIView()
    var Dist_TimeLabel:UILabel = UILabel()
    var MilstoneView:UIView = UIView()
    
    @IBOutlet var MilStoneLabel: UILabel!
    
    @IBOutlet var ToLabel: UILabel!
    @IBOutlet var FromLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        styleView()
        layoutSubviews()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        styleView()
        
    }
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
        styleView()
    }
    override func prepareForInterfaceBuilder() {
        styleView()
    }
    
    func styleView(){
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.shadowColor = shawdowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
    
    }

    override func layoutSubviews() {
        LineView.frame = CGRect(x: FromLabel.frame.origin.x + FromLabel.frame.size.width + 10, y: FromLabel.frame.origin.y + FromLabel.frame.size.height/2, width: ToLabel.frame.origin.x - 10 - LineView.frame.origin.x - FromLabel.frame.size.width, height: 1)
        LineView.backgroundColor = UIColor.lightGray
        Dist_TimeLabel.frame = CGRect(x: LineView.frame.origin.x, y: LineView.frame.origin.y - Dist_TimeLabel.frame.size.height - 5
            , width: self.LineView.frame.size.width, height: 20)
        MilstoneView.frame = CGRect(x: MilStoneLabel.frame.origin.x, y: MilStoneLabel.frame.origin.y + MilStoneLabel.frame.size.height, width: self.MilStoneLabel.frame.size.width
            , height: 1)
        Dist_TimeLabel.font = Dist_TimeLabel.font.withSize(12)
        MilstoneView.backgroundColor = UIColor.lightGray
        Dist_TimeLabel.textAlignment = .center
        Dist_TimeLabel.textColor = UIColor.lightGray
        self.addSubview(Dist_TimeLabel)
        self.addSubview(MilstoneView)
        self.addSubview(LineView)
        
    }


}


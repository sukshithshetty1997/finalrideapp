//
//  GradientView.swift
//  RideAppCreateTrip
//
//  Created by Royston Vishal  on 13/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
@IBDesignable
class GradientView: UIView {
    @IBInspectable public var firstColor: UIColor = UIColor.white {
    didSet {
        updateView()
    }
    }
    @IBInspectable public var secondColor: UIColor = UIColor.white{
        didSet{
            updateView()
        }
    }
    @IBInspectable public var horizontalGradient: Bool = false{
        didSet{
            updateView()
        }
    }
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        if (horizontalGradient){
            layer.startPoint = CGPoint(x: 0.75, y: 0.75)
            layer.endPoint = CGPoint(x: 1, y: 1)
        }
        else {
            layer.startPoint = CGPoint(x: 1, y: 1)
            layer.endPoint = CGPoint(x: 0, y: 0)
            
        }
    }
}

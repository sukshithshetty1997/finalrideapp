//
//  RecommendationCollectionViewCell.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 29/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

@IBDesignable
class RecommendationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var RecommendationCell: LabelClass!
    @IBInspectable var shawdowColor : UIColor = UIColor.clear
    @IBInspectable var shadowRadius : CGFloat = 0
    @IBInspectable var cornerRadius : CGFloat = 0
    @IBInspectable var borderWidth : CGFloat = 0
    @IBInspectable var borderColor : UIColor = UIColor.clear
    @IBInspectable var shadowOffset : CGSize = .zero
    @IBInspectable var shadowOpacity : Float = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        styleView()
    }
    
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
        styleView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        styleView()
    }
    
    override func prepareForInterfaceBuilder() {
        styleView()
    }
    
    func styleView(){
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.shadowColor = shawdowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
        // layer.clipsToBounds = true
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        
    }

}

//
//  ComponentClass.swift
//  Garage
//
//  Created by Sukshith Shetty on 01/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import Foundation
import UIKit

class ComponentClass: NSObject, NSCoding{
    required init?(coder aDecoder: NSCoder) {
        
        MobileNumber = aDecoder.decodeObject(forKey: "MobileNumber") as? String ?? ""
        VehicleNumber = aDecoder.decodeObject(forKey: "VehicleNumber") as? String ?? ""
        VehicleType = aDecoder.decodeObject(forKey: "VehicleType") as? String ?? ""
        ServiceType = aDecoder.decodeObject(forKey: "ServiceType") as? String ?? ""
        SlotDate = aDecoder.decodeObject(forKey: "SlotDate") as? String ?? ""
        Time = aDecoder.decodeObject(forKey: "Time") as? String ?? ""
        Dealer = aDecoder.decodeObject(forKey: "Dealer") as? String ?? ""
        City = aDecoder.decodeObject(forKey: "City") as? String ?? ""
        Comments = aDecoder.decodeObject(forKey: "Comments") as? String ?? ""
        DataArray = aDecoder.decodeObject(forKey: "ArrayGarage") as? [ComponentClass] ?? [ComponentClass]()
    }
    
    static var Data = ComponentClass(MobileNumber: "", VehicleNumber: "", VehicleType: "", ServiceType: "", SlotData: "", Time: "", Dealer: "", City: "", Comments: "")
    var DataArray = [ComponentClass]()
    var PassArrayDetails = [ComponentClass]()
    
    init(MobileNumber:String, VehicleNumber:String, VehicleType:String, ServiceType:String, SlotData:String, Time:String, Dealer:String, City:String, Comments:String){
        
        self.MobileNumber = MobileNumber
        self.VehicleNumber = VehicleNumber
        self.VehicleType = VehicleType
        self.ServiceType = ServiceType
        self.SlotDate = SlotData
        self.Time = Time
        self.City = City
        self.Dealer = Dealer
        self.Comments = Comments
    }
    
    var MobileNumber:String
    var VehicleNumber:String
    var VehicleType:String
    var ServiceType:String
    var SlotDate:String
    var Time:String
    var Dealer:String
    var City:String
    var Comments:String
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(MobileNumber, forKey: "MobileNumber")
        aCoder.encode(VehicleNumber, forKey: "VehicleNumber")
        aCoder.encode(VehicleType, forKey: "VehicleType")
        aCoder.encode(ServiceType, forKey: "ServiceType")
        aCoder.encode(SlotDate, forKey: "SlotDate")
        aCoder.encode(Time, forKey: "Time")
        aCoder.encode(Dealer, forKey: "Dealer")
        aCoder.encode(City, forKey: "City")
        aCoder.encode(Comments, forKey: "Comments")
        aCoder.encode(DataArray, forKey: "ArrayGarage")
    }
}

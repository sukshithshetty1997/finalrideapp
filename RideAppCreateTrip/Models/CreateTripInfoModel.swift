//
//  TripInfoModel.swift
//  Gallery
//
//  Created by Sukshith Shetty on 10/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class CreateTripInfoModel: NSObject {
    static let TripInfo = CreateTripInfoModel(TripName: "", Date: "", Home: "", Destination: "", Time: "")
    var TripName:String;
    var Date:String;
    var Home:String;
    var Destination:String;
    var Time:String;
    init(TripName:String, Date:String, Home:String, Destination:String, Time:String){
        self.TripName = TripName
        self.Date = Date
        self.Home = Home
        self.Destination = Destination
        self.Time = Time
    }
}

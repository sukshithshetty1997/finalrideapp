//
//  TripModel.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 26/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import Foundation
import UIKit
enum TripStatus: String{
    case Upcoming = "Upcoming"
    case Completed = "Completed"
}
class Trip: NSObject {
    static let Data = Trip(TripName: "", Date: "", Status: TripStatus(rawValue: "")!, BackgroundImage: .init(), Home: "", Destination: "")
    var Destination:String;
    var TripName: String;
    var Date: String;
    var Status: TripStatus
    var BackgroundImage: UIImage;
    var Home:String;
    init(TripName:String, Date:String, Status:TripStatus, BackgroundImage: UIImage,Home:String, Destination:String) {
        self.TripName = TripName
        self.Date = Date
        self.Status = Status
        self.BackgroundImage = BackgroundImage
        self.Home = Home
        self.Destination = Destination
        print(self.Home, self.Destination)
    }
  
    
}

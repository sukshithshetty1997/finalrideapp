//
//  GaragedealerModel.swift
//  Garage
//
//  Created by Sukshith Shetty on 07/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class GaragedealerModel: NSObject {
    var ShopName: String;
    var Location: String;
    var Distance: Double;
    var Number: Int;
    var RatingImage: UIImage;
    var GoldRating: Int;
    var SilverImage: UIImage;
    var ViewImage: UIImage;
    var Address1: String;
    var Address2: String;
    var id: String;
    
    
    init(ShopName:String, Location:String, Distance:Double, Number:Int, RatingImage:UIImage,SilverImage:UIImage, GoldRating:Int, ViewImage:UIImage, Address1: String, Address2:String, id: String) {
        self.ShopName = ShopName
        self.Location = Location
        self.Distance = Distance
        self.Number = Number
        self.RatingImage = RatingImage
        self.SilverImage = SilverImage
        self.GoldRating = GoldRating
        self.ViewImage = ViewImage
        self.Address1 = Address1
        self.Address2 = Address2
        self.id = id
    }

}

//
//  Person.swift
//  Gallery
//
//  Created by Sukshith Shetty on 21/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit


class Milestone: NSObject {
    var MilestoneNumber:Int;
    var From:String;
    var To:String;
    var Distance:Int;
    var Time:String;
    
    init(MilestoneNumber: Int, From:String, To:String, Distance:Int, Time:String) {
        self.MilestoneNumber = MilestoneNumber
        self.From = From
        self.To = To
        self.Distance = Distance
        self.Time = Time
    }
}

//
//  SearchRecordsModel.swift
//  Garage
//
//  Created by Sukshith Shetty on 01/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class AccessoriesModel: NSObject {
    var Date:String;
    var ItemImage:UIImage;
    var ItemName:String;
    var Price:Double;
    init(Date:String, ItemImage:UIImage, ItemName:String, Price:Double) {
        self.Date = Date
        self.ItemImage = ItemImage
        self.ItemName = ItemName
        self.Price = Price
    }
}

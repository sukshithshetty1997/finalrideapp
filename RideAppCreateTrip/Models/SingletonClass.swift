//
//  SingletonClass.swift
//  TripSearch
//
//  Created by Sukshith Shetty on 11/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class SingletonClass: NSObject {
    static let Data = SingletonClass()
    private override init() {}
    var TripName:String = ""
    var Date:String = ""
    var Home:String = ""
    var Destination:String = ""

}

//
//  DesignableButton.swift
//  Rideapp
//
//  Created by Velan Salis on 05/03/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

@IBDesignable
class NativeButton: UIButton {

    let defaultCornerRadius : CGFloat = 10
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        self.applyCustomStyles()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.applyCustomStyles()
    }
    
    private func applyCustomStyles() {
        self.layer.backgroundColor = UIColor.orange.cgColor
        self.layer.cornerRadius = defaultCornerRadius
    }
    
    @IBInspectable var cornerRadiu: CGFloat = 10 {
        didSet {
            self.layer.cornerRadius = cornerRadiu
        }
    }

}

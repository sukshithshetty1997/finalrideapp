//
//  DesignableTextField.swift
//  Rideapp
//
//  Created by Velan Salis on 02/03/20.
//  Copyright © 2020 Velan Salis. All rights reserved.
//

import UIKit

@IBDesignable
class NativeTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        self.applyCustomStyles()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.applyCustomStyles()
    }
    
    private func applyCustomStyles() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(rgb: 0xB4B3B3).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1)
        self.layer.shadowOpacity = 0.5;
        self.layer.shadowRadius = 0.0;
    }
    
    @IBInspectable var leftImage : UIImage? {
        didSet {
            if let image = leftImage {
                leftViewMode = .always
                let imageView = UIImageView(frame: CGRect(x : 0, y : 0, width: 25, height: 20))
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                let view = UIView(frame : CGRect(x : 0, y : 0, width : 32, height : 25))
                view.addSubview(imageView)
                leftView = view
            } else {
                // Handle null
                leftViewMode = .never
            }
        }
    }

}

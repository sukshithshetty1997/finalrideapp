//
//  MoreSettingsViewController.swift
//  RideAppCreateTrip
//
//  Created by Sukshith Shetty on 28/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit
import KeychainSwift

class TableCellViewClass: UITableViewCell {
    @IBOutlet var imagesViewCollection: UIImageView!
    override func layoutSubviews() {
    imagesViewCollection.transform = CGAffineTransform(scaleX: 2, y: 2)
    }}

class MoreSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    let auth = RiderInteractor()
    @IBOutlet var Tranparent: UIView!
    @IBOutlet var Subviews: UIView!
    var defaults = UserDefaults.standard
    @IBOutlet var TableView: UITableView!
    @IBOutlet var NameLabel: UILabel!
    let FaceImage = UIImagePickerController()
    let collection:[String] = ["Terms of Use", "Privacy Policy", "Help & Support", "Settings", "Sign Out"]
    let images:[UIImage] = [UIImage(named: "TC")!, UIImage(named: "privacy")!, UIImage(named: "HS")!, UIImage(named: "settings")!, UIImage(named: "logout")!]
    @IBOutlet var ProfilPicture: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        FaceImage.delegate = self
        ProfilPicture.layer.cornerRadius = ProfilPicture.frame.size.height/2
        ProfilPicture.layer.masksToBounds = true
        ProfilPicture.layer.borderColor = UIColor.lightGray.cgColor
        ProfilPicture.layer.borderWidth = 1.5
        Tranparent.layer.cornerRadius = Tranparent.frame.size.height/2
        Tranparent.backgroundColor = UIColor.clear
        Tranparent.isOpaque = false
        let gesture = UITapGestureRecognizer(target: self, action: #selector(UploadPic(_:)))
        Tranparent.addGestureRecognizer(gesture)
        TableView.tableFooterView = UIView(frame: .zero)
        viewDidLayoutSubviews()
        let token = keychain.get(Keys.accessToken)
        print(token!)
        let response = auth.getProfile(requestHeaders: ["Authorization" : "Bearer \(token!)"])
        guard let name = response.data?.name, let email = response.data?.email, let phone = response.data?.phone, let id = response.data?._id
            else{
            print("No Data Present")
            return
        }
        print(id)
        NameLabel.text = name + "\n" + "+91 " + phone + "\n" + email
//        guard let image = response.data?.avatar else {
//            print("no image")
//            return
//        }
//        print("Data collected Safely")
//        if let uploadedImage = UIImage(contentsOfFile: image){
//            print("image present")
//        }
//        let imageData = UIImage(contentsOfFile: image)
//
        let imageData:UIImage = (getObject(fileName: "Profile.png") as? UIImage)!
        ProfilPicture.image = imageData
       
    }
    @objc func UploadPic(_ sender: UITapGestureRecognizer){
        FaceImage.allowsEditing = true
        FaceImage.sourceType = .photoLibrary
        self.present(FaceImage, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            ProfilPicture.image = image
             saveObject(fileName: "Profile.png", object: image)
    }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            ProfilPicture.image = image
saveObject(fileName: "Profile.png", object: image)
        }
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
        
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        TableView.frame = CGRect(x: TableView.frame.origin.x, y: NameLabel.frame.origin.y + NameLabel.frame.size.height + 30, width: TableView.frame.size.width, height: CGFloat(images.count * 60))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableCellViewClass
        cell.textLabel?.text = collection[indexPath.row]
        cell.imageView?.image = images[indexPath.row]
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "LoginNavigate") as! UINavigationController
        if indexPath.row == collection.count - 1 {
            let alert = UIAlertController(title: "Sign Out", message: "Would you like to sign out from your account?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Sign Out", style: UIAlertAction.Style.default, handler: { action in
                if self.keychain.clear() {
                    print("Clearing keychain")
                    self.presentDet(dest)
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in
                print("Cancelled")
            }))
            self.present(alert, animated: true, completion: nil)
    }
    }

}

//
//  CardView.swift
//  BookingDetails
//
//  Created by Shruthi on 06/06/20.
//  Copyright © 2020 robosoft. All rights reserved.
//

import UIKit
@IBDesignable
class CardView: UIView {
    //var Label:UILabel!
    var cornerRadius: CGFloat = 10
    var shadowOffsetWidth: CGFloat=0
    var shadowOffsetHeight : CGFloat = 5
    var shadowcolor : UIColor = UIColor.black
    var shadowOpacity : CGFloat = 0.5
    let minDate = Calendar.current.component(.day, from: Date())
    let minMonth = Calendar.current.component(.month, from: Date()) - 1
    let minYear = Calendar.current.component(.year, from: Date())
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override func layoutSubviews() {
        
        
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowcolor.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowPath = shadowPath.cgPath
        layer.shadowOpacity = Float(shadowOpacity)
    }
    
}


//
//  ViewController.swift
//  BookingDetails
//
//  Created by Shruthi on 06/06/20.
//  Copyright © 2020 robosoft. All rights reserved.
//
//NEw Shruthi
import UIKit

class MyGarageBookingDetails: UIViewController, UITableViewDelegate,UITableViewDataSource, UIGestureRecognizerDelegate{
    @IBOutlet var CopyRatings: [UIImageView]!
    @IBOutlet var RatingButtons: [UIImageView]!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var DateLabel: UILabel!
    var Label = UILabel()
    @IBOutlet var ServiceTypeLabel: UILabel!
    @IBOutlet var YearLabel: UILabel!
    @IBOutlet var MonthLabel: UILabel!
    var DashLine = UIView()
    var ComponentClassTable:[String] = []
    let collection:[String] = ["cell1","cell2","cell3","cell4","cell5","cell6","cell7"]
    let TitleLabels:[String] = ["Mobile Number", "Vehicle Number", "Service Type", "Time", "Dealer", "City", "Comments"]
    var Month:[String] = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
    let minDate = Calendar.current.component(.day, from: Date())
    let minMonth = Calendar.current.component(.month, from: Date()) - 1
    let minYear = Calendar.current.component(.year, from: Date())
    var ScopeFlag = 0
    var RatingImage:[UIImage] = [UIImage(named: "Star-1")!, UIImage(named: "Star-5")!]
    @IBOutlet var CardView: CardView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: .zero)
        
        //  tableView.estimatedRowHeight = 55
        //tableView.rowHeight = UITableView.automaticDimension
        
        var Date = ComponentClass.Data.PassArrayDetails[0].SlotDate
        Date = Date.components(separatedBy: .punctuationCharacters).joined()
        let DateArray:[String] = Date.components(separatedBy: .whitespacesAndNewlines)
        DateLabel.text = DateArray[0]
        MonthLabel.text = DateArray[1]
        YearLabel.text = DateArray[2]
        ServiceTypeLabel.text = ComponentClass.Data.PassArrayDetails[0].ServiceType
        ComponentClassTable = [ComponentClass.Data.PassArrayDetails[0].MobileNumber, ComponentClass.Data.PassArrayDetails[0].VehicleNumber, ComponentClass.Data.PassArrayDetails[0].ServiceType, ComponentClass.Data.PassArrayDetails[0].Time, ComponentClass.Data.PassArrayDetails[0].Dealer, ComponentClass.Data.PassArrayDetails[0].City, ComponentClass.Data.PassArrayDetails[0].Comments]
        addLabelAboveCardView()
        setTag()
    }
    func setTag() {
        let taps:[UITapGestureRecognizer] = [UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:))), UITapGestureRecognizer(target: self, action: #selector(imageTapped2(_:))), UITapGestureRecognizer(target: self, action: #selector(imageTapped3(_:))), UITapGestureRecognizer(target: self, action: #selector(imageTapped4(_:))), UITapGestureRecognizer(target: self, action: #selector(imageTapped5(_:)))]
        var tag = 0
        for button in RatingButtons{
            taps[tag].view?.tag = tag
            button.addGestureRecognizer(taps[tag])
            tag = tag + 1
        }
    }
    @objc func imageTapped(_ sender:UITapGestureRecognizer){
        CopyRatings[0].image = RatingImage[0]
        RatingButtons[0].image = RatingImage[0]
        for i in 1...4{
            RatingButtons[i].image = RatingImage[1]
            CopyRatings[i].image = RatingImage[1]
        }
    }
    @objc func imageTapped2(_ sender:UITapGestureRecognizer){
        for i in 0...1{
            RatingButtons[i].image = RatingImage[0]
            CopyRatings[i].image = RatingImage[0]
        }
        for j in 2...4{
            RatingButtons[j].image = RatingImage[1]
            CopyRatings[j].image = RatingImage[1]
        }
    }
    @objc func imageTapped3(_ sender:UITapGestureRecognizer){
        for i in 0...2{
            RatingButtons[i].image = RatingImage[0]
            CopyRatings[i].image = RatingImage[0]
        }
        for j in 3...4{
            RatingButtons[j].image = RatingImage[1]
            CopyRatings[j].image = RatingImage[1]
        }
        
    }
    
    @objc func imageTapped4(_ sender:UITapGestureRecognizer){
        for i in 0...3{
            RatingButtons[i].image = RatingImage[0]
            CopyRatings[i].image = RatingImage[0]
        }
        RatingButtons[4].image = RatingImage[1]
        CopyRatings[4].image = RatingImage[1]
    }
    
    @objc func imageTapped5(_ sender:UITapGestureRecognizer){
        for i in 0...(RatingButtons.count - 1){
            RatingButtons[i].image = RatingImage[0]
            CopyRatings[i].image = RatingImage[0]
        }}
    
    override func viewDidLayoutSubviews() {
        DashLine.backgroundColor = UIColor.lightGray
        DashLine.frame = CGRect(x: CardView.frame.size.width/2 + CardView.frame.origin.x, y: CardView.frame.origin.y, width: 0.5, height: tableView.frame.origin.y - 2 - CardView.frame.origin.y)
        self.CardView.addSubview(DashLine)
    }
    
    func addLabelAboveCardView(){
        ScopeFlag = 0
        let b = CardView.frame.size.width
        let a = CardView.frame.size.height
        Label.font = UIFont.boldSystemFont(ofSize: 16)
        Label.frame = CGRect(x: CardView.frame.origin.x + (b * 0.88), y: CardView.frame.origin.y - (a * 0.06) , width: b * 0.23, height: a * 0.05)
        Label.textColor = UIColor.white
        Label.textAlignment = .center
        Label.layer.cornerRadius = 2.5
        Label.layer.masksToBounds = true
        var Date = ComponentClass.Data.PassArrayDetails[0].SlotDate
        Date = Date.components(separatedBy: .punctuationCharacters).joined()
        let DateArray:[String] = Date.components(separatedBy: .whitespacesAndNewlines)
        let month = Month.firstIndex(of: DateArray[1])!
        switch minYear == Int(DateArray[2])!  {
        case true:
            switch minMonth == month {
            case true:
                switch minDate >=  Int(DateArray[0])!{
                case true:
                    ScopeFlag = 1
                    break
                case false:
                    ScopeFlag = 0
                    break
                }
            case false:
                switch minMonth > month {
                case true:
                    ScopeFlag = 1
                    break
                case false:
                    ScopeFlag = 0
                    break
    
                }
           
            }
        case false:
            switch minYear > Int(DateArray[2])! {
            case true:
                ScopeFlag = 1
                break
            case false:
                ScopeFlag = 0
                break
         
            }
        
        }
        if ScopeFlag == 1{
            Label.backgroundColor = UIColor.init(rgb: 0x33CC66)
            Label.text = "Past"
        }
        else{
            Label.backgroundColor = UIColor.orange
            Label.text = "New"
        }
        
        self.CardView.addSubview(Label)
        
    }
    @objc func transitionBackTo() {
        self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collection.count
    }
    
    @IBAction func DismissBack(_ sender: Any) {
    self.dismissDet()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: self.collection[indexPath.item], for: indexPath) as! CustomTableViewCell
        cell.textLabel?.text = TitleLabels[indexPath.row]
        cell.detailTextLabel?.text = ComponentClassTable[indexPath.row]
        return cell
    }
    @IBAction func onClickInvoice(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceViewControllerID") as! InvoiceViewController
        self.presentDet(vc)

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
}


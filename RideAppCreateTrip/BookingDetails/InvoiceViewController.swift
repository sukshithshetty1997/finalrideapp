//
//  InvoiceViewController.swift
//  BookingDetails
//
//  Created by Shruthi on 06/06/20.
//  Copyright © 2020 robosoft. All rights reserved.
//
//new shruthi
import UIKit

class InvoiceViewController: UIViewController, UITableViewDataSource,UITableViewDelegate, UIDocumentInteractionControllerDelegate{
    //   let navigationControllerObject = CustomNavigationViewController()
    @IBOutlet weak var viewOutlet: UIView!
    @IBOutlet weak var tableView: UITableView!
    let product = ["iTEM1","iTEM2","iTEM2","iTEM3","TOTAL"]
    let unit = [" 1"," 2"," 2"," 2",""]
    let price = ["1000/-","2000/-","500/-","1000/-","4500/-"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        //        tableView.estimatedRowHeight = 16.0
        //        tableView.rowHeight = UITableView.automaticDimension
    }
    //    @objc func transitionBackTo() {
    //       // self.navigationController?.popViewController(animated: true)
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewCell = tableView.dequeueReusableCell(withIdentifier: "viewCell", for: indexPath as IndexPath) as! CustomTableViewCell
        viewCell.productLabel.text = product[indexPath.row]
        viewCell.unitLabel.text = unit[indexPath.row]
        viewCell.priceLabel.text = price[indexPath.row]
        return viewCell
    }
    
    @IBAction func onClickBackArrow(_ sender: Any) {
   self.dismissDet()
        ///self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func downloadAsPdf(_ sender: Any) {
        generatePDF()
    }
    func generatePDF() {
        let pdfWrittenPath = viewOutlet.exportAsPdfFromView()
        print("PDF written at \(pdfWrittenPath)")
        let dc = UIDocumentInteractionController(url: URL(fileURLWithPath: pdfWrittenPath))
        dc.delegate = self
        dc.annotation = "Sending Data"
        dc.name = "Garage_Service_Bill"
        dc.presentPreview(animated: true)
    }
    
    func presentPreview(animated: Bool) -> Bool{
        return true
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}


extension UIView{
    
    func exportAsPdfFromView() -> String {
        let pdfPageFrame = self.bounds
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
        UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData)
    }
    func saveViewPdf(data: NSMutableData) -> String {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docDirectoryPath = paths[0]
        let pdfPath = docDirectoryPath.appendingPathComponent("Garage_Bill_Services.pdf")
        if data.write(to: pdfPath, atomically: true) {
            return pdfPath.path
        } else {
            return ""
        }
    }
}




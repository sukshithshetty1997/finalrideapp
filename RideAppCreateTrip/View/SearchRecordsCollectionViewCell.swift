//
//  SearchRecordsCollectionViewCell.swift
//  Garage
//
//  Created by Sukshith Shetty on 01/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class SearchRecordsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var Ratings: [UIImageView]!
    @IBOutlet var ServiceTypelabel: UILabel!
    @IBOutlet var DateLabel: UILabel!
    @IBOutlet var MonthLabel: UILabel!
    @IBOutlet var Year: UILabel!
    @IBOutlet var FirstView: UIView!
    @IBOutlet var SecondView: UIView!
    var ThirdView:UIView = UIView()
    var Label1 = UILabel()
    var GoldRatings = UIImage(named: "Star-1")!
    var SilverRatings = UIImage(named: "Star-5")!
    var RatingStars:Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        SecondView.backgroundColor = UIColor.white
        SecondView.clipsToBounds = true
        let a = SecondView.frame.height
        let b = SecondView.frame.width
        Label1.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.regular)
        Label1.frame = CGRect(x: SecondView.frame.origin.x + (b * 0.765), y: SecondView.frame.origin.y - (a * 0.05), width: b * 0.25 , height: a * 0.30)
        self.SecondView.addSubview(Label1)
        Label1.textColor = UIColor.white
        Label1.textAlignment = .center
        Label1.layer.cornerRadius = 5
        Label1.layer.masksToBounds = true
        self.SecondView.addSubview(Label1)
        
    }
    override init(frame: CGRect) {
        
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    override func layoutSubviews() {
        SecondView.layer.cornerRadius = 5
        FirstView.layer.cornerRadius = 5
        FirstView.layer.shadowRadius = 5
        FirstView.layer.shadowColor = UIColor.darkGray.cgColor
        FirstView.layer.shadowOffset = .zero
        FirstView.layer.shadowOpacity = 0.75
    }
}

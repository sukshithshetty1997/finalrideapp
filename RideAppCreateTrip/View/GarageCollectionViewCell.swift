//
//  GarageCollectionViewCell.swift
//  Garage
//
//  Created by Sukshith Shetty on 13/05/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
//protocol Search: class {
//    func Search(cell: GarageCollectionViewCell)
//}

class GarageCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet var ShopName: UILabel!
    
    @IBOutlet var Location: UILabel!
    @IBOutlet var Number: UILabel!
    @IBOutlet var Distance: UILabel!
    
    @IBOutlet var Ratings: [UIImageView]!
}


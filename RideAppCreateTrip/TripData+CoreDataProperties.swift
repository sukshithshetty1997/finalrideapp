//
//  TripData+CoreDataProperties.swift
//  
//
//  Created by Zeeta Andrade on 14/07/20.
//
//

import Foundation
import CoreData


extension TripData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TripData> {
        return NSFetchRequest<TripData>(entityName: "TripData")
    }

    @NSManaged public var destination: String?
    @NSManaged public var endDate: String?
    @NSManaged public var location: String?
    @NSManaged public var name: String?
    @NSManaged public var startDate: String?
    @NSManaged public var startTime: String?

}

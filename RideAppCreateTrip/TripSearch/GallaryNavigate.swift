//
//  File.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 23/07/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation
import UIKit

class GallaryNavigate1: UINavigationController {
    var selectedCellIndex = Int()
    
    override func viewDidLoad() {
        if let vc = self.topViewController as? TripViewController {
            vc.selectedCellTripIndex = self.selectedCellIndex
        }
    }
    
}

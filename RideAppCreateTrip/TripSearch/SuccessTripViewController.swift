//
//  SuccessTripViewController.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 30/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit

class SuccessTripViewController: UIViewController {
    
    let navigationControllerObject = CustomNavigationViewController()
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
    
         navigationControllerObject.navigationBackButtonDesign(controller: self)
        //navigationControllerObject.showNavigationBar(controller: self)
    }
    
   @objc func transitionBackTo() {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CreateTripDone(_ sender: Any) {
//        if let newVC = storyboard?.instantiateViewController(withIdentifier: "GoTripRecordsViewController"),
//            let nc = self.navigationController{
//            nc.setViewControllers([newVC], animated: true)
//        }
//        }
        defaults.set("Two", forKey: "Tag")
        let GoBackTab = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! MainTabBar
        self.presentDet(GoBackTab)
//        let GoBackTab = self.storyboard?.instantiateViewController(withIdentifier: "GoTripRecordsViewController") as! GoTripRecordsViewController
//        self.navigationController?.present(GoBackTab, animated: true, completion: nil)
    }
}
class SuccessJoin: UIViewController {
    
    let navigationControllerObject = CustomNavigationViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationControllerObject.navigationBackButtonDesign(controller: self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func JoinSuccess(_ sender: Any) {
     //   let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
       // self.navigationController?.popToRootViewController(animated: true)
       self.navigationController?.popToRootViewController(animated: true)
    }
    @objc func transitionBackTo(){
        self.navigationController?.popViewController(animated: true)
    }
}

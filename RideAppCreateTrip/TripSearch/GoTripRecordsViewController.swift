//
//  ViewController.swift
//  SearchTrip
//
//  Created by Sukshith Shetty on 26/04/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift

class GoTripRecordsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate{
    let tripInteractorObject = TripsInteractor()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    var tripCoreData = [TripData]()
    var createdTripInfo: TripData?
    var searchData = [TripData]()
    var tripStatus = ""
 var  selectedName = [TripData]()
    var selectedCellIndex = Int()
    var searching = false
    var CompletedStat:TripStatus = TripStatus.Completed
    var UpcomingStat:TripStatus = TripStatus.Upcoming
    var  tripName = [ Trip(TripName: "Maza Mumbai", Date: "27 Jan", Status: TripStatus(rawValue: "Upcoming")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Bangalore", Destination:"Mumbai"), Trip(TripName: "Delhi Belly", Date: "27 Dec", Status: TripStatus(rawValue: "Upcoming")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Orissa", Destination: "Delhi"),
                      Trip(TripName: "Mangalore Mascot", Date: "27 Jan", Status: TripStatus(rawValue: "Completed")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Bangalore", Destination: "Mangalore"), Trip(TripName: "Jaipur Ride", Date: "27 Dec", Status: TripStatus(rawValue: "Completed")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Mangalore", Destination: "Jaipur")]

    @IBOutlet var SearchBar: CustomSearchBar!
    @IBOutlet var UICollectionView: UICollectionView!
    var navigationControllerObject = CustomNavigationViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.hideNavigationBar(controller: self)
        self.tabBarController?.tabBar.isHidden = false
        
        dataStorageInCoreData()
        UICollectionView.delegate = self
        UICollectionView.dataSource = self
        SearchBar.delegate = self
        SearchBar.changeSearchBarColor(color: UIColor.white)
        UICollectionView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let searchBarTextField = SearchBar.value(forKey: "searchField") as? UITextField
        searchBarTextField?.textColor = #colorLiteral(red: 0.6509803922, green: 0.6509803922, blue: 0.6509803922, alpha: 0.87)
        searchBarTextField?.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        self.tabBarController?.tabBar.isHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func dataStorageInCoreData() {
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
    }
    
    func upcomingOrCompletedTrip(tripStartdate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
//        let startDate = dateFormatter.date(from: tripStartdate)
        
        let currentDate = Date()
        let date = dateFormatter.string(from: currentDate)
        
        if tripStartdate < date {
            tripStatus = "completed"
        } else {
            tripStatus = "upcoming"
        }
        return tripStatus
    }

    @IBAction func CreateTrip(_ sender: Any) {
        //let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
        //self.navigationController?.pushViewController(dest, animated: true)
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripViewController") as! CreateTripViewController
        self.navigationController?.pushViewController(dest, animated: true)
//        UserDefaults.standard.set(true, forKey: "SetSegue")
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripNavigationController") as! CreateTripNavigationController
//        self.presentDet(dest)
        //self.present(dest, animated: true, completion: nil)
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripViewController") as! CreateTripViewController
//        self.navigationController?.pushViewController(dest, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching{
            return selectedName.count
        }
        else {
            return tripCoreData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Trip", for: indexPath) as! CustomViewCell
        cell.delegate = self
        cell.addCollectionViewCellShadow()
       
        if searching {
            cell.TripName.text = selectedName[indexPath.item].name!
            cell.Date.text = selectedName[indexPath.item].startDate!
            let status = upcomingOrCompletedTrip(tripStartdate: tripCoreData[indexPath.row].startDate!)
            cell.Status.text = status
//            cell.BackgroundImage.image = selectedName[indexPath.item].BackgroundImage
            
        }
        else{
            
//        cell.TripName.text = tripName[indexPath.item].TripName
//        cell.Date.text = tripName[indexPath.item].Date
//        cell.Status.text = tripName[indexPath.item].Status.rawValue
//        cell.BackgroundImage.image = tripName[indexPath.item].BackgroundImage
            
            cell.TripName.text = tripCoreData[indexPath.row].name
            cell.Date.text = tripCoreData[indexPath.row].startDate
            let status = upcomingOrCompletedTrip(tripStartdate: tripCoreData[indexPath.row].startDate!)
            cell.Status.text = status
//            cell.BackgroundImage.image = tripName[indexPath.item].BackgroundImage
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CustomViewCell
        let cellStatus = cell.Status.text
        
        if cellStatus == "completed"
        {
//            let dest = self.storyboard?.instantiateViewController(withIdentifier: "GalleryNavigate1") as! GallaryNavigate1
            

        if searching{
            SingletonClass.Data.TripName = selectedName[indexPath.row].name!
            SingletonClass.Data.Date = selectedName[indexPath.row].startDate!
            SingletonClass.Data.Home = selectedName[indexPath.row].location!
            SingletonClass.Data.Destination = selectedName[indexPath.row].destination!
            
        }
        else{
            SingletonClass.Data.TripName = tripCoreData[indexPath.row].name!
            SingletonClass.Data.Date = tripCoreData[indexPath.row].startDate!
            SingletonClass.Data.Home = tripCoreData[indexPath.row].location!
            SingletonClass.Data.Destination = tripCoreData[indexPath.row].destination!
        }
            let story: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let new = story.instantiateViewController(withIdentifier: "TripViewController") as! TripViewController
            new.selectedCellTripIndex = indexPath.row
            self.navigationController?.pushViewController(new, animated: true)
            
    }
        else if cellStatus == "upcoming" {
            let dest2 = self.storyboard?.instantiateViewController(withIdentifier: "TripGOViewController") as! TripGOViewController

            if searching{
                SingletonClass.Data.TripName = selectedName[indexPath.row].name!
                SingletonClass.Data.Date = selectedName[indexPath.row].startDate!
                SingletonClass.Data.Home = selectedName[indexPath.row].location!
                SingletonClass.Data.Destination = selectedName[indexPath.row].destination!
                
            }
            else{
                SingletonClass.Data.TripName = tripCoreData[indexPath.row].name!
                SingletonClass.Data.Date = tripCoreData[indexPath.row].startDate!
                SingletonClass.Data.Home = tripCoreData[indexPath.row].location!
                SingletonClass.Data.Destination = tripCoreData[indexPath.row].destination!
            }
            dest2.selectedCellTripIndex = indexPath.row
             self.navigationController?.pushViewController(dest2, animated: true)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
//        if searchText == ""{
//           // lazy tripName = self.tripName
//            searching = false
//            UICollectionView.reloadData()
//        }
//        else{
//        //let searchText = searchText.lowercased()
//       // selectedName = tripName.filter({$0.TripName.lowercased().contains(searchText)})
//            selectedName = tripName.filter({$0.TripName.prefix(searchText.count) == searchText})
//        searching = true
//        UICollectionView.reloadData()
//
//        }
//       // UICollectionView.reloadData()
        print(tripCoreData)
        guard !searchText.isEmpty else { selectedName = tripCoreData ; UICollectionView.reloadData(); return}
        selectedName = tripCoreData.filter({ tripCoreData -> Bool in
            tripCoreData.name?.prefix(searchText.count).contains(searchText) ?? false
        })
        searching = true
        UICollectionView.reloadData()
    }
    
    }
    

extension GoTripRecordsViewController: Close{
    func close(cell: CustomViewCell){
        var tripId = ""
        if let indexpath = UICollectionView?.indexPath(for: cell){
            if searching{
                selectedName.remove(at: indexpath.item)
                UICollectionView.deleteItems(at: [indexpath])
//                selectedName.remove(at: indexpath.item)
            }
            else{
             tripCoreData.remove(at: indexpath.item)
            UICollectionView.deleteItems(at: [indexpath])
            }
            tripId = CoreDataBaseHelper.shareInstance.deleteTrip(index: indexpath.item)
            print(tripId)
            deleteTrip(tripId)
        }
    }
    
    func deleteTrip(_ tripId: String) {
        guard let token = keychain.get(Keys.accessToken) else {
            return
        }
        let tripResponse = tripInteractorObject.deleteTrip(tripID: tripId, requestHeaders: ["Authorization": "Bearer \(token)"])
        print(tripResponse)
        if let data = tripResponse.data?.message {
            print(data)
        } else if let error = tripResponse.name, let message = tripResponse.message {
            print(error)
            print(message)
        }
        
    }
}

extension Date {
    var isPastDate: Bool {
        return self < Date()
    }
}


//
//  JoinTripRecordsViewController.swift
//  TripSearch
//
//  Created by Sukshith Shetty on 11/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import KeychainSwift

class JoinTripRecordsViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate{
    
    let navigationControllerObject = CustomNavigationViewController()
    let tripInteractorObject = TripsInteractor()
    let keychain = KeychainSwift(keyPrefix: Keys.keyPrefix.key1)
    var tripCoreData = [TripData]()
    var createdTripInfo: TripData?
    var searchData = [TripData]()
    var tripStatus = ""
    var  selectedName = [TripData]()
//    var  selectedName = [Trip]()
    var searching = false
    
    var  tripName = [ Trip(TripName: "Maza Mumbai", Date: "27 Jan", Status: TripStatus(rawValue: "Completed")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Bangalore", Destination:"Mumbai"), Trip(TripName: "Delhi Belly", Date: "27 Dec", Status: TripStatus(rawValue: "Upcoming")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Orissa", Destination: "Delhi"),
                      Trip(TripName: "Mangalore Mascot", Date: "27 Jan", Status: TripStatus(rawValue: "Completed")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Bangalore", Destination: "Mangalore"), Trip(TripName: "Jaipur Ride", Date: "27 Dec", Status: TripStatus(rawValue: "Upcoming")!, BackgroundImage:UIImage(named: "GOA")!,Home: "Mangalore", Destination: "Jaipur")]
    
    @IBOutlet var CreateTripButton: UIButton!
    @IBOutlet var SearchBar: CustomSearchBar!
    @IBOutlet var UICollectionView: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        navigationControllerObject.hideNavigationBar(controller: self)
    
        dataStorageInCoreData()
        UICollectionView.delegate = self
        UICollectionView.dataSource = self
        SearchBar.delegate = self
      //  SearchBar.changeSearchBarColor(color: .white)
        UICollectionView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let searchBarTextField = SearchBar.value(forKey: "searchField") as? UITextField
        searchBarTextField?.textColor = #colorLiteral(red: 0.6509803922, green: 0.6509803922, blue: 0.6509803922, alpha: 0.87)
        searchBarTextField?.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        self.navigationItem.hidesBackButton = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.isTranslucent = false
        navigationControllerObject.showNavigationBar(controller: self)
    }
    
    func dataStorageInCoreData() {
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
    }

    @IBAction func CreateTrip(_ sender: Any) {
        //let dest = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripViewController") as! CreateTripViewController
        //self.navigationController?.pushViewController(dest, animated: true)
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
//        self.present(dest, animated: true, completion: nil)
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "CreateTripViewController") as! CreateTripViewController
        self.navigationController?.pushViewController(dest, animated: true)
    }
    
    func upcomingOrCompletedTrip(tripStartdate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        //        let startDate = dateFormatter.date(from: tripStartdate)
        
        let currentDate = Date()
        let date = dateFormatter.string(from: currentDate)
        
        if tripStartdate < date {
            tripStatus = "completed"
        } else {
            tripStatus = "upcoming"
        }
        return tripStatus
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching{
            return selectedName.count
        }
        else {
            return tripCoreData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Trip", for: indexPath) as! CustomViewCell
        cell.delegate = self
        cell.addCollectionViewCellShadow()
        
        if searching {
            cell.TripName.text = selectedName[indexPath.item].name
            cell.Date.text = selectedName[indexPath.item].startDate
//            cell.Status.text = selectedName[indexPath.item].Status.rawValue
//            cell.BackgroundImage.image = selectedName[indexPath.item].BackgroundImage
 
        }
        else{
            
            cell.TripName.text = tripCoreData[indexPath.item].name
            cell.Date.text = tripCoreData[indexPath.item].startDate
            let status = upcomingOrCompletedTrip(tripStartdate: tripCoreData[indexPath.row].startDate!)
            cell.Status.text = status
//            cell.BackgroundImage.image = tripName[indexPath.item].BackgroundImage
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Inside1")
        
        let cell = collectionView.cellForItem(at: indexPath) as! CustomViewCell
        let cellStatus = cell.Status.text
        
        if cellStatus == "upcoming" {
//        if tripName[indexPath.row].Status == TripStatus.Upcoming{
        let dest1 = self.storyboard?.instantiateViewController(withIdentifier: "JoinTripViewController") as! JoinTripViewController
        if searching{
            
            SingletonClass.Data.TripName = selectedName[indexPath.row].name!
            SingletonClass.Data.Date = selectedName[indexPath.row].startDate!
            SingletonClass.Data.Home = selectedName[indexPath.row].location!
            SingletonClass.Data.Destination = selectedName[indexPath.row].destination!
      
        }
        else{
            SingletonClass.Data.TripName = tripCoreData[indexPath.row].name!
            SingletonClass.Data.Date = tripCoreData[indexPath.row].startDate!
            SingletonClass.Data.Home = tripCoreData[indexPath.row].location!
            SingletonClass.Data.Destination = tripCoreData[indexPath.row].destination!

        }
            dest1.selectedCellTripIndex = indexPath.row
        self.navigationController?.pushViewController(dest1, animated: true)
        }
        else if cellStatus == "completed" {
        
            if searching{
                SingletonClass.Data.TripName = selectedName[indexPath.row].name!
                SingletonClass.Data.Date = selectedName[indexPath.row].startDate!
                SingletonClass.Data.Home = selectedName[indexPath.row].location!
                SingletonClass.Data.Destination = selectedName[indexPath.row].destination!
                
            }
            else{
                SingletonClass.Data.TripName = tripCoreData[indexPath.row].name!
                SingletonClass.Data.Date = tripCoreData[indexPath.row].startDate!
                SingletonClass.Data.Home = tripCoreData[indexPath.row].location!
                SingletonClass.Data.Destination = tripCoreData[indexPath.row].destination!
            }
            let story: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let new = story.instantiateViewController(withIdentifier: "TripViewController") as! TripViewController
            new.selectedCellTripIndex = indexPath.row
            self.navigationController?.pushViewController(new, animated: true)
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchText == ""{
            // lazy tripName = self.tripName
            searching = false
            UICollectionView.reloadData()
        }
        else{
            //let searchText = searchText.lowercased()
            // selectedName = tripName.filter({$0.TripName.lowercased().contains(searchText)})
            selectedName = tripCoreData.filter({$0.name!.prefix(searchText.count) == searchText})
            searching = true
            UICollectionView.reloadData()
        }
    }
}

extension JoinTripRecordsViewController: Close{
    func close(cell: CustomViewCell){
        var tripId = ""
        if let indexpath = UICollectionView?.indexPath(for: cell){
            if searching{
                selectedName.remove(at: indexpath.item)
                UICollectionView.deleteItems(at: [indexpath])
                tripCoreData.remove(at: indexpath.item)
            }
            else{
                tripCoreData.remove(at: indexpath.item)
                UICollectionView.deleteItems(at: [indexpath])
            }
            tripId = CoreDataBaseHelper.shareInstance.deleteTrip(index: indexpath.item)
            print(tripId)
            deleteTrip(tripId)
        }
    }
    func deleteTrip(_ tripId: String) {
        guard let token = keychain.get(Keys.accessToken) else {
            return
        }
        let tripResponse = tripInteractorObject.deleteTrip(tripID: tripId, requestHeaders: ["Authorization": "Bearer \(token)"])
        print(tripResponse)
        if let data = tripResponse.data?.message {
            print(data)
        } else if let error = tripResponse.name, let message = tripResponse.message {
            print(error)
            print(message)
        }
        
    }
}

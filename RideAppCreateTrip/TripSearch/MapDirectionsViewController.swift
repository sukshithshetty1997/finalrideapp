//
//  ViewController.swift
//  Directions
//
//  Created by Mac on 9/2/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class AnnotationClass: NSObject {
    static let Data = AnnotationClass()
    private override init() {}
    var PlaceTitle:String = ""
}

class customPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(pinTitle:String, pinSubTitle:String, location:CLLocationCoordinate2D) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
    }
  
}

class MapDirectionsViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    let navigationControllerObject = CustomNavigationViewController()
    var tripCoreData = [TripData]()
    var createdTripInfo: TripData?
    var selectedCellTripIndex = Int()
    let reach = try? Reachability()
    var but:UIBarButtonItem!
    var destination:String = ""
    var Home:String = ""
    var MapTitle:String = ""
    var DestinationLocation:CLLocation!
    var SourceLocation:CLLocation!
    @IBOutlet var map: MKMapView!
    @IBOutlet var NavigateGPS: UIButton!
    var Flag = 0
    var On_Off = true
    var locationManger = CLLocationManager()
    var sourcepin:MKAnnotation!
    var destinationpin:MKAnnotation!
    var route:MKRoute!
    var flex1:UIBarButtonItem!
    var flex2:UIBarButtonItem!
    var NavigationButton:[UIBarButtonItem]!
    var tag = 0
    
    override func viewWillAppear(_ animated: Bool) {
        tag = 0
        //self.navigationController?.navigationBar.barTintColor = UIColor.white
       //  NavigateGPS.backgroundColor = UIColor.white
        self.navigationController?.isToolbarHidden = false
        self.navigationController?.setToolbarHidden(false, animated: true)
        self.navigationController?.toolbar.isHidden = false
        super.viewWillAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationControllerObject.navigationBackButtonDesign(controller: self)
        tag = 0
        NavigateGPS.backgroundColor = UIColor.white
        self.navigationController?.setToolbarHidden(false, animated: false)
        self.navigationController?.isToolbarHidden = false
        self.navigationController?.toolbar.barTintColor = UIColor.orange
        
        //Core Data
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
        createdTripInfo = tripCoreData[selectedCellTripIndex]
        
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        locationManger.distanceFilter = kCLDistanceFilterNone
        map.showsUserLocation = true
        map.delegate = self
        map.showsPointsOfInterest = true
        map.showsBuildings = true
        map.showsTraffic = true
        map.userTrackingMode = MKUserTrackingMode.followWithHeading
        map.isZoomEnabled = true
        map.isScrollEnabled = true

        flex1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        flex2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        but = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.play, target: self, action: #selector(Play_Pause(_:)))
        but.tintColor = UIColor.white
        self.setToolbarItems([flex1,but,flex2], animated: true)
        
        let cardbutton = UIBarButtonItem(image: UIImage(named: "insert-card"), style: UIBarButtonItem.Style.done, target: self, action: #selector(ATMPress))
        let gasStationButton = UIBarButtonItem(image: UIImage(named: "gas-station"), style: .done, target: self, action: #selector(PetrolPress))
        let bedButton =  UIBarButtonItem(image: UIImage(named: "bed"), style: UIBarButtonItem.Style.done, target: self, action: #selector(BedPress))
        let restButton = UIBarButtonItem(image: UIImage(named: "restaurant"), style: .done, target: self, action: #selector(FoodPress))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: self, action: nil)
        space.width = 35
        NavigationButton = [space,restButton,space,bedButton,space,gasStationButton,space,cardbutton,space]
        self.navigationItem.rightBarButtonItems = NavigationButton
        for i in 0...(NavigationButton.count-1){
            self.NavigationButton[i].tintColor = UIColor.orange
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(InternetChanged), name: Notification.Name.reachabilityChanged, object: reach)
        do {
            try reach?.startNotifier()
        }
        catch{
            print("Error")
        }
        getAddress()
    }
    override func viewWillDisappear(_ animated: Bool) {
     self.navigationController?.navigationBar.isTranslucent = false
     self.navigationController?.navigationBar.barTintColor = UIColor.orange
    //self.navigationController?.view.backgroundColor = UIColor.orange
        self.navigationController?.isToolbarHidden = true
        self.navigationController?.setToolbarHidden(true, animated: false)
        super.viewWillDisappear(animated)
    }
    func checkNet(){
        reach!.whenUnreachable = { _ in
            DispatchQueue.main.async {
                print("no net")
                let alert = UIAlertController(title: "Internet Connection not Reachable", message: "You need to turn on your internet to access the updated page", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        reach?.whenReachable = { _ in
            DispatchQueue.main.async {
                self.viewDidLoad()
                self.viewWillAppear(true)
            }
        }
    }
    
    @objc func InternetChanged(note: Notification){
        let reachability = note.object as? Reachability
        if reachability!.connection != .unavailable{
            DispatchQueue.main.async {
                print("Available")
                self.viewDidLoad()
                self.viewWillAppear(true)
                self.reloadInputViews()
            }
        }
        else if reachability!.connection == .unavailable{
            DispatchQueue.main.async {
                print("Unavailable")
                let alert = UIAlertController(title: "Internet Connection not Reachable", message: "You need to turn on your internet to access the updated page", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func GPSBack(_ sender: Any) {
        let current = self.map.userLocation
        var region = MKCoordinateRegion(center: current.coordinate, latitudinalMeters: current.coordinate.latitude, longitudinalMeters: current.coordinate.longitude)
        let Span = MKCoordinateSpan(latitudeDelta: 0.0025, longitudeDelta: 0.0025)
        region.span = Span
        //self.map.setCenter(current.coordinate, animated: true)
        self.map.setRegion(region, animated: true)
    }
    
    @objc func transitionBackTo() {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func Play_Pause(_ sender: Any) {
        print(On_Off)
        if On_Off{
            Stop()
        }
        else if !On_Off{
            Start()
        }
    }
    
    @objc func Start() {
        locationManger.stopUpdatingLocation()
        //GetDirections.isEnabled = false
        but = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(Play_Pause(_:)))
        self.setToolbarItems([flex1,but,flex2], animated: true)
        but.tintColor = UIColor.white
        On_Off = !On_Off
    }
    
    @objc func Stop(){
        locationManger.startUpdatingLocation()
        //GetDirections.isEnabled = true
           but = UIBarButtonItem(barButtonSystemItem: .pause, target: self, action: #selector(Play_Pause(_:)))
        self.setToolbarItems([flex1,but,flex2], animated: true)
        but.tintColor = UIColor.white
        On_Off = !On_Off
    }
    
    @objc func BedPress(){
        self.saveObject(fileName: "PlaceTitle", object: "Lodge")
        let ArraySend = ["lodge","Inn","motel","hotel"]
        populate(title: "Lodge",array: ArraySend)
    }
    
    @objc func ATMPress(){
         self.saveObject(fileName: "PlaceTitle", object: "ATM")
        let ArraySend = ["ATM","bank"]
        populate(title: "ATM", array: ArraySend)
    }
    @objc func PetrolPress(){
        self.saveObject(fileName: "PlaceTitle", object: "Petrol")
        let ArraySend = ["petrol", "gas stations", "fuel","petrol station", "car garage"]
        populate(title: "Petrol", array: ArraySend)
    }
    @objc func FoodPress(){
        self.saveObject(fileName: "PlaceTitle", object: "Food")
        let ArraySend = ["Food", "Snacks", "Breakfast", "Lunch","Dinner", "Cafe", "Restaurant","Bar","Drink"]
        AnnotationClass.Data.PlaceTitle = "Food"
        populate(title: "Food", array: ArraySend)
    }
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        print("Yess **** \(String(describing: createdTripInfo?.destination))")
        geoCoder.geocodeAddressString((createdTripInfo?.destination)!) { (placemarks, error) in
            guard let placemarks = placemarks,  let Dest = placemarks.first?.location
                else {
                    print("No Location Found")
                    return
            }
            geoCoder.geocodeAddressString((self.createdTripInfo?.location)!) { (placemarks, error) in
                guard let placemarks = placemarks,  let Source = placemarks.first?.location
                    else{
                        print("No Location Found")
                        return
                }
                self.mapThis(destinationCord: Dest.coordinate, sourceCord: Source.coordinate)
                }}
    }
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        let annotationView = views.last!
        if let annotation = annotationView.annotation{
            if annotation is MKUserLocation{
                var region = MKCoordinateRegion()
                region.center = map.userLocation.coordinate
                region.span.latitudeDelta = 0.005
                region.span.longitudeDelta = 0.005
                map.setRegion(region, animated: true)
            }
        }
    }
    func populate(title: String, array:[String]){
        var region = MKCoordinateRegion()
        region.center = CLLocationCoordinate2DMake(map.userLocation.coordinate.latitude, map.userLocation.coordinate.longitude)
        let request = MKLocalSearch.Request()
        request.region = region
        for tag in array{
            request.naturalLanguageQuery = tag
            let search = MKLocalSearch(request: request)
            search.start { (response, error) in
                guard let response = response else {return}
                for item in response.mapItems{
                    let distance = self.locationManger.location?.distance(from:item.placemark.location!) ?? 0
                    let dist = String(format: "%.2f", Double(round(distance)/1000))
                    let annotation = MKPointAnnotation()
                    let annot2 = MKAnnotationView(annotation: annotation, reuseIdentifier: title)
                    annot2.canShowCallout = true
                    AnnotationClass.Data.PlaceTitle = title
                    annot2.annotation = annotation
                    annot2.isEnabled = true
                    annotation.coordinate = item.placemark.coordinate
                    self.MapTitle = item.name!
                    annotation.title = self.MapTitle
                    annotation.subtitle = "Distance from current Location: " + dist + "km"
                    DispatchQueue.main.async {
                        self.map.addAnnotation(annotation)}
                }
            }
        }
        
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        let NameTag = getObject(fileName: "PlaceTitle") as? String
        switch NameTag! {
        case "Food":
            let FlagAnnot = MKAnnotationView(annotation: annotation, reuseIdentifier: "Food")
            FlagAnnot.canShowCallout = true
            let AnnotImage = UIImageView(image: UIImage(named: "restaurant")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate))
            AnnotImage.tintColor = UIColor.darkGray
            FlagAnnot.image = AnnotImage.image
            return FlagAnnot
        case "ATM":
            let FlagAnnot = MKAnnotationView(annotation: annotation, reuseIdentifier: "ATM")
            FlagAnnot.canShowCallout = true
            FlagAnnot.image = UIImage(named: "insert-card")
            return FlagAnnot
            
        case "Lodge":
            let FlagAnnot = MKAnnotationView(annotation: annotation, reuseIdentifier: "Lodge")
            FlagAnnot.canShowCallout = true
            FlagAnnot.image = UIImage(named: "bed")
            return FlagAnnot
            
        case "Petrol":
            let FlagAnnot = MKAnnotationView(annotation: annotation, reuseIdentifier: "Petrol")
            FlagAnnot.canShowCallout = true
            FlagAnnot.image = UIImage(named: "gas-station")
            return FlagAnnot
        case "Destination":
            let FlagAnnot = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "Destination")
            FlagAnnot.canShowCallout = true
            FlagAnnot.animatesWhenAdded = true
            FlagAnnot.isUserInteractionEnabled = true
            if tag == 0{
                FlagAnnot.glyphText = createdTripInfo?.destination
                FlagAnnot.glyphTintColor = UIColor.black
                FlagAnnot.markerTintColor = UIColor.green
                tag = tag + 1
            }
            else if tag == 1{
                FlagAnnot.glyphText = createdTripInfo?.location
                FlagAnnot.glyphTintColor = UIColor.black
                FlagAnnot.markerTintColor = UIColor.red
            }
            return FlagAnnot
        default:
            return nil
        }
    }
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        var region = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: userLocation.coordinate.latitude, longitudinalMeters: userLocation.coordinate.longitude)
        region.span.latitudeDelta = 0.005
        region.span.longitudeDelta = 0.005
        mapView.setRegion(region, animated: true)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last!
        var region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: location.coordinate.latitude, longitudinalMeters: location.coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        region.span = span
        map.setRegion(region, animated: true)    }
    
    func mapThis(destinationCord : CLLocationCoordinate2D, sourceCord: CLLocationCoordinate2D) {
         AnnotationClass.Data.PlaceTitle = "Destination"
        sourcepin = customPin(pinTitle: (createdTripInfo?.location)!, pinSubTitle: "Your Current Location", location: sourceCord)
        destinationpin = customPin(pinTitle: (createdTripInfo?.destination)!, pinSubTitle: "Your Planned Destination", location: destinationCord)
        map.removeAnnotation(destinationpin)
        self.map.addAnnotation(sourcepin)
        self.map.addAnnotation(destinationpin)
        let soucePlaceMark = MKPlacemark(coordinate: sourceCord)
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
        
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print("Something is wrong", error)
                }
                return
            }
            if self.route != nil{
                self.map.removeOverlay(self.route.polyline)
            }
           self.route = response.routes[0]
            self.map.addOverlay(self.route.polyline)
          self.map.setVisibleMapRect(self.route.polyline.boundingMapRect, animated: true)
        }
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        render.lineWidth = 5
        return render
    }

}


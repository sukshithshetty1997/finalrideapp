//
//  JoinTripViewController.swift
//  TripSearch
//
//  Created by Sukshith Shetty on 10/06/20.
//  Copyright © 2020 Sukshith Shetty. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class JoinTripViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    var navigationControllerObject = CustomNavigationViewController()
    var tripCoreData = [TripData]()
    var createdTripInfo: TripData?
    var mileStoneData = [MileStone]()
    var ridersData = [Riders]()
    var selectedCellTripIndex = Int()
    
    let reach = try? Reachability()
    var LineView:UIView = UIView()
    var locationManger = CLLocationManager()
    var sourcepin:MKAnnotation!
    var destinationpin:MKAnnotation!
    var route:MKRoute!
//    var TripInfo = CreateTripInfoModel(TripName: "Haryana GO", Date: "15-22 Nov, 2019", Home: "Mumbai", Destination: "Haryana", Time: "8:00 am")
    @IBOutlet var Date: UILabel!
    @IBOutlet var Home: UILabel!
    @IBOutlet var Time: UILabel!
    @IBOutlet var TripName: UILabel!
    @IBOutlet var Destination: UILabel!
    @IBOutlet var MapView: MKMapView!
    @IBOutlet var TripView: ViewClass!
    @IBOutlet var ContactRecords: UICollectionView!
    @IBOutlet var RecommendationRecords: UICollectionView!
    @IBOutlet var MileStoneRecords: UICollectionView!
    @IBOutlet weak var milestoneCollectionViewHeight: NSLayoutConstraint!
    var MileStoneInfo = [Milestone(MilestoneNumber: 1, From: "Mumbai", To: "Pune", Distance: 160, Time: "6 h 55 min"),Milestone(MilestoneNumber: 2, From: "Pune", To: "Raipur", Distance: 350, Time: "8 h 56 min"), Milestone(MilestoneNumber: 3, From: "Raipur", To: "Haryana", Distance: 200, Time: "5 h 19 min")]
    var Recommendations:[String] = ["Riding Gear", "Winter Wear", "Drinking Water"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationControllerObject.navigationBarDesign(controller: self, title: "Trip Summary")
        self.tabBarController?.tabBar.isHidden = true
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        dataStorageInCoreData()
        
        TripName.text = createdTripInfo?.name
        Date.text = createdTripInfo?.startDate
        Home.text = createdTripInfo?.location
        Destination.text = createdTripInfo?.destination
        Time.text = createdTripInfo?.startTime
        viewDidLayoutSubviews()
        MapOn()
        CallMap(MapView: MapView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(InternetChanged), name: Notification.Name.reachabilityChanged, object: reach)
        do {
            try reach?.startNotifier()
            
        }
        catch{
            print("Error")
        }

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        super.viewWillDisappear(animated)
    }
    
    func dataStorageInCoreData() {
        tripCoreData = CoreDataBaseHelper.shareInstance.getTripData()
        createdTripInfo = tripCoreData[selectedCellTripIndex]
        
        guard let trip = createdTripInfo
            else { return }
        if createdTripInfo?.mileStones?.allObjects != nil {
            mileStoneData = createdTripInfo?.mileStones?.allObjects as! [MileStone]
        }
        //        mileStoneData = CoreDataBaseHelper.shareInstance.getMilestoneData()
        
        if createdTripInfo?.riders?.allObjects != nil {
            ridersData = createdTripInfo?.riders?.allObjects as! [Riders]
        }
    }
    
    func checkNet(){
        reach!.whenUnreachable = { _ in
            DispatchQueue.main.async {
                print("no net")
                let alert = UIAlertController(title: "Internet Connection not Reachable", message: "You need to turn on your internet to access the updated page", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        reach?.whenReachable = { _ in
            DispatchQueue.main.async {
                self.viewDidLoad()
                self.viewWillAppear(true)
            }
        }
    }
    
    @objc func InternetChanged(note: Notification){
        let reachability = note.object as? Reachability
        if reachability!.connection != .unavailable{
            DispatchQueue.main.async {
                print("Available")
                self.viewDidLoad()
                self.viewWillAppear(true)
                self.MapView.reloadInputViews()
                self.reloadInputViews()
            }
        }
        else if reachability!.connection == .unavailable{
            DispatchQueue.main.async {
                print("Unavailable")
                let alert = UIAlertController(title: "Internet Connection not Reachable", message: "You need to turn on your internet to access the updated page", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    
    @objc func transitionBackTo() {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLayoutSubviews() {
        LineView.frame = CGRect(x: Home.frame.origin.x + Home.frame.size.width + 5, y: Home.frame.origin.y + Home.frame.size.height/2, width: Destination.frame.origin.x - 5 - LineView.frame.origin.x, height: 1)
        LineView.backgroundColor = UIColor.lightGray
        self.TripView.addSubview(LineView)
        //Dynamic collection view height
        let height = MileStoneRecords.collectionViewLayout.collectionViewContentSize.height
        milestoneCollectionViewHeight.constant = height
        self.view.layoutIfNeeded()
    }
    func CallMap(MapView: MKMapView) {
        let tapMap = UITapGestureRecognizer(target: self, action: #selector(OpenMap))
        tapMap.numberOfTapsRequired = 1
        MapView.addGestureRecognizer(tapMap)
    }

    @objc func OpenMap(){
//        let dest = self.storyboard?.instantiateViewController(withIdentifier: "MapNavigationController") as! UINavigationController
//        self.navigationController?.present(dest, animated: true, completion: nil)
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "MapDirectionsViewController") as! MapDirectionsViewController
        dest.selectedCellTripIndex = selectedCellTripIndex
        self.navigationController?.pushViewController(dest, animated: true)
    }

    @IBAction func JoinButton(_ sender: Any) {
        let dest = self.storyboard?.instantiateViewController(withIdentifier: "SuccessJoin") as! SuccessJoin
        self.navigationController?.pushViewController(dest, animated: true)
    }
    
    @IBAction func ChatApp(_ sender: Any) {
        print("Open Chat App")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case MileStoneRecords:
            return mileStoneData.count
        case RecommendationRecords:
            return Recommendations.count
        case ContactRecords:
            return ridersData.count
        default:
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case MileStoneRecords:
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "Milestone", for: indexPath) as! MileStoneCollectionViewCell
            
//            cell1.MilStoneLabel.text = "Milestone" + String(MileStoneInfo[indexPath.row].MilestoneNumber)
//            cell1.FromLabel.text = MileStoneInfo[indexPath.row].From
//            cell1.ToLabel.text = MileStoneInfo[indexPath.row].To
//            cell1.Dist_TimeLabel.text = String(MileStoneInfo[indexPath.row].Distance) + "km - " + MileStoneInfo[indexPath.row].Time
            
            for milestoneInfo in mileStoneData {
                guard let count = Int(milestoneInfo.milestoneNumber ?? "0")
                    else { return cell1 }
                if count != indexPath.row + 1 {
                } else {
                    cell1.MilStoneLabel.text = "Milestone " +  milestoneInfo.milestoneNumber!
                    cell1.FromLabel.text = milestoneInfo.fromLocation
                    cell1.ToLabel.text = milestoneInfo.toLocation
                    let distanceTime = milestoneInfo.distance! + " - " + milestoneInfo.time!
                    cell1.Dist_TimeLabel.text = distanceTime
                    break
                }
            }
            return cell1
        case RecommendationRecords:
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "Stuff", for: indexPath) as! RecommendationCollectionViewCell
             cell2.RecommendationCell!.text = Recommendations[indexPath.row]
            return cell2
        case ContactRecords:
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "Contacts", for: indexPath)
            return cell3
        default:
            return UICollectionViewCell.init()
            
        }
    }
}
extension JoinTripViewController: CLLocationManagerDelegate, MKMapViewDelegate{
    func MapOn(){
        locationManger.delegate = self
        locationManger.desiredAccuracy = kCLLocationAccuracyBest
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        locationManger.distanceFilter = kCLDistanceFilterNone
        MapView.showsUserLocation = true
        MapView.delegate = self
        getAddress()
    }
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString((createdTripInfo?.destination)!) { (placemarks, error) in
            guard let placemarks = placemarks,  let Dest = placemarks.first?.location
                else {
                    print("No Location Found")
                    return
            }
            geoCoder.geocodeAddressString((self.createdTripInfo?.location)!) { (placemarks, error) in
                guard let placemarks = placemarks,  let Source = placemarks.first?.location
                    else{
                        print("No Location Found")
                        return
                }
                self.mapThis(destinationCord: Dest.coordinate, sourceCord: Source.coordinate)
                
            }
            
        }
        print(createdTripInfo?.destination)
        print(createdTripInfo?.location)
        }
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            print(locations)
        }
        
        func mapThis(destinationCord : CLLocationCoordinate2D, sourceCord: CLLocationCoordinate2D) {
//            if destinationpin != nil{
//                MapView.removeAnnotation(destinationpin)
//            }
      //      let souceCordinate = (locationManger.location?.coordinate)!
            sourcepin = customPin(pinTitle: (createdTripInfo?.location)!, pinSubTitle: "Your Current Location", location: sourceCord)
            destinationpin = customPin(pinTitle: (createdTripInfo?.destination)!, pinSubTitle: "Your Planned Destination", location: destinationCord)
            MapView.removeAnnotation(destinationpin)
            self.MapView.addAnnotation(sourcepin)
            self.MapView.addAnnotation(destinationpin)
            let soucePlaceMark = MKPlacemark(coordinate: sourceCord)
            let destPlaceMark = MKPlacemark(coordinate: destinationCord)
            
            let sourceItem = MKMapItem(placemark: soucePlaceMark)
            let destItem = MKMapItem(placemark: destPlaceMark)
            
            let destinationRequest = MKDirections.Request()
            destinationRequest.source = sourceItem
            destinationRequest.destination = destItem
            destinationRequest.transportType = .automobile
            destinationRequest.requestsAlternateRoutes = true
            
            let directions = MKDirections(request: destinationRequest)
            directions.calculate { (response, error) in
                guard let response = response else {
                    if let error = error {
                        print("Something is wrong", error)
                    }
                    return
                }
                if self.route != nil{
                    self.MapView.removeOverlay(self.route.polyline)
                }
                self.route = response.routes[0]
                self.MapView.addOverlay(self.route.polyline)
                self.MapView.setVisibleMapRect(self.route.polyline.boundingMapRect, animated: true)
            }
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            
            let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
            render.strokeColor = .blue
            render.lineWidth = 5
            return render
        }
        
        
    }
    

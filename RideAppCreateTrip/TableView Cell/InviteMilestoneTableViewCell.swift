//
//  InvireMilestoneTableViewCell.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 25/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class InviteMilestoneTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageLabelVerticalLayout: NSLayoutConstraint!
    @IBOutlet weak var imageLabelCellView: UIView!
    @IBOutlet weak var milestoneHeightLayout: NSLayoutConstraint!
    @IBOutlet weak var invitedView: UIView!
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var milestoneContainerView: UIView!
    
    @IBOutlet weak var cellImageContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        milestoneContainerView.layer.cornerRadius = 18.0
        milestoneContainerView.layer.shadowColor = UIColor.darkGray.cgColor
        milestoneContainerView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        milestoneContainerView.layer.shadowRadius = 25.0
        milestoneContainerView.layer.shadowOpacity = 0.2
        
        cellImageContainerView.layer.cornerRadius = cellImageContainerView.frame.height / 2
        cellImageContainerView.layer.shadowColor = UIColor.darkGray.cgColor
        cellImageContainerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cellImageContainerView.layer.shadowRadius = 5.0
        cellImageContainerView.layer.shadowOpacity = 0.5
        
        cellImageView.layer.cornerRadius = cellImageView.frame.height / 2
        cellImageView.clipsToBounds = true
        
        
    }

}


//
//  ContactInfoTableViewCell.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 28/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class ContactInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var contactNamesLabel: UILabel!
    
    var link: InviteOtherRidersViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //backgroundColor = .red
        
        
        //starButton.setTitle("Something", for: .normal)
       // starButton.setImage(UIImage(named: "star"), for: .normal)
        //starButton.setImage(UIImage(named: "star"), for: .normal)
        //starButton.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        //starButton.tintColor = .white
        
        //starButton.addTarget(self, action: #selector(handleFavoriteMark), for: .touchUpInside)
        //accessoryView = starButton
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   /* @objc func handleFavoriteMark() {
        print("Marking as favorite")
        
        starButton.tintColor = .red
        link?.someFunctionIWantToCall(cell: self)
    }
    */
    
    
    

}

//
//  ExpandableContactNames.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 21/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation
import UIKit
import Contacts

struct ExpandableNames {
    var isExpanded: Bool
    var names: [FavoritableContact]
}
struct FavoritableContact {
    
    var contact: CNContact
    
    var hasFavorited: Bool
    var hasChecked: Bool

    /*
 init(starButton: UIButton, contact: CNContact, checkMarkButton: UIButton) {
        self.starButton = starButton
        self.contact = contact
        self.checkMarkButton = checkMarkButton
    }
 */
}

struct ContactStruct {
    var givenName: String
    var familyName: String
    var number: String
}




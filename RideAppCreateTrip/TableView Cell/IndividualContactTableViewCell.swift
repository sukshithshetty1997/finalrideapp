//
//  IndividualContactTableViewCell.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 29/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class IndividualContactTableViewCell: UITableViewCell {

    var link = InviteOtherRidersViewController()
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkMarkButton: UIButton!
    @IBOutlet weak var starButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        starButton.setImage(UIImage(named: "star")?.withRenderingMode(.alwaysTemplate), for: .normal)
        starButton.tintColor = UIColor.white
        
        starButton.addTarget(self, action: #selector(setContactAsFavorite(sender: )), for: .touchUpInside)
        
        checkMarkButton.addTarget(self, action: #selector(contactNameTapped(sender: )), for: .touchUpInside)
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
   
    @objc func contactNameTapped(sender: UIButton) {
        checkMarkButton.setImage(UIImage(named: "Group-1"), for: .normal)
        link.someContactNameTapped(sender: self)
    }
    
    @objc func setContactAsFavorite(sender: UIButton) {
        //starButton.setImage(UIImage(named: "star")?.withRenderingMode(.alwaysTemplate), for: .normal)
        starButton.tintColor = UIColor.red
    }
 
}







//
//  DesignableElementsofInterface.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 03/06/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableCreateTripTextField: UITextField {
    
    
    @IBInspectable var cornerRadiusTextField : CGFloat = 0.0{
        didSet{
            self.layer.cornerRadius = cornerRadiusTextField
          //  self.layer.masksToBounds = true
            self.clipsToBounds = true
        }
    }
    @IBInspectable var shadowRadiusTextField : CGFloat = 0.0{
        didSet{
            self.layer.shadowRadius = shadowRadiusTextField
            self.layer.shadowOpacity = 1
        }
    }
    @IBInspectable var shadowColorTextField : UIColor = .clear{
        didSet{
            self.layer.shadowColor = shadowColorTextField.cgColor
        }
    }
    
    @IBInspectable var BorderWidthTextField : CGFloat = 0.0 {
        didSet{
            self.layer.borderWidth =  BorderWidthTextField
        }
    }
    
    @IBInspectable var leftImage : UIImage? {
        didSet {
            if let image = leftImage {
                leftViewMode = .always
                let imageView = UIImageView(frame: CGRect(x : 0, y : 0, width: 25, height: 20))
                imageView.contentMode = .scaleAspectFit
                imageView.image = image
                let view = UIView(frame : CGRect(x : 0, y : 0, width : 32, height : 25))
                view.addSubview(imageView)
                leftView = view
            } else {
                // Handle null
                leftViewMode = .never
            }
        }
    }
    
    @IBInspectable var BorderColorTextField : UIColor = .clear {
        didSet{
            self.layer.borderColor =  BorderColorTextField.cgColor
        }
    }
    @IBInspectable var bottomBorder : CGFloat = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var bottomBorderColor: UIColor = UIColor.clear {
        didSet {
            //refreshBorderColor(_colorBorder: bottomBorderColor)
            dropShadow()
        }
    }
    
    //override func layoutSubviews() {
        //refreshBorderColor(_colorBorder: bottomBorderColor)
    //}
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //self.textColor = UIColor(rgb: 0x9F9F9F)
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    /*func refreshBorderColor(_colorBorder: UIColor) {
        layer.borderColor = _colorBorder.cgColor
    }*/

    func setupView() {
        //refreshBorderColor(_colorBorder: bottomBorderColor)
    }
    
    func dropShadow() {
        //self.layer.borderColor = bottomBorderColor.cgColor
        self.layer.shadowColor = bottomBorderColor.cgColor
        self.layer.shadowOffset  = CGSize(width: 0.0, height: bottomBorder)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}


@IBDesignable
class DesignableButtons: UIButton {
    
     @IBInspectable  var cornerRadius: CGFloat = 0.0 {
        didSet {
            refreshCorners(value: cornerRadius)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            refreshBorder(_borderWidth: borderWidth)
        }
    }
    
    @IBInspectable var customBorderColor: UIColor = UIColor.clear {
        didSet {
            refreshBorderColor(_colorBorder: customBorderColor)
        }
    }
    
    @IBInspectable var shadowOpacity : Float = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    func refreshBorder(_borderWidth: CGFloat) {
        layer.borderWidth = _borderWidth
    }
    
    func refreshBorderColor(_colorBorder: UIColor) {
        layer.borderColor = _colorBorder.cgColor
    }
    
    func refreshCorners(value: CGFloat) {
        layer.cornerRadius = value
        self.clipsToBounds = true
    }

    /*override func layoutSubviews() {
       dropShadow()
    }*/
    func dropShadow() {
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    private func setupView() {
        refreshCorners(value: cornerRadius)
        refreshBorderColor(_colorBorder: customBorderColor)
        refreshBorder(_borderWidth: borderWidth)
        
    }
    
}


@IBDesignable
class DesignableImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    func setUpView() {
        self.layer.cornerRadius = cornerRadius
    }
    
}

@IBDesignable
class ContainerView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowColor: UIColor = UIColor.clear {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = .zero {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowOpacity : Float = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    override func layoutSubviews() {
        dropShadow()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        self.layer.cornerRadius = cornerRadius
    }
    
    func dropShadow() {
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
}

@IBDesignable
class DesignableSearchBar: UISearchBar {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowColor: UIColor = UIColor.clear {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowOffset : CGSize = .zero {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowOpacity : Float = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    @IBInspectable var shadowRadius : CGFloat = 0.0 {
        didSet {
            dropShadow()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
        dropShadow()
        
    }
    
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    private func setupView() {
        self.layer.cornerRadius = cornerRadius
        dropShadow()
    }
    
    func dropShadow() {
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        //self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
}
extension UIViewController {
    
    func saveObject(fileName: String, object: Any) -> Any {
        
        let filePath = self.getDirectoryPath().appendingPathComponent(fileName)
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: object, requiringSecureCoding: false)
            try data.write(to: filePath)
            return true
        } catch {
            print("error is: \(error.localizedDescription)")
        }
        return false
    }
    
    
    func getObject(fileName: String) -> Any? {
        
        let filePath = self.getDirectoryPath().appendingPathComponent(fileName)
        do {
            let data = try Data(contentsOf: filePath)
            let object = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
            return object
        } catch {
            print("error is: \(error.localizedDescription)")
        }
        return nil
    }
    
    func getDirectoryPath() -> URL {
        let arrayPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return arrayPaths[0]
    }
    func presentDet(_ viewContro: UIViewController){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(viewContro, animated: false, completion: nil)
    }
    func dismissDet(){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }
    func presentSec(_ viewContro: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromRight
        guard let presentedViewController = presentedViewController else {return}
        presentedViewController.dismiss(animated: false) {
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.present(viewContro, animated: false, completion: nil)
        }
    }
    func presentLeft(_ viewContro: UIViewController){
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = .push
        transition.subtype = .fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(viewContro, animated: false, completion: nil)
    }
}

extension UITableView {
    func addShadowSetUp() {
        self.layer.cornerRadius = 10
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
    func addShadowAndBorder(){
        self.layer.cornerRadius = 5
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.75
        self.layer.shadowOffset = .zero
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
    }
}

extension UITextView{
    func addShadowSetUp() {
        self.layer.cornerRadius = 10
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
}
extension UICollectionView{
    func addShadowSetUp() {
        self.layer.cornerRadius = 10
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
}

extension UICollectionViewCell{
    func addCollectionViewCellShadow(){
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 6
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 6
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
    
    func addShadowSetUp() {
        self.layer.cornerRadius = 10
        
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
    }
}


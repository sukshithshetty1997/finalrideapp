//
//  UIViewGradientExtension.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 23/05/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 3.0)
        gradientLayer.locations = [0.0, 0.1]
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}

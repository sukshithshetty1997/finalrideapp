//
//  CustomNavigationViewController.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 22/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

class CustomNavigationViewController: UINavigationController {
    var defaults = UserDefaults.standard
    
    
        override func viewDidLoad(){
            super.viewDidLoad()
            let TagView = defaults.object(forKey: "Tag") as? String
            print(self.restorationIdentifier!)
            let tag = self.restorationIdentifier
            if tag == "Trips"{
                if TagView == "One"{
                    print(TagView!)
                    let dest = self.storyboard?.instantiateViewController(withIdentifier: "WelComeAboardViewController") as! WelComeAboardViewController
                    self.setViewControllers([dest], animated: true)
                    self.viewControllers = [dest]
                }
                else if TagView == "Two"{
                    print(TagView!)
                       let dest =  self.storyboard?.instantiateViewController(withIdentifier: "GoTripRecordsViewController") as! GoTripRecordsViewController
                    self.setViewControllers([dest], animated: true)
                    self.viewControllers = [dest]
                }
            }
        }
    
    func backButtonLeftItem(_ controller: AnyObject) {
        let backBarImage = UIImage(named: "app bar")?.withRenderingMode(.alwaysTemplate)
        let backButton = UIBarButtonItem(image: backBarImage, style: .plain, target: controller, action: #selector(controller.transitionBackTo))
        controller.navigationItem.leftBarButtonItem = backButton
    }
    
    func backButtonRightItem(_ controller: AnyObject, _ rightImage: UIImage) {
        let editCreateTrip = UIBarButtonItem(image: rightImage, style: UIBarButtonItem.Style.plain, target: controller, action: #selector(controller.PopToCreateTrip))
        controller.navigationItem.rightBarButtonItem = editCreateTrip
        controller.navigationItem.rightBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)
    }
    
    func transparent(controller: AnyObject){
        controller.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        controller.navigationController?.navigationBar.shadowImage = UIImage()
        controller.navigationController?.navigationBar.isTranslucent = true
        controller.navigationController?.view.backgroundColor = .clear
        
    }
    
    func navigationBackButtonDesign(controller: AnyObject) {
        backButtonLeftItem(controller)
        controller.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0x4F504F)
        controller.navigationItem.rightBarButtonItem?.tintColor = UIColor(rgb: 0xF2944E)
        transparent(controller: controller)
    }
    
    func navigationBarDesign(controller: AnyObject, title: String) {
        backButtonLeftItem(controller)
        controller.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)
        
        controller.navigationItem.title = title
        controller.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 20) as Any,NSAttributedString.Key.foregroundColor : UIColor(rgb: 0xFFFFFF)]
        
        controller.navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xF09455)
        controller.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    func navigationLeftRightDesign(controller: AnyObject, title: String, rightImage: UIImage) {
        backButtonLeftItem(controller)
        backButtonRightItem(controller, rightImage)
        controller.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)
        
        controller.navigationItem.title = title
        controller.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 20) as Any,NSAttributedString.Key.foregroundColor : UIColor(rgb: 0xFFFFFF)]
        
        controller.navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xF09455)
        controller.navigationController?.navigationBar.isTranslucent = false
    }
    
    func barRightItems(_ controller: AnyObject, _ rightImages: [UIImage]) {

        let ShareButton = UIBarButtonItem(image: rightImages[0], style: UIBarButtonItem.Style.plain, target: controller, action: #selector(controller.navigationRightItem1))
        let EditButton = UIBarButtonItem(image: rightImages[1], style: UIBarButtonItem.Style.plain, target: controller, action: #selector(controller.navigationRightItem2))
        let ArrayBarButtons:[UIBarButtonItem] = [ShareButton, EditButton]
        controller.navigationItem.rightBarButtonItems = ArrayBarButtons
        controller.navigationItem.rightBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)
        
    }
    
    func navigationBarLeftRightDesign(controller: AnyObject, title: String, rightImages: [UIImage]) {
        backButtonLeftItem(controller)
        barRightItems(controller, rightImages)
        controller.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0xFFFFFF)
        
        controller.navigationItem.title = title
        controller.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 20) as Any,NSAttributedString.Key.foregroundColor : UIColor(rgb: 0xFFFFFF)]
        
        controller.navigationController?.navigationBar.barTintColor = UIColor(rgb: 0xF99C56)
        controller.navigationController?.navigationBar.tintColor = UIColor(rgb: 0xFFFFFF)
        controller.navigationController?.navigationBar.isTranslucent = false
    }
    
    func navigationLeftRightItems(controller: AnyObject, rightImage: UIImage) {
        print("Hello")
        backButtonLeftItem(controller)
        backButtonRightItem(controller, rightImage)
        controller.navigationItem.leftBarButtonItem?.tintColor = UIColor(rgb: 0xED7F2C)
        controller.navigationItem.rightBarButtonItem?.tintColor = UIColor(rgb: 0xED7F2C)
        transparent(controller: controller)
    }
    
    func hideNavigationBar(controller: AnyObject) {
        //controller.navigationController?.setNavigationBarHidden(true, animated: true)
        controller.navigationController?.navigationBar.isHidden = true
        controller.navigationController?.isNavigationBarHidden = true
    }
    
    func showNavigationBar(controller: AnyObject){
        controller.navigationController?.navigationBar.isHidden = false
        controller.navigationController?.isNavigationBarHidden = false
    }

    func addBorderShadow(controller: AnyObject){
        controller.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        controller.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        controller.navigationController?.navigationBar.layer.shadowRadius = 4.0
        controller.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        controller.navigationController?.navigationBar.layer.masksToBounds = false
    }
}

//
//  DesiganbleTextFields.swift
//  RideAppCreateTrip
//
//  Created by Zeeta Andrade on 16/04/20.
//  Copyright © 2020 Zeeta Andrade. All rights reserved.
//

import UIKit

@IBDesignable
class DesiganbleTextFields: UITextField {

    @IBInspectable var bottomBorder : CGFloat = 0.0 {
        didSet {
            /*
            let  frame = self.frame
            let bottomLayer = CALayer()
            bottomLayer.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width - 2, height: bottomBorder)
            bottomLayer.backgroundColor = UIColor.lightGray.cgColor
            // bottomLayer.borderWidth = 1
            self.layer.addSublayer(bottomLayer)
            */
            
            self.layer.shadowColor = UIColor.gray.cgColor
            self.layer.shadowOffset  = CGSize(width: 0.0, height: bottomBorder)
            self.layer.shadowOpacity = 1.0
            self.layer.shadowRadius = 0.0
        }
    }
    
    
}
